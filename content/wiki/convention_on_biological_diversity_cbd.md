+++
title = "Convention on Biological Diversity"
author = ["Matthew Harris"]
draft = false
math = true
toc = true
group_tag = "Conservation"
+++

## History of the Convention on Biological Diversity {#history-of-the-convention-on-biological-diversity}

-   (<a href="#citeproc_bib_item_7">United Nations, 1992</a>)
    -   Original convention
    -   "Conscious of the intrinsic value of biological diversity and of the ecological, genetic, socila, economic, scientific, educational, cultural, recreational and aesthetic values of biological diversity and its components"
    -   "Consious also of the importance of biological diversity for evolution and for maintaining life sustaining systems of the biosphere"
-   <https://www.cbd.int/history/>
    -   "on 22 May 1992 with the Nairobi Conference for the Adoption of the Agreed Text of the Convention on Biological Diversity."
    -   "The Convention was opened for signature on 5 June 1992 at the United Nations Conference on Environment and Development (the Rio "Earth Summit"). It remained open for signature until 4 June 1993, by which time it had received 168 signatures. The Convention entered into force on 29 December 1993, which was 90 days after the 30th ratification. The first session of the Conference of the Parties was scheduled for 28 November – 9 December 1994 in the Bahamas"


## Strategic Plan for Biodiversity 2011-2020 {#strategic-plan-for-biodiversity-2011-2020}


## The Post-2020 Global Biodiversity Framework {#the-post-2020-global-biodiversity-framework}


### First Draft of the Post-2020 Global Biodiversity Framework {#first-draft-of-the-post-2020-global-biodiversity-framework}

-   <a href="#citeproc_bib_item_5">Convention on Biological Diversity, 2020</a>
-   Paragraph 3:

    > The framework aims to facilitate implementation, which will primarily be through activities at the national level, with supporting action at the subnational, regional and global levels. Specifically, it provides a global, outcome-oriented framework for the development of national, and as appropriate, regional, goals and targets and, as necessary, the updating of national biodiversity strategies and action plans to achieve these, and to facilitate regular monitoring and review of progress at the global level
-   4 long-term goals for 2050, with milestones for 2030
-   21 action-oriented targets


### Kunming-Montreal Global biodiversity framework (18 December 2022) {#kunming-montreal-global-biodiversity-framework--18-december-2022}

On 18 December 2022, the Kunming-Montreal Global Biodiversity Framework <a href="#citeproc_bib_item_3">Convention on Biological Diversity, 2022</a> of the Convention on Biological Diversity was agreed.


#### Section G. Kunming-Montreal Global Goals for 2050 {#section-g-dot-kunming-montreal-global-goals-for-2050}

-   **Goal A**:

    > The integrity, connectivity and resilience of all ecosystems are maintained, enhanced, or restored, substantially increasing the area of natural ecosystems by 2050;
    >
    > Human induced extinction of known species is halted, and, by 2050, extinction rate and risk of all species are reduced tenfold and the abundance of native wild species is increased to healthy and resilient levels;
    >
    > The genetic diversity within populations of wild and domesticated species, is maintained, safeguarding their adaptive potential.


#### Section H. Kunming-Montreal 2030 Global Targets {#section-h-dot-kunming-montreal-2030-global-targets}

-   **Target 1**

    > Ensure that all areas are under participatory integrated biodiversity inclusive spatial planning and/or effective management processes addressing land and sea use change, to bring the loss of areas of high biodiversity importance, including ecosystems of high ecological integrity, close to zero by 2030, while respecting the rights of indigenous peoples and local communities.
-   **Target 2**

    > Ensure that by 2030 at least 30 per cent of degraded terrestrial, inland water, and coastal and marine ecosystems are under effective restoration, in order to enhance biodiversity and ecosystem functions and services, ecological integrity and connectivity.
-   **Target 12**

    > Significantly increase the area and quality and connectivity of, access to, and benefits from green and blue spaces in urban and densely populated areas sustainably, by mainstreaming the conservation and sustainable use of biodiversity, and ensure biodiversity-inclusive urban planning, enhancing native biodiversity, ecological connectivity and integrity, and improving human health and well-being and connection to nature and contributing to inclusive and sustainable urbanization and the provision of ecosystem functions and services.


### Monitoring framework for the Kunming-Montreal global biodiversity framework {#monitoring-framework-for-the-kunming-montreal-global-biodiversity-framework}

-   <a href="#citeproc_bib_item_4">Convention on Biological Diversity, 2022</a>
-   2011-2020 as reference period
-   "_Decides_ to use the period from 2011-2020, where data is available, as the reference period ... while noting that baselines, conditions and periods used to express desirable states or levels of ambition in goals and targets should, where relevant, take into account historical trends, current status, future scenarios of biodiversity and available information on the **natural** state" (emphasis added; paragraph 2)
-   Headline indicator for A.2 is Extent of natural ecosystems and A.1 is Red List of Ecosystems.
-   Their component indicators include Ecosystem Intactness index and Ecosystem Integrity Index
-   paragraph 3

    > Headline indicators use methodologies agreed by Parties and are calculated based on national data from national monitoring networks and national sources, calculated at national level, recognizing that in some cases this may need to draw on global dataset and if national indicators are not available then the use of global indicators at a national level must be validated through appropriate national mechanisms. These indicators would allow for consistent, standardized and scalable tracking of global goals and targets


### Statements regarding the CBD GBF agreed at COP15 {#statements-regarding-the-cbd-gbf-agreed-at-cop15}

-   **Wildlife Conservation Society** (<a href="#citeproc_bib_item_10">World Conservation Society, 2022</a>) -- "the Kunming-Montreal Global Biodiversity Framework has key commitments for nature, but governments will need to treat it as **a floor, not a ceiling**, for global action to halt the ongoing crisis of biodiversity loss." [emphasis added]
    -   Criticisms:
        -   "the document calls for achieving the agenda in 2050, way too late to address the biodiversity collapse crisis"
        -   "there is a lack of reference to vulnerable or threatened ecosystems"
    -   Susan Lieberman, vice president of international policy for the Wildlife Conservation Society: "The Kunming-Montreal Global Biodiversity Framework is a compromise and although it has several very good and hard-fought elements, it could have gone further to truly transform our destructive relationship with nature"
    -   Alfred DeGemmis, associate director of international policy for the Wildlife Conservation Society: "Governments will need to treat the Kunming-Montreal Global Biodiversity Framework as a floor, not a ceiling, for global action to halt the ongoing crisis of biodiversity. If fully implemented, this will make an important contribution to biodiversity conservation"
-   **WWF International** (<a href="#citeproc_bib_item_8">WWF, 2022</a>)
    -   Marco Lambertini, Director General of WWF International:
        -   "WWF celebrates the inclusion of an overarching global goal of halting and reversing biodiversity loss by 2030"
        -   "However, there still remain several loopholes, weak language, and timelines around actions that aren’t commensurate with the scale of the nature crisis we’re all witnessing, and importantly may not add up to achieve this shared global goal"
        -   "it is vital that 30% each of land, freshwater and oceans is conserved respectively"
        -   "We are particularly concerned by the weak language on species which would commit countries to halting extinctions at some point before 2050, instead of 2030"
        -   "We must start giving back to the planet, or we risk it giving up on us."
-   **The Nature Conservancy** (<a href="#citeproc_bib_item_6">The Nature Conservancy, 2022</a>)
    -   Andrew Deutz (Director of Global Policy, Institutions and Conservation Finance):
        -   "The Kunming-Montreal Global Biodiversity Framework provides a long-needed international blueprint to guide our collective turnaround of nature’s fortunes within this crucial decade."
-   **Client Earth** (<a href="#citeproc_bib_item_2">ClientEarth, 2022</a>)
    -   Ioannis Agapakis (CE environmental lawyer)
        -   "The final agreement is a noteworthy moment ... but it is no way the 'Paris moment' for nature we were promised"
        -   "World leaders seem to be planning around multi-decade horizons, when we only have eight years"
        -   "Worryingly, there are several targets where ambition is even lower than in the previous framework ... backtracking on these targets shows that global leaders have failed to understand the true importance of biodiversity by shying away from transforming the global economic system and mitigating its impact on nature"
        -   "the language around enhancing action at national level in response to the global review of progress is weak and vague in places"
    -   Agata Szafraniuk (CE head of wildlife and habitats)
        -   "The challenge is that the GBF is not legally binding, so it cannot be used on its own to hold countries accountable if they fail to meet the Framework’s goals and targets"
-   **Wetlands International** (<a href="#citeproc_bib_item_9">Wetlands International, 2022</a>)
    -   "Wetlands International welcomes the adoption of the Kunming-Montreal Agreement adopted by 196 countries under the UN Convention on Biological Diversity, committing the world to halting and reversing biodiversity loss by 2030"


## Criticisms of the CBD {#criticisms-of-the-cbd}

-   <a href="#citeproc_bib_item_1">Büscher and Duffy, 2022</a>
    -   "have they [UN summits] become empty institutional hangovers of a lingering status quo that must be abandoned? Or is holding on to the fraying shreds of multilateralism worth the effort, even if they are becoming little more than extravagant witnesses to unfolding disaster?"
    -   "And so, there is a case to be made that international treaties actually deepen environmental destruction by making the problem seem soluble without changing a deeply unsustainable global economic system. They promote carbon offsets, biodiversity credits, no net loss (the idea that negative and positive consequences for biodiversity can be balanced as if on an accounting sheet) and other non-solutions. Fundamentally missing is a plan for an economy that accepts ecological limits to growth"


## Secretariat of the Convention on Biological Diversity (2020) 'Global Biodiversity Outlook 5' {#secretariat-of-the-convention-on-biological-diversity--2020--global-biodiversity-outlook-5}


### Summary for Policy Makers {#summary-for-policy-makers}

-   "At the global level none of the 20 targets have been fully achieved, though six targets have been partially achieved (Targets 9, 11, 16, 17, 19 and 20)" _[10]_


## Bibliography {#bibliography}

<style>.csl-entry{text-indent: -1.5em; margin-left: 1.5em;}</style><div class="csl-bib-body">
  <div class="csl-entry"><a id="citeproc_bib_item_1"></a>Büscher, B. and Duffy, R. (2022) “Biodiversity Treaty: UN Deal Fails to Address the Root Causes of Nature’s Destruction”. <i>The Conservation</i> [online] Available at: <a href="https://theconversation.com/amp/biodiversity-treaty-un-deal-fails-to-address-the-root-causes-of-natures-destruction-196905">https://theconversation.com/amp/biodiversity-treaty-un-deal-fails-to-address-the-root-causes-of-natures-destruction-196905</a> (Accessed: December 23, 2022)</div>
  <div class="csl-entry"><a id="citeproc_bib_item_2"></a>ClientEarth (2022) <i>COP15: Final Agreement Shies Away from Real Change to Global Economy to Save Biodiversity – ClientEarth | ClientEarth</i> [online] Available at: <a href="https://www.clientearth.org/latest/press-office/press/cop15-final-agreement-shies-away-from-real-change-to-global-economy-to-save-biodiversity-clientearth/">https://www.clientearth.org/latest/press-office/press/cop15-final-agreement-shies-away-from-real-change-to-global-economy-to-save-biodiversity-clientearth/</a> (Accessed: December 21, 2022)</div>
  <div class="csl-entry"><a id="citeproc_bib_item_3"></a>Convention on Biological Diversity (2022) <i>Kunming-Montreal Global Biodiversity Framework</i> [online] CBD/COP/15/L.25. Montreal. Available at: <a href="https://www.cbd.int/doc/c/e6d3/cd1d/daf663719a03902a9b116c34/cop-15-l-25-en.pdf">https://www.cbd.int/doc/c/e6d3/cd1d/daf663719a03902a9b116c34/cop-15-l-25-en.pdf</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_4"></a>Convention on Biological Diversity (2022) <i>Monitoring Framework for the Kunming-Montreal Global Biodiversity Framework</i> [online] CBD/COP/15/L.26. Montreal. Available at: <a href="https://www.cbd.int/doc/decisions/cop-15/cop-15-dec-05-en.pdf">https://www.cbd.int/doc/decisions/cop-15/cop-15-dec-05-en.pdf</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_5"></a>Convention on Biological Diversity (2020) <i>First Draft of the Post-2020 Global Biodiversity Framework</i> [online] CBD/WG2020/3/3. Available at: <a href="https://www.cbd.int/doc/c/abb5/591f/2e46096d3f0330b08ce87a45/wg2020-03-03-en.pdf">https://www.cbd.int/doc/c/abb5/591f/2e46096d3f0330b08ce87a45/wg2020-03-03-en.pdf</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_6"></a>The Nature Conservancy (2022) <i>Media Statement: UN Biodiversity Conference CBD-COP15 Scores Historic Goal for Nature</i> [online] Available at: <a href="https://www.nature.org/en-us/newsroom/media-statement-tnc-kunming-montreal-global-biodiversity-framework/">https://www.nature.org/en-us/newsroom/media-statement-tnc-kunming-montreal-global-biodiversity-framework/</a> (Accessed: December 21, 2022)</div>
  <div class="csl-entry"><a id="citeproc_bib_item_7"></a>United Nations (1992) <i>Convention on Biological Diversity</i> [online] Available at: <a href="https://www.cbd.int/doc/legal/cbd-en.pdf">https://www.cbd.int/doc/legal/cbd-en.pdf</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_8"></a>WWF (2022) <i>WWF Reaction to New COP15 Text</i> [online] Available at: <a href="https://wwf.panda.org/wwf_news/?7334466/WWF-reaction-to-new-COP15-text-published-18-Dec">https://wwf.panda.org/wwf_news/?7334466/WWF-reaction-to-new-COP15-text-published-18-Dec</a> (Accessed: December 21, 2022)</div>
  <div class="csl-entry"><a id="citeproc_bib_item_9"></a>Wetlands International (2022) <i>Wetlands International Welcomes the Adoption of the Kunming-Montreal Biodiversity Agreement</i> [online] Available at: <a href="https://www.wetlands.org/news/wetlands-international-welcomes-the-adoption-of-the-kunming-montreal-agreement/">https://www.wetlands.org/news/wetlands-international-welcomes-the-adoption-of-the-kunming-montreal-agreement/</a> (Accessed: December 21, 2022)</div>
  <div class="csl-entry"><a id="citeproc_bib_item_10"></a>World Conservation Society (2022) <i>The Kunming-Montreal Global Biodiversity Framework Is ‘The Floor, Not A Ceiling’ for Global Action to Halt Biodiversity Crisis</i> [online] Available at: <a href="https://newsroom.wcs.org/News-Releases/articleType/ArticleView/articleId/18452/The-Kunming-Montreal-Global-Biodiversity-Framework-Is-The-Floor-Not-A-Ceiling-for-Global-Action-to-Halt-Biodiversity-Crisis.aspx">https://newsroom.wcs.org/News-Releases/articleType/ArticleView/articleId/18452/The-Kunming-Montreal-Global-Biodiversity-Framework-Is-The-Floor-Not-A-Ceiling-for-Global-Action-to-Halt-Biodiversity-Crisis.aspx</a> (Accessed: December 19, 2022)</div>
</div>
