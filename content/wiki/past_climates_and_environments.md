+++
title = "Past Climates and Environments"
author = ["Matthew Harris"]
draft = false
math = true
toc = true
group_tag = "Other"
+++

## <span class="section-num">1</span> Records, Proxies and Methods {#records-proxies-and-methods}


### <span class="section-num">1.1</span> General {#general}

-   <span class="underline">Uniformitarianism</span>: the present is the key to the past
-   <span class="underline">Autochthonous</span>: formed _in situ_
-   <span class="underline">Allochthonous</span>: originated from far away (in-washed, wind-blown)


### <span class="section-num">1.2</span> Lake and Peatland Records {#lake-and-peatland-records}


#### <span class="section-num">1.2.1</span> Types of Lake {#types-of-lake}

-   Eutrophic lakes: nutrient-rich, high organic productivity, mainly autochthonous
-   Oligotrophic: nutrient-poor, low productivity, mainly allochthonous


#### <span class="section-num">1.2.2</span> Sampling methods {#sampling-methods}

<span class="underline">Gravity cores</span> (lakes)

-   cm-scale | 10s years
-   Un-compacted organic and inorganic material
-   Sediment-water interface

<span class="underline">Sediment Cores</span> (lakes)

-   m-scale | 1000s years
-   Lake edge, platform, paleolake surgace

<span class="underline">Drill Cores</span> (lakes)

-   10s-100s m | &gt; 10,000 years
-   Comapcted sediment

<span class="underline">Russian Coring</span> (peat)

-   &lt; 10 m
-   Manual coring from bog surface


#### <span class="section-num">1.2.3</span> Lake Sediment Proxies {#lake-sediment-proxies}

<span class="underline">Sediment -- clastic</span>
<span class="underline">Organic</span>

-   Pollen
-   Chironomids
    -   4 life stages: Egg &rarr; Larva &rarr;  Pupa &rarr; Imago (adult)
    -   They are species-rich, climate sensitive and **stenotopic** (have a narrow ecological optima)
    -   Quantitative climate proxy using multivariate transfer functions based on training sets

<span class="underline">Chemical</span>


#### <span class="section-num">1.2.4</span> Lake and Peat Formation - Hydrosere Succession {#lake-and-peat-formation-hydrosere-succession}


#### <span class="section-num">1.2.5</span> Ombrotrophic bogs {#ombrotrophic-bogs}

-   Water table maintained by atmospheric moisture -- i.e., precipitation is main control
-   Anoxic preservation


#### <span class="section-num">1.2.6</span> Minerotrophic Bogs {#minerotrophic-bogs}

-   groundwater-fed


#### <span class="section-num">1.2.7</span> Peatland proxies {#peatland-proxies}

<span class="underline">Sediment proxies</span>
<span class="underline">Organic proxies</span>

-   Sphagnum macrofossils
-   Testate Amoeba

<span class="underline">Chemical Proxies</span>


#### <span class="section-num">1.2.8</span> Dating Lake and Peat Records {#dating-lake-and-peat-records}

<span class="underline">Direct Dating</span>

-   Radiocarbon Dating
-   Lead Dating
-   Luminescence Dating
-   Varve Counting -- counting annual layers formed by seasonal biogenic or clastic cycles

<span class="underline">Indirect Dating</span>

-   Tephra layers
-   Paeleomagnetic excursions
-   Age Modelling


### <span class="section-num">1.3</span> Terrestrial Evidence for Glaciated Landscapes {#terrestrial-evidence-for-glaciated-landscapes}


#### <span class="section-num">1.3.1</span> General {#general}

-   Direct evidence from glacial landforms and sediments and periglacial landscapes
-   "Relict" or "fossil" features
-   Important to date


#### <span class="section-num">1.3.2</span> Mapping {#mapping}

<span class="underline">Geomorphological mapping</span>

-   Landscape interpretation
-   _relatice_ chronological data

<span class="underline">Field mapping</span>

-   Detailed groynd observations
    -   Breaks of slope, heights, distances
    -   Orientation
    -   Feature relationships
-   Lithological evidence

<span class="underline">Remote Sensing</span>

-   DEMs from aerial photography, satellite imagery, radar, drones, sonar, and seismic mapping (latter two for submarine mapping)
-   Allows imaging inaccessible areas
-   Fast, accurate and increased spatial coverage
-   Evans _et al._ (2020) -- DEM, aerial photos, google earth and archival maps
-   Hughes _et al._ (2010) -- combining shaded relief images to map drumlin fields
-   **Stratigraphic order** &rarr; relative ages

<span class="underline">Interpreting mapped landscapes</span>

-   Requries understanding of glacial processes


#### <span class="section-num">1.3.3</span> Glacial Landforms {#glacial-landforms}

-   Extent, thickness and behaviour of ice masses: location, volume, direction and rate of movement, advance and retreat


#### <span class="section-num">1.3.4</span> Sub-glacial landforms {#sub-glacial-landforms}

-   Sediment/till
-   Drumlins, eskers, channels, kame and kettle landscapes
-   Indicate: ice contact type, drainage, sediment supply, flow direction


#### <span class="section-num">1.3.5</span> Proglacial (glacier-terminal) landforms {#proglacial--glacier-terminal--landforms}

-   Moraines -- **maximum** positions and recessions
-   Overprinting &rarr; can't map advances
-   Moraines can extend beyond coastlines
-   Dowdeswell _et al._ (2016) -- mapping submarine glacial landforms


#### <span class="section-num">1.3.6</span> Peri-glacial processes and landforms {#peri-glacial-processes-and-landforms}

-   Cryoturbation and frost action
-   Ice-wedge polygons
-   Solifluction
-   Patterned ground (e.g., in the Breckland)
-   Pingoes
-   Frost weathering and depris cones
-   Blockfields -- re-interpreting them? Cold-based ice?
-   Striations and moulded bedrock &rarr; wet-based ice


#### <span class="section-num">1.3.7</span> Sediments {#sediments}

-   Fabric analysis reveals ice and sediment flow directions, transport processes and provenance

<span class="underline">Sediment core sequences</span>

-   paraglacial conditions in periglacial depressions
-   Glacial processes in glacier-fed lakes


#### <span class="section-num">1.3.8</span> Challenges {#challenges}

-   Discontinuous, eroded, dating difficulties


#### <span class="section-num">1.3.9</span> Examples {#examples}

<a href="#citeproc_bib_item_7">Van der Bilt and Lane, 2019</a>

-   Azorean Tephra between 278 and 279 cm core depth
-   Implication of unglacieated between 30 and 20 ka BP then cold-based ice
-   "the reported tephra isochron marks the most distal find of volcanic ash from the Azores and represents a valuable new LGM time marker that has the potential to synchronize paleoclimate reconstructions across the entire North Atlantic region."


### <span class="section-num">1.4</span> Paleoclimate records {#paleoclimate-records}


#### <span class="section-num">1.4.1</span> Tephrochronology {#tephrochronology}

-   <a href="#citeproc_bib_item_2">Abbott and Davies, 2012</a>
-   <a href="#citeproc_bib_item_6">Berben <i>et al.</i>, 2020</a>
-   <a href="#citeproc_bib_item_16">Davies, 2015</a>
-   <a href="#citeproc_bib_item_40">Lowe, 2011</a>
-   <a href="#citeproc_bib_item_41">Lowe <i>et al.</i>, 2015</a>


#### <span class="section-num">1.4.2</span> Paleoceonagraphy {#paleoceonagraphy}


##### <span class="section-num">1.4.2.1</span> Marine \\(\delta\ce{^{18}O}\\) records {#marine-delta-ce-18-o-records}

"Foraminiferal \\(\delta\ce{^{18}O}\\) is a function of the temperature and \\(\delta\ce{^{18}O}\\) of the water in which it forms, and the \\(\delta\ce{^{18}O}\\) of the seawater is a function of global ice volume and water salinity" (<a href="#citeproc_bib_item_38">Lisiecki and Raymo, 2005</a>).

<a href="#citeproc_bib_item_18">Elderfield <i>et al.</i>, 2012</a> find no evidence of progressive cooling throughout their record.


##### <span class="section-num">1.4.2.2</span> Assumptions and Challenges of Marine Records {#assumptions-and-challenges-of-marine-records}

<a href="#citeproc_bib_item_68">Shackleton, 1987</a> -- never a perfect deep-sea record:

-   Temperature affects foraminifera
-   Bioturbation and other effects on the marine sediment

Many factors affect the oxygen isotope composition of foraminifera, including the season of life and the inhabited depth in the water colymn, therefore, benthic foraminifera are better <a href="#citeproc_bib_item_68">Shackleton, 1987</a>.

A study by <a href="#citeproc_bib_item_18">Elderfield <i>et al.</i>, 2012</a> found that "benthic \\(\delta\ce{^{18}O}\\) is not a direct proxy for ice volume or sea level [... and] because the LR04 stack is often taken as a proxy for ice volume, it is interesting that the temperature component of the record may approach as much as 50% of the range in \\(\delta\ce{^{18}O}\\)" (p. 706).


## <span class="section-num">2</span> The Early-Mid Pleistocene Transition {#the-early-mid-pleistocene-transition}

"The EMPT is the marked prolongation and intensification of glacial-interglacial climate cycles initiated sometime between 900 and 650 ka" (<a href="#citeproc_bib_item_43">Maslin and Brierley, 2015, 47</a>).

<a href="#citeproc_bib_item_18">Elderfield <i>et al.</i>, 2012</a> argue that before the MPT, obliquity had a weaker role due to smaller ice sheets.


## <span class="section-num">3</span> General Causes of climatic change {#general-causes-of-climatic-change}


### <span class="section-num">3.1</span> Causes of the Quaternary Ice Age {#causes-of-the-quaternary-ice-age}

<a href="#citeproc_bib_item_3">Bartoli <i>et al.</i>, 2011</a> estimated atmospheric \\(\ce{CO2}\\) during the Pliocene and early Plesitocene, and found a decrease in both the minimal and maximal \\(\ce{CO2}\\) estimates since 4.1 Ma, with a step at 2.7 Ma. They argue that the timing of the final closure of the Panama (e.g., Bartoli _et al._ 2005) suggests that it did **not** influence the decrease in atmospheric \\(\ce{CO2}\\).

The Isthmus of Panama closed between 13 and 1.9 Myr, with the final closing at 2.7 Myr (<a href="#citeproc_bib_item_22">Haug and Tiedemann, 1998</a>), which has impacted deep sea ocean circulation since 4.6 Myr. At 3.6 Ma, a ventilation maximum transported moisture and heat to the NH, perhaps contributing to the mid-Pliocene warm period.

<a href="#citeproc_bib_item_22">Haug and Tiedemann, 1998</a> argue that sustained and prominent NH ice sheet(s) require three factors, that were met at the Pliocene-Pleistocene transition:
a) Threshold level of cooling &rarr; snowfall
b) NH high-latitude moisture levels (increase THC and Gulf stream from 4.6 Myr)
c) And the final trigger was the "progressive increase in obliquity amplitudes between 3.1 and 2.5 Myrs"


### <span class="section-num">3.2</span> Interglacials and Glacials {#interglacials-and-glacials}


#### <span class="section-num">3.2.1</span> Orbital Cycles {#orbital-cycles}

"precession will tend to influence the precise timing of a deglaciation within an Obliquity cycle, but Obliquity will more fundamentally govern the interval between deglaciations" (<a href="#citeproc_bib_item_29">Huybers, 2011, 231</a>)

<a href="#citeproc_bib_item_23">Hays <i>et al.</i>, 1976</a> provided some of the first evidence of orbital forcing of climatic change during the Quaternary, by analysing a 450,000 year record of \\(\delta\ce{^{18}O}\\). They found that the frequencies of insolation spectra reflect obliquity and precession, but not eccentricity, and that the importance of the obliquity and precession components varies with latitude and season. The dominant cycle in \\(\delta\ce{^{18}O}\\) was 100,000 years, but they argued that this was not a linear response, and that eccentricity was **not** the driver of the cycle (<a href="#citeproc_bib_item_23">Hays <i>et al.</i>, 1976</a>).


#### <span class="section-num">3.2.2</span> Solar Insolation {#solar-insolation}

A 640kyr-330 kyr BP record from Sanbai Cave, China of the Asian Monsoon, added to previous records and created a 640-kyr proxy record (<a href="#citeproc_bib_item_12">Cheng <i>et al.</i>, 2016</a>). The observations of the past seven glacial terminations "support the hypothesis that rising NHSI [solar insolation] triggers an initial ice-sheet disintegration, which in turn perturbs the oceanic and atmospheric heat and carbon cycles, resulting in a CO2 increase, which further drives the termination" (<a href="#citeproc_bib_item_12">Cheng <i>et al.</i>, 2016, 642</a>)


#### <span class="section-num">3.2.3</span> Other {#other}

<a href="#citeproc_bib_item_62">Raymo, 1997</a> argued that a 'true' glacial termination occured when there is 'surplus' ice build up due to "unusually long intervals of low summer insolation ... caused by the interaction of obliquity and eccentricity modulation of precession". They argue that before 800ka, it was not cold enough; the boundary change was a decrease in \\(\ce{CO2}\\) due to increased drawdown beause of increased uplift (e.g., see (<a href="#citeproc_bib_item_63">Raymo and Ruddiman, 1992</a>)). The next increase in summer insolation led to warming and catastrophic melt of the IC.


### <span class="section-num">3.3</span> Volcanism and Climate {#volcanism-and-climate}

-   <a href="#citeproc_bib_item_30">Huybers and Langmuir, 2009</a>
-   <a href="#citeproc_bib_item_31">Huybers and Langmuir, 2017</a>


## <span class="section-num">4</span> The Pliocene {#the-pliocene}

-   <a href="#citeproc_bib_item_19">Fedorov <i>et al.</i>, 2013</a>
-   <a href="#citeproc_bib_item_3">Bartoli <i>et al.</i>, 2011</a>


## <span class="section-num">5</span> The Holocene {#the-holocene}


### <span class="section-num">5.1</span> The Homeric Climate Oscillation {#the-homeric-climate-oscillation}

-   <a href="#citeproc_bib_item_42">Martin-Puertas <i>et al.</i>, 2012</a>
-   <a href="#citeproc_bib_item_73">Van Geel <i>et al.</i>, 1996</a>
-   <a href="#citeproc_bib_item_60">Rach <i>et al.</i>, 2017</a>
-   <a href="#citeproc_bib_item_44">Mauquoy <i>et al.</i>, 2004</a>
-   <a href="#citeproc_bib_item_45">Mighall <i>et al.</i>, 2006</a>
-   <a href="#citeproc_bib_item_21">Van Geel <i>et al.</i>, 2014</a>
-   <a href="#citeproc_bib_item_34">Lamentowicz <i>et al.</i>, 2015</a>
-   <a href="#citeproc_bib_item_71">Swindles <i>et al.</i>, 2007</a>
-   <a href="#citeproc_bib_item_33">Kilian <i>et al.</i>, 1995</a>


### <span class="section-num">5.2</span> General {#general}

The '2kyr shift' is an anomalous increase in the Asian Monsoon (AM) in the last 2kyrs of the Holocene and was a global, not regional event: the AM was positively correlated with atmospheric \\(\delta\ce{^{18}O}\\) and anti-correlated with the South American Monsoon and Antarctic temperatures (<a href="#citeproc_bib_item_12">Cheng <i>et al.</i>, 2016</a>). <a href="#citeproc_bib_item_12">Cheng <i>et al.</i>, 2016</a> suggest that this is possibly linked to the increase in AMOC seen over the last 2kyrs.


### <span class="section-num">5.3</span> References {#references}

-   <a href="#citeproc_bib_item_14">Claussen <i>et al.</i>, 1999</a>
-   <a href="#citeproc_bib_item_75">Wanamaker <i>et al.</i>, 2012</a>
-   <a href="#citeproc_bib_item_10">Büntgen <i>et al.</i>, 2016</a>


## <span class="section-num">6</span> Millennial-scale climate variability during the Last Glacial Period {#millennial-scale-climate-variability-during-the-last-glacial-period}

Key Papers

| Papers                                                                | Summary                                        |
|-----------------------------------------------------------------------|------------------------------------------------|
| <a href="#citeproc_bib_item_66">Sanchez Goñi and Harrison, 2010</a>   | Overview of Millennial-scale change during LGP |
| <a href="#citeproc_bib_item_74">WAIS Divide Project Members, 2015</a> | Precise inter-polar phasing during LGP         |


### <span class="section-num">6.1</span> Dansgaard-Oeschger Cycles {#dansgaard-oeschger-cycles}

Test


#### <span class="section-num">6.1.1</span> Reference summary {#reference-summary}

| Ref                                                                   | Summary                          |
|-----------------------------------------------------------------------|----------------------------------|
| <a href="#citeproc_bib_item_17">Dokken <i>et al.</i>, 2013</a>        | Nordic Sea Ice Cover Change      |
| <a href="#citeproc_bib_item_37">Li and Born, 2019</a>                 | Atmosphere-ocean-ice coupling    |
| <a href="#citeproc_bib_item_67">Seidov and Maslin, 2001</a>           | Bi-polar see-saw: ocean conveyor |
| <a href="#citeproc_bib_item_56">Peltier and Vettoretti, 2014</a>      | Kicked Salt Oscillator           |
| <a href="#citeproc_bib_item_78">Zhang <i>et al.</i>, 2014</a>         | Intermediate LIS Height Changes  |
| <a href="#citeproc_bib_item_74">WAIS Divide Project Members, 2015</a> | Precise inter-polar phasing      |
| <a href="#citeproc_bib_item_25">Henry <i>et al.</i>, 2016</a>         | AMOC Circulation Changes (Pa/Th) |


#### <span class="section-num">6.1.2</span> Nordic Sea Stratification and Sea Ice Changes {#nordic-sea-stratification-and-sea-ice-changes}


#### <span class="section-num">6.1.3</span> Bi-Polar See-saw {#bi-polar-see-saw}

Longer term climate change is likely driven by the ocean due to its volume, inertia and heat capacity (<a href="#citeproc_bib_item_67">Seidov and Maslin, 2001</a>).

The '**oscillating conveyor hypothesis**' suggests that as the southern hemisphere ocean warms, the northern hemisphere ocean cools (<a href="#citeproc_bib_item_67">Seidov and Maslin, 2001</a>). <a href="#citeproc_bib_item_67">Seidov and Maslin, 2001, 324</a> argue that "The imbalance bewteen the NADW and Antarctic Bottom Water (AABW) productions, i.e. between _the deep ocean flows_, is the primary control on corss-equatorial heat transport, and thus could be the sole agent responsible for the observed see-saw climate oscillations". This imbalance leads to 'heat piracy', where heat is transported between the oceans. During Heinrich events, there is South Atlantic heat piracy as the AABW is greater than the NADW, leading to heat transfer southwards across the equator, cooling the northern hemisphere. During recovery, there is North Atlantic Heat Piracy, cooling the southern hemisphere.

<a href="#citeproc_bib_item_67">Seidov and Maslin, 2001, 324</a> argue that "The imbalance bewteen the NADW and Antarctic Botto mWater (AABW) productions, i.e. between _the deep ocean flows_, is the primary control on corss-equatorial heat transport, and thus could be the sole agent responsible for the observed see-saw climate oscillations".

<a href="#citeproc_bib_item_56">Peltier and Vettoretti, 2014</a> use the CSM1 model with their configuration that produces DO-cycles through a [kicked salt oscillator](#kicked-salt-oscillator) mechanism, to reproduce the bi-polar see-saw.


#### <span class="section-num">6.1.4</span> The \`Kicked' Salt Oscillator {#kicked-salt-oscillator}

<a href="#citeproc_bib_item_56">Peltier and Vettoretti, 2014</a> suggest a 'kicked' salt oscillator to explain Dansgaard-Oeschger Cycles. They suggest that Heinrich events provide a 'kick' to the system to create DO Cycles and present a mechanism due to the "buildup and collapse of the meridional salinity gradient between the subtropical gyre of the North Atlantic Ocean and that beneath the sea ice cover to the north" (<a href="#citeproc_bib_item_56">Peltier and Vettoretti, 2014, 7309</a>).

Stadial conditions: Anomalously high salinity in NA Subtropical gyre; polar halocline, underneath sea ice, anomalously low.

AMOC and sea ice variability is tightly coupled.

1.  Development of stadial conditions
2.  &uarr; latitudinal salinity gradient
3.  &uarr; salinity of subtropical gyre
4.  &darr; salinity of halocline beneath sea ice lid
5.  Critical salinity gradient &rarr; "a strong northward flux of salinity ensures" _[7310]_
6.  Collapse of salinity gradient &rarr; AMOC re-invigoration
7.  Interstadial transition

The mechanism proposed by <a href="#citeproc_bib_item_56">Peltier and Vettoretti, 2014</a> does **not** require a freshwater input even though it is a salt oscillation.


#### <span class="section-num">6.1.5</span> Laurentide Ice Sheet Height Changes {#lis-height}

<a href="#citeproc_bib_item_78">Zhang <i>et al.</i>, 2014</a> argue that because DO events almost always occur with intermediate sea levels, and thus intermediate ice sheets, ice sheet changes could be a driver of DO events. Their hypothesis is that changes in the height of the Laurentide Ice Sheet causes changes in the Northern Westerlies, which affects sea ice export in the NE N.Atlantic and NADW formation through two mechanisms:

1.  Sea ice export changes
2.  wind field changes affecting subpolar and subtropical gyre

a

1.  NHIS height changes
2.  Shift in Northern Westerlies
3.  decreased zonal stress in South Labrador Sea
4.  Weakened sea ice export to the NE North Atalntic
5.  &darr; sea ice cover
6.  surface warming
7.  Open convection
8.  NADW formation and enhanced AMOC

Including a model with \\(\ce{CO2}\\) shows changes in-line with those observed during Heinrich events.


#### <span class="section-num">6.1.6</span> Atmosphere-ocean-ice Coupling {#atmosphere-ocean-ice-coupling}


#### <span class="section-num">6.1.7</span> Volcanic Eruptions {#volcanic-eruptions}

<a href="#citeproc_bib_item_39">Lohmann and Svensson, 2022</a> [note: preprint at time of notes] use Greenland and Antarctic ice core records to create a record of bipolar volcanism and show "that bipolar volcanic eruptions occured significantly more frequently than expected by chance just before the onset of Dansgaard-Oeschger events" _[abstract]_. "Out of the 20 abrupt warming events in the 12-60 ka period, 5 (7) occur within 20 (50) years after a bipolar eruption", but not the same relationship before the abrupt cooling events. "We hypothesise that this asymmetric response to volcanic eruptions may be a result of the direct influence of volcanic cooling" on the AMOC. "Transitions from a weak to a strong circulation mode, but not vice versa, may be triggered by cooling in the North Atlantic, given the circulation is close to a stability threshold". Use global ocean models to show this mechanism.


### <span class="section-num">6.2</span> Heinrich Events {#heinrich-events}

| Ref                                                           | Summary                             |
|---------------------------------------------------------------|-------------------------------------|
| <a href="#citeproc_bib_item_24">Hemming, 2004</a>             | Overview of Heinrich Events and IRD |
| <a href="#citeproc_bib_item_5">Bassis <i>et al.</i>, 2017</a> | Ice Sheet Advance &amp; Retreat     |


#### <span class="section-num">6.2.1</span> Ice Sheet Advance and Retreat {#ice-sheet-advance-and-retreat}

<a href="#citeproc_bib_item_5">Bassis <i>et al.</i>, 2017</a> used a ocean-ice-atmospheric model with constant atmospheric forcing (as cold temperatures were observed at the onset of HEs) to model the ocean-ice interactions. AMOC reductions lead to subsurface warming. <a href="#citeproc_bib_item_5">Bassis <i>et al.</i>, 2017</a> suggest a mechanism for Heinrich Events, whereby subsurface ocean warming leads to melt and rapid retreat of the ice sheet front, until isostatic adjustment isoaltes the terminus. There is then slow regrowth (as the uplifted sill protects against warming at the next DO event) and then collapse. The model predicted \\(\sim2\\) m of sea level rise, which is similar to observations. It is likely that this mechanism triggers the melt of other ice masses and releases small amounts of IRD -- which matches the low concentration of non-Hudson-Strait derived IRD. <a href="#citeproc_bib_item_5">Bassis <i>et al.</i>, 2017</a> note that this idea is based on current Greenland marine-terminating glaciers, and suggest that areas of the West Antarctic Ice Sheet, such as the Amundsen Sea Embayment, "could be vulnerable to an ocean-triggered Heinrich-event-style demise, even in the absence of atmospheric warming" (p. 334).


## <span class="section-num">7</span> The Younger Dryas {#the-younger-dryas}


### <span class="section-num">7.1</span> Introduction {#introduction}

The Younger Dryas is an abrupt cold period that occurred during the Last Deglaciation. A Greenland Ice Core chronology for the Last Deglaciation (<a href="#citeproc_bib_item_61">Rasmussen <i>et al.</i>, 2006</a>) dates the onset of the YD to 12,896 years b2k (maximum counting error, 138), and the termination (or the Younger Dryas-Preboreal transition) to 11,703 years b2k (MCE, 99).

Summers in Europe were on average \\(1.7\celsius\\) cooler than the Allerød interstadial, with Northwestern Europe experiencing the greatest cooling (\\(4\celsius\\)) and the southeast the least (\\(0.5\celsius\\)) (<a href="#citeproc_bib_item_65">Renssen <i>et al.</i>, 2015</a>). This sharp temperature drop occurred despite a 11 ka solar maximum (<a href="#citeproc_bib_item_65">Renssen <i>et al.</i>, 2015</a>).

"The magnitude of the Younger Dryas climate anomaly (cooler/drier) increases in with latitude in the Northern Hemisphere, with an opposite pattern (warmer/wetter) in the Southern Hemisphere reflecting a general bipolar seesaw climate response" (<a href="#citeproc_bib_item_69">Shakun and Carlson, 2010</a>). "Global mean temperature decreased by \\(\sim0.6\celsius\\) during the Younger Dryas. Therefore, our analysis supports the paradigm that whi the Younger Dryas was a period of global climate change, it was not a major cooling event but rather a manifestation of the bipolar seesaw driven by a reduction in Atlantic meridional overturning circulation strength" (<a href="#citeproc_bib_item_69">Shakun and Carlson, 2010</a>).


#### <span class="section-num">7.1.1</span> A ? {#a}

Multivariate statistical analysis by <a href="#citeproc_bib_item_52">Nye and Condron, 2021</a> found that the Bølling-Allerød/Younger Dryas event is "not statistically different from other D-O events in the Greenland record and that it should not necessarily be considered unique when investigating the drivers of abrupt climate change" _[1409]_. The NGRIP \\(\delta\ce{^{18}O}\\) record for the BA/YD is very similar to other . However, the YD/BA does not "strictly follow the trend of the seven time series lines or mean line in the two Antarctic record overlays (EDML \\(\delta\ce{^{18}O}\\) and \\(\ce{CO2}\\)), which indicates that further study is required to determine how the BA/YD might constitute an exceptional event from the perspective of these records" (<a href="#citeproc_bib_item_52">Nye and Condron, 2021, 1414</a>).


### <span class="section-num">7.2</span> General {#general}

<a href="#citeproc_bib_item_36">Larsson <i>et al.</i>, 2022</a> use tephrochronology to try and understand the synchronicity of the Younger Dryas. By comparing European sites with Hässeldalen Tephra, the Vedde Ash and the Lacher See Tephra, we find that further investigation is required to answer the question of whether the Younger Dryas was a synchronous event or not. The answer is yet obscured by dating uncertainties and complexities regarding the interpretation of palaeoclimate proxy data, and such matters must therefore be taken into strict consideration in future studies.


### <span class="section-num">7.3</span> Onset of the Younger Dryas {#onset-of-the-younger-dryas}


#### <span class="section-num">7.3.1</span> Mechanisms of the Younger Dryas {#mechanisms-of-the-younger-dryas}


##### <span class="section-num">7.3.1.1</span> General {#general}

The paper by <a href="#citeproc_bib_item_13">Cheng <i>et al.</i>, 2020</a> has a detailed section on triggers of YD.

<a href="#citeproc_bib_item_64">Renssen, 2020</a> compared five climate models of the BA/YD/PB transitions and found that although a model with a full AMOC collapse has best agreements with some proxy data, the proxy evidence does not suggest a full shutdown. They argue that the mismatch "indicates uncertainty in the YD forcing. It is proposed that negative radiative forcing [&uarr; dust; &darr; methane and NO] could have been more important during the YD-bA climate change than assumed until now" _[17]_. In some of the experiments, cooling and winter sea-ice cover conditions were prescribed; freshwater forcing was applied in the CCSM3 experiment.


##### <span class="section-num">7.3.1.2</span> The Freshwater Forcing Hypothesis {#the-freshwater-forcing-hypothesis}


###### General North Atlantic circulation changes {#general-north-atlantic-circulation-changes}

<a href="#citeproc_bib_item_48">Muschitiello <i>et al.</i>, 2019</a> used a Nordic Sea record of deep water ventilation (difference between benthic and planktic forams \\(\ce{^{14}C}\\) ages) and found a "~400-year lead of changes in high-latitude NADW ahead of abrupt climate changes recorded in Greenland ice cores at the onset and end of the Younger Dryas stadial" <a href="#citeproc_bib_item_48">Muschitiello <i>et al.</i>, 2019</a>, abstract. They suggest that there were changes in Nordic Sea NADW formation 385+-32 years before the climate shift into GS-1 and 447+-27 before the climate shift out of GS-1. They propose a "threshold response to gradual changes in surface temperature-driven freshwater transport" <a href="#citeproc_bib_item_48">Muschitiello <i>et al.</i>, 2019, 5</a>. They also suggest the role of meltwater flux changes and atmospheric blocking. The latter would impact sea ice export and northward heat and salt transport.

<a href="#citeproc_bib_item_50">Ng <i>et al.</i>, 2018</a> use 231Pa/230Th changes to identify a reduction in Atlantic deep-water transport during the YD, but which is smaller than during HS-1. However, they suggest that there is not evidence of strong Eurasian freshwater forcing, but there is some evidence of short-lived LIS meltwater forcing.

<a href="#citeproc_bib_item_54">Partin <i>et al.</i>, 2015</a> suggest that the contemporaneous onset and termination of the YD in both Atlantic and Pacific proxies suggests and AMOC driver. However, other proxies record a gradual onset of YD climatic changes, which suggest changes in vegetation/land cover, ocean circulation or ice sheets.


###### The drainage of Lake Agassiz and the Arctic Route {#the-drainage-of-lake-agassiz-and-the-arctic-route}

<a href="#citeproc_bib_item_9">Broecker <i>et al.</i>, 1989</a> first hypothesised LIS freshwater forcing for the YD, but suggested a route through the Gulf of St Lawrence.

<a href="#citeproc_bib_item_72">Tarasov and Peltier, 2005</a> suggested an Arctic route of the freshwater forcing. They suggest that the "lack of a direct connection between the eustatic sea level record and climate response during the YD onset implies that the most important factor in determining the effect of the freshwater forcing upon the COM is not the total amount o melt water that enters the ocean basins but rather its regional distribution."

<a href="#citeproc_bib_item_47">Murton <i>et al.</i>, 2010</a> identified two glacial lake outburst flood paths from geomorphological evidence at the onset of the Younger Dryas, which followed an Arctic route. This provides evidence for the Arctic route flow hypothesis (<a href="#citeproc_bib_item_72">Tarasov and Peltier, 2005</a>). Sediment types suggest it must be glacial meltwater, not non-glacial fluvial (<a href="#citeproc_bib_item_47">Murton <i>et al.</i>, 2010</a>). The flood submerged highland areas and was likely initially affected by glacial drainage systems. The water depth was a minimum of \\(10\unit{m}\\). The retreat and re-advance of the Laurentide Ice Sheet during the Younger Dryas event lead to the second outburst flood.

<a href="#citeproc_bib_item_15">Condron and Winsor, 2012</a> use an ice-ocean model to look at the route of meltwater at the onset of the Younger Dryas. They find that meltwater from the Mackenzie valley, rather than from the St Lawrence valley (<a href="#citeproc_bib_item_9">Broecker <i>et al.</i>, 1989</a>), "disrupts the frequency and depth of open-ocean deep convection in the Greenland and Labrador Seas and significantly weakens both the AMOC and northward transport of heat to the Northern North Atlantic and Arctic Ocean". <a href="#citeproc_bib_item_15">Condron and Winsor, 2012</a> conducted two modelled experiments, with a closed and open Canadian Arctic Archipelago (CAA), to understand the impact of ice sheet blocking:

-   <span class="underline">Open CAA</span>:
    -   Meltwater doesn't enter the central Labrador Sea
    -   Little penetration into Nordic Seas
    -   40% advected eastwards toward north coast of Greenland
    -   Eastward flowing currents reach Jan Mayen and East Icelandic currents and lead to \\(\sim1\\) practical salinity units (psu) freshening
-   <span class="underline">Closed CAA</span>:
    -   "the entire meltwater flood is forced to exit the Arctic at Fram Strait" -- larger amount of water in the West Greenland Current (WGC) &rarr; "enables the boundary current eddies shedding from this current to freshen the surface of the central Labrador Sea"
    -   Greenland Sea freshening: \\(\sim2\unit{psu}\\)
    -   central Labrador Sea freshening: \\(\sim3\unit{psu}\\)

Freshwater forcing leads to AMOC slowing and a reduction in northward heat transport (<a href="#citeproc_bib_item_15">Condron and Winsor, 2012</a>):

| Meltwater Source       | AMOC Slowdown | Heat Transport |
|------------------------|---------------|----------------|
| St Lawrence Valley     | 14%           | 13%            |
| Mackenzie (Open CAA)   | 27%           | 23%            |
| Mackenzie (Closed CAA) | 32%           | 29%            |

<a href="#citeproc_bib_item_32">Keigwin <i>et al.</i>, 2018</a> found freshening of the Beaufort Sea at \\(12.94\pm0.15\unit{ka}\\), with a change in the meltwater route at 13 ka from the Gulf of Mexico to the Beaufort Sea. \\(\delta\ce{^{18}O}\\) from sediment cores shows low values (below 2 &permil;) for ~660 years, and a minimum for 130 years, suggesting that "most of the fresh-water transport could have occurred in about 130 years". They suggest that "most probably, the initial Mackenzie discharge at \\(12.9\unit{ka}\\) was a combination of both a routing change from the Gulf of Mexico and an outburst flood from the glacial Lake Agassiz" <a href="#citeproc_bib_item_32">Keigwin <i>et al.</i>, 2018, 603</a>. Furthermore, "by the onset of the YD, the AMOC may have already been close to a tipping point after ~1,500 years of low salinity leakage from the Beaufort Sea and transport to the nearshore convective regions of the Nordic Seas" <a href="#citeproc_bib_item_32">Keigwin <i>et al.</i>, 2018, 603</a>.

Challenge of uncertainty in the timing and thus difficult to attribute causal relationships _[From IB-5-11 Lecture Slides]_.

<a href="#citeproc_bib_item_77">Young <i>et al.</i>, 2021</a> provide \\(\ce{^{14}C}\\) and Luminescence dates for the phases of Lake Agassiz. They find that "the modelled onset of the Moorhead phase post-dates the initiation of YD cooling in the North Atlantic, suggesting either that Lake Agassiz drainage associated with the Moorhead Phase drawdown was not the immediate cause of the YD, or that the  chronological vetting and calibration model implemented here rejected (erroneously) radio-carbon dates for onset of the Moorhead Phase" (p. 13).

<a href="#citeproc_bib_item_51">Norris <i>et al.</i>, 2021</a> use a one-dimensional step-backwater model and a zero-dimension gradual-incision model to quantify floodwater output from glacial lakes in North America. They find that catastrophic drainage of Lake Agassiz is viable, but it may not have been near the beginning of the YD (e.g., (<a href="#citeproc_bib_item_77">Young <i>et al.</i>, 2021</a>)). Furthermore, they suggest the drainage would have lasted 3-9 months, which wouldn't have been enough for a millenial-scale disruption in AMOC, unless combined with LIS melt and precipitation runoff, which could have travelled through the Clearwater Lower Athabasca Spillway (CLAS), newly formed by the lake drainage.


###### Long-term meltwater flux {#long-term-meltwater-flux}

<a href="#citeproc_bib_item_57">Pico <i>et al.</i>, 2020</a> provide a refined geometry and timing of sea level change in the Bering Strait, to suggest that during 13 and 11.5 ka there was substantial melting of the Cordilleran and Laurentide Ice Sheets, of which some of it would have freshened the subpolar North Atlantic and could have suppressed deep water formation.


###### The drainage of the Baltic Ice Lake (BIL) {#the-drainage-of-the-baltic-ice-lake--bil}

<a href="#citeproc_bib_item_49">Muschitiello <i>et al.</i>, 2015</a> demonstrate that melting of the Fennoscandian Ice Sheet (FIS) is synchronous with GS-1 and the hydroclimate transition in Europe. They suggest that at the end of the Allerød interstadial continued mass loss of the FIS led to Nordic Sea freshening and early cooling in Northern Europe. They suggest that the mechanism is similar to that proposed by <a href="#citeproc_bib_item_17">Dokken <i>et al.</i>, 2013</a> for : "Potentially a sudden westward drainage of the Baltic Ice Lake through the south-central Swedish lowlands ... may have contributed to drive sea ice and freshwater to the western sector of the Nordic Seas" <a href="#citeproc_bib_item_49">Muschitiello <i>et al.</i>, 2015, 5</a> and a southward shift in the storm track.


##### <span class="section-num">7.3.1.3</span> Atmospheric Circulation {#atmospheric-circulation}

<a href="#citeproc_bib_item_8">Brauer <i>et al.</i>, 2008</a> identify an abrupt autumn-to-spring increase in storminess over a single year at 12,679 yr BP, which represent "an abrupt change in North Atlantic Westerlies towards a stronger and more zonal jet" (p. 520), but which cannot be fully explained by AMOC changes.

Evidence from sub-fossil kauri trees in New Zealand (<a href="#citeproc_bib_item_27">Hogg <i>et al.</i>, 2016</a>) suggests that there were two short collapses of AMOC during the YD, and therefore, "sustained North Atlantic cooling across GS-1 was not driven by a prolonged AMOC reduction but probably due to an equatorward migration of the Polar Front, reducing the advection of southwesterly air masses to high latitudes", "and hemispheric temperature trends were driven by atmospheric teleconnections" (p. 1).

<a href="#citeproc_bib_item_53">Obreht <i>et al.</i>, 2020</a> provide an annually resolved chronostratigraphy of lipid biomarkers from Meerfelder Maar over the BA/YD transition and show that Western European cooling and aridification lagged the Greenland cooling. Furthermore, they suggest a "major reorganization in the North Atlantic Westerlies amplified by the asynchronous increase in sea-ice and the polar front influence over the North Atlantic as a potential common forcing mechansm for time-transgressive Young Dryas cooling over the North Atlantic realm" (p. 13).


##### <span class="section-num">7.3.1.4</span> Circulation Changes {#circulation-changes}

A record of the \\(\ce{^{14}C}\\) interhemispheric gradient (IHG) shows an increase in the \\(\ce{^{14}C}\\) -IHG between the Allerød and the YD, which is "compatible with ocean circulation changes, namely: a decrease in deep-water convection in the North-Atlantic and an associated increase in wind-driven upwelling in the Southern Ocean" <a href="#citeproc_bib_item_11">Capano <i>et al.</i>, 2020, 915</a>.


##### <span class="section-num">7.3.1.5</span> Younger Dryas Impact Hypothesis {#younger-dryas-impact-hypothesis}

First suggested by <a href="#citeproc_bib_item_20">Firestone <i>et al.</i>, 2007</a> due to the presence of a carbon-rich layer in sediment records dated to ~12.9 ka. They suggest that the impact could lead to cooling due to a combination of short- and long-term effects:

-   Short term:
    a) Ozone depletion
    b) Sulfates, dust, etc. &rarr; solar blocking
    c) Water vapour and ice injection &rarr; cloudiness and noctilucent clouds &rarr; solar blocking
-   Long term
    -   Feedbacks -- e.g., (c) -- high lat cooling and &uarr; snow accumulation
    -   (partial) destabilisation/melting of the ice sheet :
        -   &rarr; short term meltwater and IRD &rarr; &darr; ocean salinity &rarr; surface coolign
        -   THC weakening over long period

<a href="#citeproc_bib_item_70">Sweatman, 2021</a> reviewed the evidence of the YDIH, the original claim by <a href="#citeproc_bib_item_20">Firestone <i>et al.</i>, 2007</a> and the evidence proposed against the YDIH. They argue that since <a href="#citeproc_bib_item_20">Firestone <i>et al.</i>, 2007</a> suggest a 'low-density fragment', it would not form a crater as it would burn up in the atmosphere; furthermore, as the impact would've been concentrated in the LIS, it is even less likely that there would be a crater. There have been over 100 'black mats' discovered across N.Am. <a href="#citeproc_bib_item_70">Sweatman, 2021</a> concludes that there is sufficient evidence for the YDIH, and all but one paper that opposes this hypothesis actually contains evidence supporting it, and fails to objectively consider the hypothesis, evidence and the uncertainties. <a href="#citeproc_bib_item_70">Sweatman, 2021, 20</a> argues that "with the YD impact event essentially confirmed, the YD impact hypothesis should be called a 'theory'"

<a href="#citeproc_bib_item_59">Powell, 2022</a> -- false rejection of the hypothesis: "In this author's opinion, there is a strong case that it [YDIH] does [meet the definition of a theory]. Moreover, it should not be forgotten that no other single theory can explain the YD and its associated effects" (p. 37), supporting the argument of <a href="#citeproc_bib_item_70">Sweatman, 2021</a>, above.

<a href="#citeproc_bib_item_46">Moore <i>et al.</i>, 2020</a> analyse meltglass from Abu Hureya, Syria from a excavation site, in a layer dated to 12825 +- 55 cal yr BP. They conclude that there is evidence here of an extraterrestrial formation of this meltglass, which supports the YDIH.

<a href="#citeproc_bib_item_28">Holliday <i>et al.</i>, 2020</a> refutes the YDIH and disagrees with much of the evidence provided in Wolbach _et al._ (2018). However, <a href="#citeproc_bib_item_76">Wolbach <i>et al.</i>, 2020</a> refute the arguments proposed by <a href="#citeproc_bib_item_28">Holliday <i>et al.</i>, 2020</a>.

<a href="#citeproc_bib_item_13">Cheng <i>et al.</i>, 2020, 23414</a> argue that "the YD impact hypothesis remains untenable and offers a less parsimonous explanation for the global timing and structure of the YD event", particularly as there is a similar YD-like excusrion during glacial termination-III.


##### <span class="section-num">7.3.1.6</span> Combination of Mechanisms {#combination-of-mechanisms}

<a href="#citeproc_bib_item_65">Renssen <i>et al.</i>, 2015</a> "conclude that the YD was most likely caused by a combination of sustained, severe AMOC weakening, anomalous atmospheric northerly flow over Europe, and moderate radiative cooling related to an enhanced atmospheric dust load and/or reduced atmospheric methane and nitrous oxide levels" (p. 949) and highlight that a full AMOC collapse is implausible.


##### <span class="section-num">7.3.1.7</span> Volcanic Forcing {#volcanic-forcing}

<a href="#citeproc_bib_item_1">Abbott <i>et al.</i>, 2021</a> find 30 volcanic eruptions including four bipolar major eruptions between 12,980 and 12,870 yrs BP (GICC05), with the overall volcanic forcing grater than that associated with cooling during the Common Era. They argue that "the magnitude and persistence of volcanic forcing directly preceding large scale climatic cooling need to be considered when exploring the mechanisms triggering abrupt cooling events during times of metastable AMOC conditions" <a href="#citeproc_bib_item_1">Abbott <i>et al.</i>, 2021, 7</a>. They suggest that "volcanic forcing may have acted as a trigger for the subsequent coolign during a period when the AMOC was more sensitive to external disturbance than during the Common Era" and suggest that this could be tested in models <a href="#citeproc_bib_item_1">Abbott <i>et al.</i>, 2021, 5</a>. Volcanic forcing, although likely to have had an influence on the YD onset, it would not have been able to be the sole mechanism, due to the long-scale transition and period of cooling (<a href="#citeproc_bib_item_1">Abbott <i>et al.</i>, 2021</a>)


### <span class="section-num">7.4</span> Termination of the Younger Dryas {#termination-of-the-younger-dryas}

<a href="#citeproc_bib_item_55">Pearce <i>et al.</i>, 2013</a> provide evidence that ocean circulation changes lead the termination of the Younger Dryas. At the mid-to-end of the YD, 12.3--11.7 kyrs BP, there was a gradual decrease in the influence of the Labrador Current and the oceanic front at the Labrador Current-Greenland Sea migrated northwards. This is likely linked to increased AMOC strength and created increasingly unstable sea ice conditions. "Just before ... the YD, our record shows a rapid loss of sea-ice cover and an inreased influx of warmer GS source waters, centuries after the initial decline in LC influence" (<a href="#citeproc_bib_item_55">Pearce <i>et al.</i>, 2013, 3</a>). This oceanic circulation changes and western North Atlantic warming occured before the atmospheric changes recorded by the Greenland Ice Sheet and the Cariaco Basin. <a href="#citeproc_bib_item_55">Pearce <i>et al.</i>, 2013</a> suggest that the oceanic circulation changes could be linked to tropics and southern hemisphere changes, and could have triggered the ITCZ and polar front shifts.

<a href="#citeproc_bib_item_13">Cheng <i>et al.</i>, 2020</a> suggest that the termination of the YD started in Antarctica at 11,900 BP, "or even earlier in the western tropical Pacific, followed by the North Atlantic" (p. 23408). "The underlying climate dynamics during the YD termination manifested through an SH and or tropics to northern high-latitude directionality" <a href="#citeproc_bib_item_13">Cheng <i>et al.</i>, 2020, 23414</a>. This could happen through interhemispheric heat transport, or through direct radiative forcing due to the \\(15\unit{ppm} \ce{CO2}\\) increase, which may have led to a threshold change in Central American atmospheric moisture transport, thus affecting the North Atlantic freshwater budget and causing a weak-to-strong AMOC transition.


### <span class="section-num">7.5</span> Records of the Younger Dryas {#records-of-the-younger-dryas}


### <span class="section-num">7.6</span> Mid-YD Transition {#mid-yd-transition}

Using synchronisation of timescales and the Vedde Ash later, <a href="#citeproc_bib_item_35">Lane <i>et al.</i>, 2013</a> find that the mid-YD climatic amelioration was "locally-abrupt but time-transgressive across Europe". They suggest that the lack of change in Greenland \\(\delta\ce{^{18}O}\\) record during GS-1 is explained by the fact that it was north of the polar front, and therefore does not record the Polar Front changes that drove the transition in Europe.

<a href="#citeproc_bib_item_4">Bartolomé <i>et al.</i>, 2015</a> use a stalagmite record from Seso Cave in the Central Pyrenees, which is a direct temperature and moisture record (along with 17 U/Th dates) to find a "time-transgressive transition [that] took about 350 y from its record in Seso Cave to influence Northern European regions" (p. 6571) and "conclude that the resumption of the Atlantic overturning circulation is the main mechanism behind the hydrological response in Europe during this mid-GS-1 transitions" (p. 6571).

<a href="#citeproc_bib_item_58">Pigati and Springer, 2022</a> "present hydroclimate data from paleospring deposits in Death Valley National Park (California, USA) that demonstrate unequivocal evidence of two-stage partitioning within the YD event. High groundwater levels supported persistent and long-lived spring ecosystems between ~13.0 and 12.2 ka, which were immediately replaced by alternating wet and dry environments until ~11.8 ka. These results establish the mid-YD climate transition extended into western North America at approximately the same time it was recorded by hydrologic systems elsewhere in the Northern Hemisphere and show that even short-lived changes in the AMOC can have profound consequences for ecosystems worldwide." (abstract) "The Death Valley record also demonstrates that teleconnections between the North Atlantic and southwestern U.S. are strong enough that even short-lived changes in the AMOC can dramatically affect ecosystems in the American Southwest" (conclusion)


## <span class="section-num">8</span> Other (TEMP) {#other--temp}

The Asian monsoon (AM) is strongly correlated with Precession (anti-phase) and obliquity (weaker,anti-phase) and the AM and Antarctic tempterature are anti-phased, which is attributed to the bi-polar see-saw (<a href="#citeproc_bib_item_12">Cheng <i>et al.</i>, 2016</a>). Antarctic temperatures show nearly in-phase spectral power with precession and obliquity.

"Without accurate time control, paleoclimate observations and interpretations remain ambiguous or circular, limiting our ability to infer past mechanisms of climate change" -- this means that we need an astronomical tuning-indepdent time scale (<a href="#citeproc_bib_item_26">Hodell, 2016</a>). [Maybe in a separate doc called 'Paleoclimate'?]


### <span class="section-num">8.1</span> The LGIT {#the-lgit}

-   Heiri _et al._ (2007) -- The **Tripartite Sequence**: Allerod, YD and Holocene | Light &rarr; dark &rarr; light &rarr; dark sediment
-   Bjork _et al._ (1998) -- LGIT records across North Atlantic


### <span class="section-num">8.2</span> Scottish Lateglacial type-site {#scottish-lateglacial-type-site}

-   Walker and Lowe (2019)
-   Whitrig Bog, Scotland
-   Brooks _et al._ (1997) - Vedde Ash and Borrobol Tephra
-   Brooks _et al._ (2016) -- Multi-lake correlation across Scotland, Ireland and Northern England using Vedde Ash, Penifiler Tephra and Borrobol Tephra


## <span class="section-num">9</span> Bibliography and acknowledgements {#bibliography-and-acknowledgements}

For some of the information in this document, I am grateful to the lectures by Professor Christine Lane and Professor Francesco Muschietello during part IB (2021-22) of my Cambridge Geography Degree.

The following bibliography only contains the references for the articles I have read, not those borrowed from the lectures.

<style>.csl-entry{text-indent: -1.5em; margin-left: 1.5em;}</style><div class="csl-bib-body">
  <div class="csl-entry"><a id="citeproc_bib_item_1"></a>Abbott, P., Niemeier, U., Timmreck, C., Riede, F., McConnell, J., Severi, M., Fischer, H., <i>et al.</i> (2021) “Volcanic Climate Forcing Preceding the Inception of the Younger Dryas: Implications for Tracing the Laacher See Eruption”. <i>Quaternary Science Reviews</i> [online] 274, 107260. doi: <a href="https://doi.org/10.1016/j.quascirev.2021.107260">10.1016/j.quascirev.2021.107260</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_2"></a>Abbott, P.M. and Davies, S.M. (2012) “Volcanism and the Greenland Ice-Cores: The Tephra Record”. <i>Earth-Science Reviews</i> [online] 115 (3), 173–191. doi: <a href="https://doi.org/10.1016/j.earscirev.2012.09.001">10.1016/j.earscirev.2012.09.001</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_3"></a>Bartoli, G., Hönisch, B., and Zeebe, R.E. (2011) “Atmospheric CO 2 Decline during the Pliocene Intensification of Northern Hemisphere Glaciations”. <i>Paleoceanography</i> [online] 26 (4), 2010PA002055. doi: <a href="https://doi.org/10.1029/2010PA002055">10.1029/2010PA002055</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_4"></a>Bartolomé, M., Moreno, A., Sancho, C., Stoll, H.M., Cacho, I., Spötl, C., Belmonte, Á., <i>et al.</i> (2015) “Hydrological Change in Southern Europe Responding to Increasing North Atlantic Overturning during Greenland Stadial 1”. <i>Proceedings of the National Academy of Sciences</i> [online] 112 (21), 6568–6572. doi: <a href="https://doi.org/10.1073/pnas.1503990112">10.1073/pnas.1503990112</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_5"></a>Bassis, J.N., Petersen, S.V., and Mac Cathles, L. (2017) “Heinrich Events Triggered by Ocean Forcing and Modulated by Isostatic Adjustment”. <i>Nature</i> [online] 542 (7641), 332–334. doi: <a href="https://doi.org/10.1038/nature21069">10.1038/nature21069</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_6"></a>Berben, S.M., Dokken, T.M., Abbott, P.M., Cook, E., Sadatzki, H., Simon, M.H., and Jansen, E. (2020) “Independent Tephrochronological Evidence for Rapid and Synchronous Oceanic and Atmospheric Temperature Rises over the Greenland Stadial-Interstadial Transitions between ca. 32 and 40 Ka B2k”. <i>Quaternary Science Reviews</i> [online] 236, 1–25. doi: <a href="https://doi.org/10.1016/j.quascirev.2020.106277">10.1016/j.quascirev.2020.106277</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_7"></a>van der Bilt, W.G.M. and Lane, C.S. (2019) “Lake Sediments with Azorean Tephra Reveal Ice-Free Conditions on Coastal Northwest Spitsbergen during the Last Glacial Maximum”. <i>Science Advances</i> [online] 5 (10), 1–7. doi: <a href="https://doi.org/10.1126/sciadv.aaw5980">10.1126/sciadv.aaw5980</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_8"></a>Brauer, A., Haug, G.H., Dulski, P., Sigman, D.M., and Negendank, J.F.W. (2008) “An Abrupt Wind Shift in Western Europe at the Onset of the Younger Dryas Cold Period”. <i>Nature Geoscience</i> [online] 1 (8), 520–523. doi: <a href="https://doi.org/10.1038/ngeo263">10.1038/ngeo263</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_9"></a>Broecker, W.S., Kennett, J.P., Flower, B.P., Teller, J.T., Trumbore, S., Bonani, G., and Wolfli, W. (1989) “Routing of Meltwater from the Laurentide Ice Sheet during the Younger Dryas Cold Episode”. <i>Nature</i> [online] 341 (6240), 318–321. doi: <a href="https://doi.org/10.1038/341318a0">10.1038/341318a0</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_10"></a>Büntgen, U., Myglan, V.S., Ljungqvist, F.C., McCormick, M., Di Cosmo, N., Sigl, M., Jungclaus, J., <i>et al.</i> (2016) “Cooling and Societal Change during the Late Antique Little Ice Age from 536 to around 660 AD”. <i>Nature Geoscience</i> [online] 9 (3), 231–236. doi: <a href="https://doi.org/10.1038/ngeo2652">10.1038/ngeo2652</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_11"></a>Capano, M., Miramont, C., Shindo, L., Guibal, F., Marschal, C., Kromer, B., Tuna, T., <i>et al.</i> (2020) “Onset of the Younger Dryas Recorded with 14C at Annual Resolution in French Subfossil Trees”. <i>Radiocarbon</i> [online] 62 (4), 901–918. doi: <a href="https://doi.org/10.1017/RDC.2019.116">10.1017/RDC.2019.116</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_12"></a>Cheng, H., Edwards, R.L., Sinha, A., Spötl, C., Yi, L., Chen, S., Kelly, M., <i>et al.</i> (2016) “The Asian Monsoon over the Past 640,000 Years and Ice Age Terminations”. <i>Nature</i> [online] 534 (7609), 640–646. doi: <a href="https://doi.org/10.1038/nature18591">10.1038/nature18591</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_13"></a>Cheng, H., Zhang, H., Spötl, C., Baker, J., Sinha, A., Li, H., Bartolomé, M., <i>et al.</i> (2020) “Timing and Structure of the Younger Dryas Event and Its Underlying Climate Dynamics”. <i>Proceedings of the National Academy of Sciences</i> [online] 117 (38), 23408–23417. doi: <a href="https://doi.org/10.1073/pnas.2007869117">10.1073/pnas.2007869117</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_14"></a>Claussen, M., Kubatzki, C., Brovkin, V., Ganopolski, A., Hoelzmann, P., and Pachur, H.-J. (1999) “Simulation of an Abrupt Change in Saharan Vegetation in the Mid-Holocene”. <i>Geophysical Research Letters</i> [online] 26 (14), 2037–2040. doi: <a href="https://doi.org/10.1029/1999GL900494">10.1029/1999GL900494</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_15"></a>Condron, A. and Winsor, P. (2012) “Meltwater Routing and the Younger Dryas”. <i>Proceedings of the National Academy of Sciences</i> [online] 109 (49), 19928–19933. doi: <a href="https://doi.org/10.1073/pnas.1207381109">10.1073/pnas.1207381109</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_16"></a>Davies, S.M. (2015) “Cryptotephras: The Revolution in Correlation and Precision Dating”. <i>Journal of Quaternary Science</i> [online] 30 (2), 114–130. doi: <a href="https://doi.org/10.1002/jqs.2766">10.1002/jqs.2766</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_17"></a>Dokken, T.M., Nisancioglu, K.H., Li, C., Battisti, D.S., and Kissel, C. (2013) “Dansgaard-Oeschger Cycles: Interactions between Ocean and Sea Ice Intrinsic to the Nordic Seas”. <i>Paleoceanography</i> [online] 28 (3), 491–502. doi: <a href="https://doi.org/10.1002/palo.20042">10.1002/palo.20042</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_18"></a>Elderfield, H., Ferretti, P., Greaves, M., Crowhurst, S., McCave, I.N., Hodell, D., and Piotrowski, A.M. (2012) “Evolution of Ocean Temperature and Ice Volume Through the Mid-Pleistocene Climate Transition”. <i>Science</i> [online] 337 (6095), 704–709. doi: <a href="https://doi.org/10.1126/science.1221294">10.1126/science.1221294</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_19"></a>Fedorov, A.V., Brierley, C.M., Lawrence, K.T., Liu, Z., Dekens, P.S., and Ravelo, A.C. (2013) “Patterns and Mechanisms of Early Pliocene Warmth”. <i>Nature</i> [online] 496 (7443), 43–49. doi: <a href="https://doi.org/10.1038/nature12003">10.1038/nature12003</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_20"></a>Firestone, R.B., West, A., Kennett, J.P., Becker, L., Bunch, T.E., Revay, Z.S., Schultz, P.H., <i>et al.</i> (2007) “Evidence for an Extraterrestrial Impact 12,900 Years Ago That Contributed to the Megafaunal Extinctions and the Younger Dryas Cooling”. <i>Proceedings of the National Academy of Sciences</i> [online] 104 (41), 16016–16021. doi: <a href="https://doi.org/10.1073/pnas.0706977104">10.1073/pnas.0706977104</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_21"></a>van Geel, B., Heijnis, H., Charman, D.J., Thompson, G., and Engels, S. (2014) “Bog Burst in the Eastern Netherlands Triggered by the 2.8 Kyr BP Climate Event”. <i>The Holocene</i> [online] 24 (11), 1465–1477. doi: <a href="https://doi.org/10.1177/0959683614544066">10.1177/0959683614544066</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_22"></a>Haug, G.H. and Tiedemann, R. (1998) “Effect of the Formation of the Isthmus of Panama on Atlantic Ocean Thermohaline Circulation”. <i>Nature</i> [online] 393 (6686), 673–676. doi: <a href="https://doi.org/10.1038/31447">10.1038/31447</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_23"></a>Hays, J.D., Imbrie, J., and Shackleton, N.J. (1976) “Variations in the Earth’s Orbit: Pacemaker of the Ice Ages”. <i>Science</i> [online] 194 (4270), 1121–1132. doi: <a href="https://doi.org/10.1126/science.194.4270.1121">10.1126/science.194.4270.1121</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_24"></a>Hemming, S.R. (2004) “Heinrich Events: Massive Late Pleistocene Detritus Layers of the North Atlantic and Their Global Climate Imprint”. <i>Reviews of Geophysics</i> [online] 42 (1). doi: <a href="https://doi.org/10.1029/2003RG000128">10.1029/2003RG000128</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_25"></a>Henry, L.G., McManus, J.F., Curry, W.B., Roberts, N.L., Piotrowski, A.M., and Keigwin, L.D. (2016) “North Atlantic Ocean Circulation and Abrupt Climate Change during the Last Glaciation”. <i>Science</i> [online] 353 (6298), 470–474. doi: <a href="https://doi.org/10.1126/science.aaf5529">10.1126/science.aaf5529</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_26"></a>Hodell, D.A. (2016) “The Smoking Gun of the Ice Ages”. <i>Science</i> [online] 354 (6317), 1235–1236. doi: <a href="https://doi.org/10.1126/science.aal4111">10.1126/science.aal4111</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_27"></a>Hogg, A., Southon, J., Turney, C., Palmer, J., Bronk Ramsey, C., Fenwick, P., Boswijk, G., <i>et al.</i> (2016) “Punctuated Shutdown of Atlantic Meridional Overturning Circulation during Greenland Stadial 1”. <i>Scientific Reports</i> [online] 6 (1), 25902. doi: <a href="https://doi.org/10.1038/srep25902">10.1038/srep25902</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_28"></a>Holliday, V.T., Bartlein, P.J., Scott, A.C., and Marlon, J.R. (2020) “Extraordinary Biomass-Burning Episode and Impact Winter Triggered by the Younger Dryas Cosmic Impact ∼12,800 Years Ago, Parts 1 and 2: A Discussion”. <i>The Journal of Geology</i> [online] 128 (1), 69–94. doi: <a href="https://doi.org/10.1086/706264">10.1086/706264</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_29"></a>Huybers, P. (2011) “Combined Obliquity and Precession Pacing of Late Pleistocene Deglaciations”. <i>Nature</i> [online] 480 (7376), 229–232. doi: <a href="https://doi.org/10.1038/nature10626">10.1038/nature10626</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_30"></a>Huybers, P. and Langmuir, C. (2009) “Feedback between Deglaciation, Volcanism, and Atmospheric CO2”. <i>Earth and Planetary Science Letters</i> [online] 286 (3-4), 479–491. doi: <a href="https://doi.org/10.1016/j.epsl.2009.07.014">10.1016/j.epsl.2009.07.014</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_31"></a>Huybers, P. and Langmuir, C.H. (2017) “Delayed CO2 Emissions from Mid-Ocean Ridge Volcanism as a Possible Cause of Late-Pleistocene Glacial Cycles”. <i>Earth and Planetary Science Letters</i> [online] 457, 238–249. doi: <a href="https://doi.org/10.1016/j.epsl.2016.09.021">10.1016/j.epsl.2016.09.021</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_32"></a>Keigwin, L.D., Klotsko, S., Zhao, N., Reilly, B., Giosan, L., and Driscoll, N.W. (2018) “Deglacial Floods in the Beaufort Sea Preceded Younger Dryas Cooling”. <i>Nature Geoscience</i> [online] 11 (8), 599–604. doi: <a href="https://doi.org/10.1038/s41561-018-0169-6">10.1038/s41561-018-0169-6</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_33"></a>Kilian, M., Van der Plicht, J., and Van Geel, B. (1995) “Dating Raised Bogs: New Aspects of AMS 14C Wiggle Matching, a Reservoir Effect and Climatic Change”. <i>Quaternary Science Reviews</i> [online] 14 (10), 959–966. doi: <a href="https://doi.org/10.1016/0277-3791(95)00081-X">10.1016/0277-3791(95)00081-X</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_34"></a>Lamentowicz, M., Gałka, M., Lamentowicz, Ł., Obremska, M., Kühl, N., Lücke, A., and Jassey, V. (2015) “Reconstructing Climate Change and Ombrotrophic Bog Development during the Last 4000years in Northern Poland Using Biotic Proxies, Stable Isotopes and Trait-Based Approach”. <i>Palaeogeography, Palaeoclimatology, Palaeoecology</i> [online] 418, 261–277. doi: <a href="https://doi.org/10.1016/j.palaeo.2014.11.015">10.1016/j.palaeo.2014.11.015</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_35"></a>Lane, C.S., Chorn, B.T., and Johnson, T.C. (2013) “Ash from the Toba Supereruption in Lake Malawi Shows No Volcanic Winter in East Africa at 75 Ka”. <i>Proceedings of the National Academy of Sciences</i> [online] 110 (20), 8025–8029. doi: <a href="https://doi.org/10.1073/pnas.1301474110">10.1073/pnas.1301474110</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_36"></a>Larsson, S.A., Kylander, M.E., Sannel, A.B.K., and Hammarlund, D. (2022) “Synchronous or Not? The Timing of the Younger Dryas and Greenland Stadial-1 Reviewed Using Tephrochronology”. <i>Quaternary</i> [online] 5 (2), 19. doi: <a href="https://doi.org/10.3390/quat5020019">10.3390/quat5020019</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_37"></a>Li, C. and Born, A. (2019) “Coupled Atmosphere-Ice-Ocean Dynamics in Dansgaard-Oeschger Events”. <i>Quaternary Science Reviews</i> [online] 203, 1–20. doi: <a href="https://doi.org/10.1016/j.quascirev.2018.10.031">10.1016/j.quascirev.2018.10.031</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_38"></a>Lisiecki, L.E. and Raymo, M.E. (2005) “A Pliocene-Pleistocene Stack of 57 Globally Distributed Benthic Δ 18 O Records: PLIOCENE-PLEISTOCENE BENTHIC STACK”. <i>Paleoceanography</i> [online] 20 (1), n/a-n/a. doi: <a href="https://doi.org/10.1029/2004PA001071">10.1029/2004PA001071</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_39"></a>Lohmann, J. and Svensson, A. (2022) <i>Ice Core Evidence for Major Volcanic Eruptions at the Onset of Dansgaard-Oeschger Warming Events</i> [online] preprint. Feedback and Forcing/Ice Cores/Millenial/D-O. doi: <a href="https://doi.org/10.5194/cp-2022-1">10.5194/cp-2022-1</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_40"></a>Lowe, D.J. (2011) “Tephrochronology and Its Application: A Review”. <i>Quaternary Geochronology</i> [online] 6 (2), 107–153. doi: <a href="https://doi.org/10.1016/j.quageo.2010.08.003">10.1016/j.quageo.2010.08.003</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_41"></a>Lowe, J.J., Ramsey, C.B., Housley, R.A., Lane, C.S., and Tomlinson, E.L. (2015) “The RESET Project: Constructing a European Tephra Lattice for Refined Synchronisation of Environmental and Archaeological Events during the Last c. 100 Ka”. <i>Quaternary Science Reviews</i> [online] 118, 1–17. doi: <a href="https://doi.org/10.1016/j.quascirev.2015.04.006">10.1016/j.quascirev.2015.04.006</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_42"></a>Martin-Puertas, C., Matthes, K., Brauer, A., Muscheler, R., Hansen, F., Petrick, C., Aldahan, A., <i>et al.</i> (2012) “Regional Atmospheric Circulation Shifts Induced by a Grand Solar Minimum”. <i>Nature Geoscience</i> [online] 5 (6), 397–401. doi: <a href="https://doi.org/10.1038/ngeo1460">10.1038/ngeo1460</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_43"></a>Maslin, M.A. and Brierley, C.M. (2015) “The Role of Orbital Forcing in the Early Middle Pleistocene Transition”. <i>Quaternary International</i> [online] 389, 47–55. doi: <a href="https://doi.org/10.1016/j.quaint.2015.01.047">10.1016/j.quaint.2015.01.047</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_44"></a>Mauquoy, D., van Geel, B., Blaauw, M., Speranza, A., and vander Plicht, J. (2004) “Changes in Solar Activity and Holocene Climatic Shifts Derived from 14C Wiggle-Match Dated Peat Deposits”. <i>The Holocene</i> [online] 14 (1), 45–52. doi: <a href="https://doi.org/10.1191/0959683604hl688rp">10.1191/0959683604hl688rp</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_45"></a>Mighall, T., Martínez Cortizas, A., Biester, H., and Turner, S. (2006) “Proxy Climate and Vegetation Changes during the Last Five Millennia in NW Iberia: Pollen and Non-Pollen Palynomorph Data from Two Ombrotrophic Peat Bogs in the North Western Iberian Peninsula”. <i>Review of Palaeobotany and Palynology</i> [online] 141 (1-2), 203–223. doi: <a href="https://doi.org/10.1016/j.revpalbo.2006.03.013">10.1016/j.revpalbo.2006.03.013</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_46"></a>Moore, A.M.T., Kennett, J.P., Napier, W.M., Bunch, T.E., Weaver, J.C., LeCompte, M., Adedeji, A.V., <i>et al.</i> (2020) “Evidence of Cosmic Impact at Abu Hureyra, Syria at the Younger Dryas Onset ( 12.8 Ka): High-temperature Melting at $&#62;$2200 °C”. <i>Scientific Reports</i> [online] 10 (1), 4185. doi: <a href="https://doi.org/10.1038/s41598-020-60867-w">10.1038/s41598-020-60867-w</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_47"></a>Murton, J.B., Bateman, M.D., Dallimore, S.R., Teller, J.T., and Yang, Z. (2010) “Identification of Younger Dryas Outburst Flood Path from Lake Agassiz to the Arctic Ocean”. <i>Nature</i> [online] 464 (7289), 740–743. doi: <a href="https://doi.org/10.1038/nature08954">10.1038/nature08954</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_48"></a>Muschitiello, F., D’Andrea, W.J., Schmittner, A., Heaton, T.J., Balascio, N.L., deRoberts, N., Caffee, M.W., <i>et al.</i> (2019) “Deep-Water Circulation Changes Lead North Atlantic Climate during Deglaciation”. <i>Nature Communications</i> [online] 10 (1), 1272. doi: <a href="https://doi.org/10.1038/s41467-019-09237-3">10.1038/s41467-019-09237-3</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_49"></a>Muschitiello, F., Pausata, F.S.R., Watson, J.E., Smittenberg, R.H., Salih, A.A.M., Brooks, S.J., Whitehouse, N.J., <i>et al.</i> (2015) “Fennoscandian Freshwater Control on Greenland Hydroclimate Shifts at the Onset of the Younger Dryas”. <i>Nature Communications</i> [online] 6 (1), 8939. doi: <a href="https://doi.org/10.1038/ncomms9939">10.1038/ncomms9939</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_50"></a>Ng, H.C., Robinson, L.F., McManus, J.F., Mohamed, K.J., Jacobel, A.W., Ivanovic, R.F., Gregoire, L.J., <i>et al.</i> (2018) “Coherent Deglacial Changes in Western Atlantic Ocean Circulation”. <i>Nature Communications</i> [online] 9 (1), 2947. doi: <a href="https://doi.org/10.1038/s41467-018-05312-3">10.1038/s41467-018-05312-3</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_51"></a>Norris, S.L., Garcia‐Castellanos D., Jansen, J.D., Carling, P.A., Margold, M., Woywitka, R.J., and Froese, D.G. (2021) “Catastrophic Drainage From the Northwestern Outlet of Glacial Lake Agassiz During the Younger Dryas”. <i>Geophysical Research Letters</i> [online] 48 (15). doi: <a href="https://doi.org/10.1029/2021GL093919">10.1029/2021GL093919</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_52"></a>Nye, H. and Condron, A. (2021) “Assessing the Statistical Uniqueness of the Younger Dryas: A Robust Multivariate Analysis”. <i>Climate of the Past</i> [online] 17 (3), 1409–1421. doi: <a href="https://doi.org/10.5194/cp-17-1409-2021">10.5194/cp-17-1409-2021</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_53"></a>Obreht, I., Wörmer, L., Brauer, A., Wendt, J., Alfken, S., De Vleeschouwer, D., Elvert, M., <i>et al.</i> (2020) “An Annually Resolved Record of Western European Vegetation Response to Younger Dryas Cooling”. <i>Quaternary Science Reviews</i> [online] 231, 106198. doi: <a href="https://doi.org/10.1016/j.quascirev.2020.106198">10.1016/j.quascirev.2020.106198</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_54"></a>Partin, J., Quinn, T., Shen, C.-C., Okumura, Y., Cardenas, M., Siringan, F., Banner, J., <i>et al.</i> (2015) “Gradual Onset and Recovery of the Younger Dryas Abrupt Climate Event in the Tropics”. <i>Nature Communications</i> [online] 6 (1), 8061. doi: <a href="https://doi.org/10.1038/ncomms9061">10.1038/ncomms9061</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_55"></a>Pearce, C., Seidenkrantz, M.-S., Kuijpers, A., Massé, G., Reynisson, N.F., and Kristiansen, S.M. (2013) “Ocean Lead at the Termination of the Younger Dryas Cold Spell”. <i>Nature Communications</i> [online] 4 (1), 1664. doi: <a href="https://doi.org/10.1038/ncomms2686">10.1038/ncomms2686</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_56"></a>Peltier, W.R. and Vettoretti, G. (2014) “Dansgaard-Oeschger Oscillations Predicted in a Comprehensive Model of Glacial Climate: A ‘Kicked’ Salt Oscillator in the Atlantic”. <i>Geophysical Research Letters</i> [online] 41 (20), 7306–7313. doi: <a href="https://doi.org/10.1002/2014GL061413">10.1002/2014GL061413</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_57"></a>Pico, T., Mitrovica, J.X., and Mix, A.C. (2020) “Sea Level Fingerprinting of the Bering Strait Flooding History Detects the Source of the Younger Dryas Climate Event”. <i>Science Advances</i> [online] 6 (9), eaay2935. doi: <a href="https://doi.org/10.1126/sciadv.aay2935">10.1126/sciadv.aay2935</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_58"></a>Pigati, J.S. and Springer, K.B. (2022) “Hydroclimate Response of Spring Ecosystems to a Two-Stage Younger Dryas Event in Western North America”. <i>Scientific Reports</i> [online] 12 (1), 7323. doi: <a href="https://doi.org/10.1038/s41598-022-11377-4">10.1038/s41598-022-11377-4</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_59"></a>Powell, J.L. (2022) “Premature Rejection in Science: The Case of the Younger Dryas Impact Hypothesis”. <i>Science Progress</i> [online] 105 (1), 003685042110642. doi: <a href="https://doi.org/10.1177/00368504211064272">10.1177/00368504211064272</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_60"></a>Rach, O., Engels, S., Kahmen, A., Brauer, A., Martín-Puertas, C., van Geel, B., and Sachse, D. (2017) “Hydrological and Ecological Changes in Western Europe between 3200 and 2000 Years BP Derived from Lipid Biomarker δD Values in Lake Meerfelder Maar Sediments”. <i>Quaternary Science Reviews</i> [online] 172, 44–54. doi: <a href="https://doi.org/10.1016/j.quascirev.2017.07.019">10.1016/j.quascirev.2017.07.019</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_61"></a>Rasmussen, S.O., Andersen, K.K., Svensson, A.M., Steffensen, J.P., Vinther, B.M., Clausen, H.B., Siggaard-Andersen, M.-L., <i>et al.</i> (2006) “A New Greenland Ice Core Chronology for the Last Glacial Termination”. <i>Journal of Geophysical Research</i> [online] 111 (D06102). doi: <a href="https://doi.org/10.1029/2005JD006079">10.1029/2005JD006079</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_62"></a>Raymo, M.E. (1997) “The Timing of Major Climate Terminations”. <i>Paleoceanography</i> [online] 12 (4), 577–585. doi: <a href="https://doi.org/10.1029/97PA01169">10.1029/97PA01169</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_63"></a>Raymo, M.E. and Ruddiman, W.F. (1992) “Tectonic Forcing of Late Cenozoic Climate”. <i>Nature</i> [online] 359 (6391), 117–122. doi: <a href="https://doi.org/10.1038/359117a0">10.1038/359117a0</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_64"></a>Renssen, H. (2020) “Comparison of Climate Model Simulations of the Younger Dryas Cold Event”. <i>Quaternary</i> [online] 3 (4), 29. doi: <a href="https://doi.org/10.3390/quat3040029">10.3390/quat3040029</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_65"></a>Renssen, H., Mairesse, A., Goosse, H., Mathiot, P., Heiri, O., Roche, D.M., Nisancioglu, K.H., <i>et al.</i> (2015) “Multiple Causes of the Younger Dryas Cold Period”. <i>Nature Geoscience</i> 8, 946–950</div>
  <div class="csl-entry"><a id="citeproc_bib_item_66"></a>Sanchez Goñi, M.F. and Harrison, S.P. (2010) “Millennial-Scale Climate Variability and Vegetation Changes during the Last Glacial: Concepts and Terminology”. <i>Quaternary Science Reviews</i> [online] 29 (21-22), 2823–2827. doi: <a href="https://doi.org/10.1016/j.quascirev.2009.11.014">10.1016/j.quascirev.2009.11.014</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_67"></a>Seidov, D. and Maslin, M. (2001) “Atlantic Ocean Heat Piracy and the Bipolar Climate See-Saw during Heinrich and Dansgaard-Oeschger Events”. <i>Journal of Quaternary Science</i> [online] 16 (4), 321–328. doi: <a href="https://doi.org/10.1002/jqs.595">10.1002/jqs.595</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_68"></a>Shackleton, N. (1987) “Oxygen Isotopes, Ice Volume and Sea Level”. <i>Quaternary Science Reviews</i> [online] 6 (3-4), 183–190. doi: <a href="https://doi.org/10.1016/0277-3791(87)90003-5">10.1016/0277-3791(87)90003-5</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_69"></a>Shakun, J.D. and Carlson, A.E. (2010) “A Global Perspective on Last Glacial Maximum to Holocene Climate Change”. <i>Quaternary Science Reviews</i> [online] 29 (15), 1801–1816. doi: <a href="https://doi.org/10.1016/j.quascirev.2010.03.016">10.1016/j.quascirev.2010.03.016</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_70"></a>Sweatman, M.B. (2021) “The Younger Dryas Impact Hypothesis: Review of the Impact Evidence”. <i>Earth-Science Reviews</i> [online] 218, 103677. doi: <a href="https://doi.org/10.1016/j.earscirev.2021.103677">10.1016/j.earscirev.2021.103677</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_71"></a>Swindles, G.T., Plunkett, G., and Roe, H.M. (2007) “A Multiproxy Climate Record from a Raised Bog in County Fermanagh, Northern Ireland: A Critical Examination of the Link between Bog Surface Wetness and Solar Variability”. <i>Journal of Quaternary Science</i> [online] 22 (7), 667–679. doi: <a href="https://doi.org/10.1002/jqs.1093">10.1002/jqs.1093</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_72"></a>Tarasov, L. and Peltier, W. (2005) “Arctic Freshwater Forcing of the Younger Dryas Cold Reversal”. <i>Nature</i> [online] 435 (7042), 662–665. doi: <a href="https://doi.org/10.1038/nature03617">10.1038/nature03617</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_73"></a>Van Geel, B., Buurman, J., and Waterbolk, H.T. (1996) “Archaeological and Palaeoecological Indications of an Abrupt Climate Change in The Netherlands, and Evidence for Climatological Teleconnections around 2650 BP”. <i>Journal of Quaternary Science</i> [online] 11 (6), 451–460. doi: <a href="https://doi.org/10.1002/(SICI)1099-1417(199611/12)11:6<451::AID-JQS275>3.0.CO;2-9">10.1002/(SICI)1099-1417(199611/12)11:6&#60;451:AID-JQS275&#62;3.0.CO;2-9</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_74"></a>WAIS Divide Project Members (2015) “Precise Interpolar Phasing of Abrupt Climate Change during the Last Ice Age”. <i>Nature</i> [online] 520 (7549), 661–665. doi: <a href="https://doi.org/10.1038/nature14401">10.1038/nature14401</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_75"></a>Wanamaker, A.D., Butler, P.G., Scourse, J.D., Heinemeier, J., Eiríksson, J., Knudsen, K.L., and Richardson, C.A. (2012) “Surface Changes in the North Atlantic Meridional Overturning Circulation during the Last Millennium”. <i>Nature Communications</i> [online] 3 (1), 899. doi: <a href="https://doi.org/10.1038/ncomms1901">10.1038/ncomms1901</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_76"></a>Wolbach, W.S., Ballard, J.P., Mayewski, P.A., Kurbatov, A., Bunch, T.E., LeCompte, M.A., Adedeji, V., <i>et al.</i> (2020) “Extraordinary Biomass-Burning Episode and Impact Winter Triggered by the Younger Dryas Cosmic Impact ∼12,800 Years Ago: A Reply”. <i>The Journal of Geology</i> [online] 128 (1), 95–107. doi: <a href="https://doi.org/10.1086/706265">10.1086/706265</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_77"></a>Young, J.M., Reyes, A.V., and Froese, D.G. (2021) “Assessing the Ages of the Moorhead and Emerson Phases of Glacial Lake Agassiz and Their Temporal Connection to the Younger Dryas Cold Reversal”. <i>Quaternary Science Reviews</i> [online] 251, 106714. doi: <a href="https://doi.org/10.1016/j.quascirev.2020.106714">10.1016/j.quascirev.2020.106714</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_78"></a>Zhang, X., Lohmann, G., Knorr, G., and Purcell, C. (2014) “Abrupt Glacial Climate Shifts Controlled by Ice Sheet Changes”. <i>Nature</i> [online] 512 (7514), 290–294. doi: <a href="https://doi.org/10.1038/nature13592">10.1038/nature13592</a></div>
</div>
