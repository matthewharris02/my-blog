+++
title = "Monitoring and Measuring Nature and Biodiversity"
author = ["Matthew Harris"]
draft = false
math = true
toc = true
group_tag = "Conservation"
+++

## <span class="section-num">1</span> Collection of terms {#collection-of-terms}

-   <span class="underline">Area of Habitat</span> (Brooks et al 2019) -- see Ectent of Suitable Habitat (ESH)
-   <span class="underline">biodiversity significance</span> (<a href="#citeproc_bib_item_4">Hill <i>et al.</i>, 2019, 6</a>) -- "combines spatial variation in both species richness and levels of endemism, and hence shows the relative contribution of any location to the persistence of forest species"
-   <span class="underline">Extent of Suitable Habitat</span> (ESH) (<a href="#citeproc_bib_item_4">Hill <i>et al.</i>, 2019</a>)
-   <span class="underline">range-size rarity</span> (<a href="#citeproc_bib_item_4">Hill <i>et al.</i>, 2019, 3</a>) -- the contribution of each \\(\sim30\unit{m}\\) cell toward the global extent of suitable habitat for the species (i.e., the inverse of the ESH within each species' distribution)"


## <span class="section-num">2</span> Definitions of Natural {#definitions-of-natural}


### <span class="section-num">2.1</span> Natural Habitat {#natural-habitat}

<a href="#citeproc_bib_item_5">International Finance Corporation, 2012</a>:

> Areas composed of viable assemblages of plant and/or animal species largely native in origin, and/or where human activity has not essentially modified an area’s primary ecological functions and species composition.

<a href="#citeproc_bib_item_2"><i>Council Directive 92/43/EEC of 21 May 1992 on the Conservation of Natural Habitats and of Wild Fauna and Flora</i>, 1992</a>:

> Terrestrial or aquatic areas distinguished by geographic, abiotic or biotic features, whether entirely natural or semi-natural


### <span class="section-num">2.2</span> Natural Ecosystem {#natural-ecosystem}

<a href="#citeproc_bib_item_1">Acountability Framework Initiative, 2019</a>:

> An ecosystem that substantially resembles – in terms of species composition, structure, and ecological function – one that is or would be found in a given area in the absence of major human impacts. This includes human-managed ecosystems where much of the natural species composition, structure, and ecological function are present.


## <span class="section-num">3</span> Different Definitions of Terms {#different-definitions-of-terms}


### <span class="section-num">3.1</span> Credit Nature {#credit-nature}

The following definitions come from the CreditNature guide to Nature Metrics (<a href="#citeproc_bib_item_6">Jepson, 2023</a>):

<span class="underline">Ecosystem Integrity</span>:

> Ecosystem integrity refers to the ability of an ecosystem to maintain its structure, function, composition over time, including its ability to adapt to and recover from human, climate and natural stressors. Higher levels of ecosystem integrity are directly linked to the land’s capacity to provide essential ecosystem services and natural resources. In short, ecosystem integrity offers a holistic view of ecosystem health, functioning and resilience.

<span class="underline">Biodiversity</span>:

> Biodiversity generally refers to the abundance and variability of living organisms within an ecosystem, it is an outcome of ecosystem integrity. Of course there is a feedback loop whereby biodiversity contributes to the creation of biotic structures and functions and enhanced ecosystem complexity, but biodiversity is often considered as a natural resource and therefore an ecosystem outcome.  Higher levels of biodiversity underpin the discovery and development of crops, medicines and foods, as well as promoting health and well-being.

<span class="underline">Ecosystem condition</span>:

> The term ‘condition’ can refer to ecosystem integrity, but in the field of nature metrics it often carries an implicit or explicit assessment of whether the ecosystem is in a desirable or undesirable state relative to a defined ecological benchmark.  It is commonly used to discern the quality of a habitat or vegetation type (used as a proxy for an ecosystem) within a context, offering a yardstick against which improvement or deterioration can be measured.

<span class="underline">Ecosystem Intactness</span>:

> Ecosystem intactness, in contrast, conveys the extent to which an ecosystem remains unscathed or unchanged by human actions or natural disturbances. It’s a barometer indicating how closely the ecosystem mirrors its original or pristine state, hence reflecting the persistence of natural conditions in the face of anthropogenic or environmental stressors.  In this sense it is also a comparative benchmark, but the reference condition is a period before alteration by modern industrial society.

They also provide a short descriptor of each:

-   Ecosystem Integrity: "The Pillar of Resilience"
-   Biodiversity: "The Outcome of Ecosystem Integrity"
-   Ecosystem Condition: "The comparative benchmark"
-   Ecosystem intactness: "The state of anthropogenic alterations"


### <span class="section-num">3.2</span> International Finance Corporation PS6 {#international-finance-corporation-ps6}

-   <span class="underline">Habitat</span>: "Habitat is defined as a terrestrial, freshwater, or marine geographical unit or airway that supports assemblages of living organisms and their interactions with the non-living environment. For the purposes of implementation of this Performance Standard, habitats are divided into modified, natural, and critical. Critical habitats are a subset of modified or natural habitats"
-   <span class="underline">Modified habitat</span>: "Modified habitats are areas that may contain a large proportion of plant and/or animal species of non-native origin, and/or where human activity has substantially modified an area’s primary ecological functions and species composition.5 Modified habitats may include areas managed for agriculture, forest plantations, reclaimed6 coastal zones, and reclaimed wetlands."
-   <span class="underline">Natural habitat</span>: "Natural habitats are areas composed of viable assemblages of plant and/or animal species of largely native origin, and/or where human activity has not essentially modified an area’s primary ecological functions and species composition."


## <span class="section-num">4</span> Frameworks {#frameworks}

-   <a href="#citeproc_bib_item_9">Parrish <i>et al.</i>, 2003</a> -- framework for measuring conservation outcomes developed by the Nature Conservancy -- "Measures of Success Framework"


## <span class="section-num">5</span> Measuring ecosystem change and forest loss {#measuring-ecosystem-change-and-forest-loss}

-   see <a href="#citeproc_bib_item_8">Milodowski <i>et al.</i>, 2017</a> for a look at underestimating deforestation from global/regional satellite datasets rather than local data


## <span class="section-num">6</span> Methods {#methods}

-   <a href="#citeproc_bib_item_3">Davies and Asner, 2014</a> -- 3D-LiDAR for animal ecology
-   <a href="#citeproc_bib_item_10">Simonson <i>et al.</i>, 2014</a> -- LiDAR for animal ecology
-   <a href="#citeproc_bib_item_11">Simonson <i>et al.</i>, 2013</a>, <a href="#citeproc_bib_item_12">2012</a> -- LiDAR for forest conservation and plant diversity
-   <a href="#citeproc_bib_item_7">McMahon <i>et al.</i>, 2015</a> -- LiDAR for forest canopy structure


## <span class="section-num">7</span> Bibliography {#bibliography}

<style>.csl-entry{text-indent: -1.5em; margin-left: 1.5em;}</style><div class="csl-bib-body">
  <div class="csl-entry"><a id="citeproc_bib_item_1"></a>Acountability Framework Initiative (2019) <i>Terms and Definitions</i> [online] Available at: <a href="https://accountability-framework.org/wp-content/uploads/2019/07/Definitions.pdf">https://accountability-framework.org/wp-content/uploads/2019/07/Definitions.pdf</a> (Accessed: April 12, 2023)</div>
  <div class="csl-entry"><a id="citeproc_bib_item_2"></a><i>Council Directive 92/43/EEC of 21 May 1992 on the Conservation of Natural Habitats and of Wild Fauna and Flora</i> (1992) [online] Available at: <a href="http://data.europa.eu/eli/dir/1992/43/oj/eng">http://data.europa.eu/eli/dir/1992/43/oj/eng</a> (Accessed: February 5, 2023)</div>
  <div class="csl-entry"><a id="citeproc_bib_item_3"></a>Davies, A.B. and Asner, G.P. (2014) “Advances in Animal Ecology from 3D-LiDAR Ecosystem Mapping”. <i>Trends in Ecology &#38; Evolution</i> [online] 29 (12), 681–691. doi: <a href="https://doi.org/10.1016/j.tree.2014.10.005">10.1016/j.tree.2014.10.005</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_4"></a>Hill, S.L.L., Arnell, A., Maney, C., Butchart, S.H.M., Hilton-Taylor, C., Ciciarelli, C., Davis, C., <i>et al.</i> (2019) “Measuring Forest Biodiversity Status and Changes Globally”. <i>Frontiers in Forests and Global Change</i> [online] 2. Available at: <a href="https://www.frontiersin.org/articles/10.3389/ffgc.2019.00070">https://www.frontiersin.org/articles/10.3389/ffgc.2019.00070</a> (Accessed: February 19, 2023)</div>
  <div class="csl-entry"><a id="citeproc_bib_item_5"></a>International Finance Corporation (2012) <i>Performance Standard 6: Biodiversity Conservation and Sustainable Management of Living Natural Resources</i>.</div>
  <div class="csl-entry"><a id="citeproc_bib_item_6"></a>Jepson, P. (2023) <i>Making Sense of Nature Metrics: Distinguishing Integrity, Diversity, Condition, and Intactness</i> [online] Available at: <a href="https://creditnature.com/2023/08/09/making-sense-of-nature-metrics-distinguishing-integrity-diversity-condition-and-intactness/">https://creditnature.com/2023/08/09/making-sense-of-nature-metrics-distinguishing-integrity-diversity-condition-and-intactness/</a> (Accessed: August 20, 2023)</div>
  <div class="csl-entry"><a id="citeproc_bib_item_7"></a>McMahon, S.M., Bebber, D.P., Butt, N., Crockatt, M., Kirby, K., Parker, G.G., Riutta, T., <i>et al.</i> (2015) “Ground Based LiDAR Demonstrates the Legacy of Management History to Canopy Structure and Composition across a Fragmented Temperate Woodland”. <i>Forest Ecology and Management</i> [online] 335, 255–260. doi: <a href="https://doi.org/10.1016/j.foreco.2014.08.039">10.1016/j.foreco.2014.08.039</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_8"></a>Milodowski, D.T., Mitchard, E.T.A., and Williams, M. (2017) “Forest Loss Maps from Regional Satellite Monitoring Systematically Underestimate Deforestation in Two Rapidly Changing Parts of the Amazon”. <i>Environmental Research Letters</i> [online] 12 (9), 094003. doi: <a href="https://doi.org/10.1088/1748-9326/aa7e1e">10.1088/1748-9326/aa7e1e</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_9"></a>Parrish, J.D., Braun, D.P., and Unnasch, R.S. (2003) “Are We Conserving What We Say We Are? Measuring Ecological Integrity within Protected Areas”. <i>BioScience</i> [online] 53 (9), 851–860. doi: <a href="https://doi.org/10.1641/0006-3568(2003)053[0851:AWCWWS]2.0.CO;2">10.1641/0006-3568(2003)053[0851:AWCWWS]2.0.CO;2</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_10"></a>Simonson, W.D., Allen, H.D., and Coomes, D.A. (2014) “Applications of Airborne Lidar for the Assessment of Animal Species Diversity”. <i>Methods in Ecology and Evolution</i> [online] 5 (8), 719–729. doi: <a href="https://doi.org/10.1111/2041-210X.12219">10.1111/2041-210X.12219</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_11"></a>Simonson, W.D., Allen, H.D., and Coomes, D.A. (2013) “Remotely Sensed Indicators of Forest Conservation Status: Case Study from a Natura 2000 Site in Southern Portugal”. <i>Ecological Indicators</i> [online] 24, 636–647. doi: <a href="https://doi.org/10.1016/j.ecolind.2012.08.024">10.1016/j.ecolind.2012.08.024</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_12"></a>Simonson, W.D., Allen, H.D., and Coomes, D.A. (2012) “Use of an Airborne Lidar System to Model Plant Species Composition and Diversity of Mediterranean Oak Forests”. <i>Conservation Biology</i> [online] 26 (5), 840–850. doi: <a href="https://doi.org/10.1111/j.1523-1739.2012.01869.x">10.1111/j.1523-1739.2012.01869.x</a></div>
</div>
