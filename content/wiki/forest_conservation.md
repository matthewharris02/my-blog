+++
title = "Forest Conservation"
author = ["Matthew Harris"]
draft = false
math = true
toc = true
group_tag = "Conservation"
+++

## <span class="section-num">1</span> News {#news}

-   **2023-06-XX** | UK Law: Amendment 91 to Clause 65 to the Financial Services and Markets Bill in the House of Lords
    -   Link to amendment: <https://bills.parliament.uk/bills/3326/stages/17577/amendments/95396>
    -   Also see statement by [The Nature Conservancy](https://www.nature.org/en-us/newsroom/uk-leads-by-example-passing-amendment-curb-funding-deforestation/)
        -   TNC description: "extends due diligence requirements to financial institutions to ensure damaging deforestation practices are not financed further"
-   **2022-12-01** | ART issues first TREES credits to Guyana (<a href="#citeproc_bib_item_1">Architecture for REDD+ Transactions, 2022</a>)
    -   33.47 million TREES credits to Guyana over 2016-2020 period
    -   listed on ART registry; available to be bought
-   **2022-06-29** | _Nestlé and Unilever CEOs: we will make our supply chains deforestation-free_ (<a href="#citeproc_bib_item_21">Schneider and Jope, 2022</a>)
    -   outline four things needed for Net-Zero


## <span class="section-num">2</span> REDD+ {#redd-plus}


### <span class="section-num">2.1</span> Introduction {#introduction}


#### What is REDD+? {#what-is-redd-plus}

REDD+ is a UNFCCC COP framework for sustainable forest management and conservation, particularly in mitigation and adaptation to modern climate change: "reducing emissions from deforestation and forest degradation and the role of conservation, sustainable management of forests and enhancement of forest carbon stocks in developing countries".


#### REDD+ in The Paris Agreement {#redd-plus-in-the-paris-agreement}

<span class="underline">Article 5.2</span> states:

> Parties are encouraged to implement and support, including through results-based payments, the existing framework as set out in related guidnace and decisions already agreed under the Convention for: policy approaches and positive incentives for activities relating to **reducing emissions from deforestation and forest degradation, and the role of conservation, sustainable management of forests and enhancement of forest carbon stocks in developing countries**; and alternative policy approaches such as joint mitigation and adaptation approaches for the integral and sustainable management of forests, while reaffirming the importance of incentivizing, as appropriate, non-carbon benefits associated with such approaches

(emphasis mine)


#### Warsaw Framework for REDD+ {#warsaw-framework-for-redd-plus}


### <span class="section-num">2.2</span> Impacts of REDD+ {#impacts-of-redd-plus}


#### Socio-Economic Impacts of REDD+ {#socio-economic-impacts-of-redd-plus}

<!--list-separator-->

-  Indigenous Peoples


#### Environmental Impacts of REDD+ {#environmental-impacts-of-redd-plus}


### <span class="section-num">2.3</span> Financing for REDD+ {#financing-for-redd-plus}


#### Sources {#sources}

-   <a href="#citeproc_bib_item_4">Directorate General for Climate Action (European Commission) and COWI, 2018</a> -- compiled REDDfit database of REDD+ financing
-   <a href="#citeproc_bib_item_22">Silva-Chávez <i>et al.</i>, 2015</a> -- REDD+ finance flows
-   <a href="#citeproc_bib_item_6">Falconer <i>et al.</i>, 2017</a> -- REDD+ finance in Côte D'Iovire
-   <a href="#citeproc_bib_item_16">New York Declaration on Forests, 2022</a>; <a href="#citeproc_bib_item_15">NYDF Assessment Partners, 2020</a>; <a href="#citeproc_bib_item_17">New York Declaration on Forests Assessment Partners, 2020</a>, <a href="#citeproc_bib_item_18">2020</a> -- NYDF Assessments
-   <a href="#citeproc_bib_item_27">Watson and Schalatek, 2021</a> -- Climate Funds Update
-   <a href="#citeproc_bib_item_10">Lujan and Silva-Chávez, 2018</a> -- forest finance
-   <a href="#citeproc_bib_item_19">Ormeño and Gregory, 2017</a> -- Financial architecture for Tambopata-Bahuaja REDD+ and Agroforestry Project
-   <a href="#citeproc_bib_item_30">Wolosin <i>et al.</i>, 2016</a> -- REDD+ finance for the 10 REDD X countries
-   <a href="#citeproc_bib_item_2">Asare and Gohil, 2016</a> -- forest finance in Ghana, Tanzania, Ethiopia, Liberia and DRC (REDD X partners since launch in 2011)


#### Tambopata-Bahuaja REDD+ and Agroforestry Project {#tambopata-bahuaja-redd-plus-and-agroforestry-project}

As described in <a href="#citeproc_bib_item_19">Ormeño and Gregory, 2017</a>.

Funded by Althelia Ecosphere [now acquired by Mirova] and in partnership with Asociación para la Investigacion y Desarrollo Integral (AIDER; a Peruvian non-profit), Ecotierra (a Peruvian-Canadian social business) and SERNANP, Peru’s National Service for Natural Protected Areas.
The investment is in two major stages of EUR 3.6 million (productive leg; target of 1,2500 hectares of agroforestry; Q2 2014 to Q4 2017) and EUR 2.0 million (protection leg; operational expenses, control and surveillance activities; Q2 2014 to Q2 2020.

Design elements:
a. Fund-level guarantee (USAID Development Credit Authority guaranteed “private investors in Althelia Climate Fund up to 50% of the fund’s performance on a portfolio basis”
b. Acceptance of REDD+ credits as Loan Collateral and source of loan repayments
c. In-kind packages to producers rather than credit (US$2,000/hectare, distributed over three years)
d. Revenue sharing agreement that will support conservation by AIDER
e. Creation of a cooperative (COOPASER)
f. Mixed-crop systems to provide alternative income and (timber and banana trees) shade cocoa plants
g. Experienced implementation partners
h. Diversified cocoa seedling sources
i. Location to minimise costs and benefit from land titling program


### <span class="section-num">2.4</span> REDD+ Structure {#redd-plus-structure}

-   <a href="#citeproc_bib_item_9">Hamrick <i>et al.</i>, 2021</a> -- REDD+ Nesting


## <span class="section-num">3</span> Land rights {#land-rights}

-   <a href="#citeproc_bib_item_11">Mider and Quigley, 2020</a> -- Bloomberg article with a discussion of land right challenges in the Alto Mayo avoided deforestation project in the Peruvian Amazon


## <span class="section-num">4</span> Forest Carbon Offset Controversy {#forest-carbon-offset-controversy}

An investigation released in January 2023 by the Guardian, Diet Zie and Source Material (<a href="#citeproc_bib_item_7">Greenfield, 2023</a>; <a href="#citeproc_bib_item_23">SourceMaterial, 2023</a>) claimed that the majority (&gt;90%) of forest carbon credits were invalid due to misrepresentation of baselines. It was based on an investigation by journalists, combining peer-reviewed and reprint academic articles ((NO_ITEM_DATA:west.etal2023-preprint; <a href="#citeproc_bib_item_28">West <i>et al.</i>, 2020</a>; <a href="#citeproc_bib_item_8">Guizar-Coutiño <i>et al.</i>, 2022</a>); hereafter W20, W23, and GC22), with interviews and on-the-ground reporting. The pre-print (NO_ITEM_DATA:west.etal2023-preprint) has now been peer-reviewed and published (<a href="#citeproc_bib_item_29">West <i>et al.</i>, 2023</a>).

This claim was disputed by <a href="#citeproc_bib_item_26">Verra, 2023</a> and by Everland (<a href="#citeproc_bib_item_20">Pauly <i>et al.</i>, 2023</a>; <a href="#citeproc_bib_item_24">Tosteson and Pauly, 2023</a>) -- a company that sells REDD+ credits.

<a href="#citeproc_bib_item_13">Mitchard, 2023</a> in a comment on Carbon Pulse provides further criticisms: a) that W20, W23, and GC22 all used global satellite datasets with known issues on accurately measuring local forest change (see (<a href="#citeproc_bib_item_12">Milodowski <i>et al.</i>, 2017</a>)); b) that W23 used a synthetic control method that had significant errors in their validation but was used nonetheless; c) W23 excluded two successful projects in Cambodia because of the lack of synthetic control, even though these conserve large blocks of rare forest and therefore would not have similar sites.

<a href="#citeproc_bib_item_5">Everland, 2023</a> provide a further analysis of W23, claiming that this contained five 'fundamental flaws', including mathematical errors, omission of important deforestation risk factors, and lack of transparency. I am not sure if this last claim is entirely true, as W23 do provide their [underlying data](https://dataverse.nl/file.xhtml?fileId=374696&version=1.1), but I cannot work out whether they properly provide their controls.

On the other hand, <a href="#citeproc_bib_item_3">Calyx Global, 2023</a> support West _et al._'s <a href="#citeproc_bib_item_29">2023</a> findings and disagree with <a href="#citeproc_bib_item_20">Pauly <i>et al.</i>, 2023</a>. They criticise <a href="#citeproc_bib_item_20">Pauly <i>et al.</i>, 2023</a> for faulty assumptions and drawing the wrong conclusions from insufficient data.

<a href="#citeproc_bib_item_14">Mitchard <i>et al.</i>, 2023</a> argue that there are multiple serious flaws in the <a href="#citeproc_bib_item_29">West <i>et al.</i>, 2023</a> analysis that invalidates their results.


## <span class="section-num">5</span> Forest Carbon Markets {#forest-carbon-markets}

On 03 May 2024, the Article 6.4 ('Paris Agreeemnt Crediting Mechanism') Supervisory Body established new safeguards for people affected by activities conducted under Art. 6.4 <a href="#citeproc_bib_item_25">United Nations Framework Convention on Climate Change, 2024</a>. They established the 'Appeals and Grievances Procecudre' ([A6.4-SB011-A03](https://unfccc.int/sites/default/files/resource/a64-sb011-a03.pdf)), which comes in to immediate effect.


## <span class="section-num">6</span> Bibliography {#bibliography}

<style>.csl-entry{text-indent: -1.5em; margin-left: 1.5em;}</style><div class="csl-bib-body">
  <div class="csl-entry"><a id="citeproc_bib_item_1"></a>Architecture for REDD+ Transactions (2022) <i>ART Issues World’s First Jurisdictional Forestry TREES Carbon Credits to Guyana</i> [online] Available at: <a href="https://www.artredd.org/art-issues-worlds-first-jurisdictional-forestry-carbon-credits-to-guyana/">https://www.artredd.org/art-issues-worlds-first-jurisdictional-forestry-carbon-credits-to-guyana/</a> (Accessed: December 21, 2022)</div>
  <div class="csl-entry"><a id="citeproc_bib_item_2"></a>Asare, R.A. and Gohil, D. (2016) <i>The Evolution of Forest Finance in Five African Countries</i> [online] Washington D.C: Forest Trends. Available at: <a href="https://www.forest-trends.org/publications/the-evolution-of-forest-finance-in-five-african-countries/">https://www.forest-trends.org/publications/the-evolution-of-forest-finance-in-five-african-countries/</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_3"></a>Calyx Global (2023) <i>Turning REDD into Green: Improving the GHG Integrity of Avoided Deforestation Credits</i> [online] Available at: <a href="https://calyxglobal.com/resource-post/?q=9">https://calyxglobal.com/resource-post/?q=9</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_4"></a>Directorate General for Climate Action (European Commission) and COWI (2018) <i>Study on EU Financing of REDD+ Related Activities, and Results-Based Payments Pre and Post 2020: Sources, Cost Effectiveness and Fair Allocation of Incentives.</i> [online] Luxembourg. doi: <a href="https://doi.org/10.2834/687514">10.2834/687514</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_5"></a>Everland (2023) <i>Science Vs West: When Experts Buy Bad Science</i> [online] Available at: <a href="https://everland.earth/news/science-vs-west/">https://everland.earth/news/science-vs-west/</a> (Accessed: August 31, 2023)</div>
  <div class="csl-entry"><a id="citeproc_bib_item_6"></a>Falconer, A., Dontenville, A., Parker, C., Daubrey, M., and Gnaore, L. (2017) <i>The Landscape of REDD+ Aligned Finance in Côte d’Ivoire</i> [online] Climate Policy Initiative. Available at: <a href="https://www.climatepolicyinitiative.org/publication/landscape-redd-aligned-finance-cote-divoire/">https://www.climatepolicyinitiative.org/publication/landscape-redd-aligned-finance-cote-divoire/</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_7"></a>Greenfield, P. (2023) “Revealed: More than 90\% of Rainforest Carbon Offsets by Biggest Provider Are Worthless, Analysis Shows”. <i>The Guardian: Environment</i> [online] 18 January. Available at: <a href="https://www.theguardian.com/environment/2023/jan/18/revealed-forest-carbon-offsets-biggest-provider-worthless-verra-aoe">https://www.theguardian.com/environment/2023/jan/18/revealed-forest-carbon-offsets-biggest-provider-worthless-verra-aoe</a> (Accessed: January 21, 2023)</div>
  <div class="csl-entry"><a id="citeproc_bib_item_8"></a>Guizar-Coutiño, A., Jones, J.P.G., Balmford, A., Carmenta, R., and Coomes, D.A. (2022) “A Global Evaluation of the Effectiveness of Voluntary REDD+ Projects at Reducing Deforestation and Degradation in the Moist Tropics”. <i>Conservation Biology</i> [online] 36 (6), e13970. doi: <a href="https://doi.org/10.1111/cobi.13970">10.1111/cobi.13970</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_9"></a>Hamrick, K., Webb, C., and Ellis, R. (2021) <i>Nesting REDD+: Pathways to Bridge Project and Jurisdictional Programs</i> [online] Virginia, USA: The Nature Conservancy. Available at: <a href="https://www.nature.org/content/dam/tnc/nature/en/documents/REDDPlus_PathwaystoBridgeProjectandJurisdictionalPrograms.pdf">https://www.nature.org/content/dam/tnc/nature/en/documents/REDDPlus_PathwaystoBridgeProjectandJurisdictionalPrograms.pdf</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_10"></a>Lujan, B. and Silva-Chávez, G. (2018) <i>Mapping Forest Finance: A Landscape of Available Sources of Finance for REDD+ and Climate Action in Forests</i> [online] New York; Washington, DC: Environmental Defense Fund; Forest Trends. Available at: <a href="https://www.forest-trends.org/publications/mapping-forest-finance/">https://www.forest-trends.org/publications/mapping-forest-finance/</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_11"></a>Mider, Z.R. and Quigley, J. (2020) “Disney Spent Millions to Save a Rainforest. Why Are People There So Mad?”. <i>Bloomberg.Com</i> [online] 9 June. Available at: <a href="https://www.bloomberg.com/graphics/2020-disney-peru-deforestation/">https://www.bloomberg.com/graphics/2020-disney-peru-deforestation/</a> (Accessed: August 31, 2023)</div>
  <div class="csl-entry"><a id="citeproc_bib_item_12"></a>Milodowski, D.T., Mitchard, E.T.A., and Williams, M. (2017) “Forest Loss Maps from Regional Satellite Monitoring Systematically Underestimate Deforestation in Two Rapidly Changing Parts of the Amazon”. <i>Environmental Research Letters</i> [online] 12 (9), 094003. doi: <a href="https://doi.org/10.1088/1748-9326/aa7e1e">10.1088/1748-9326/aa7e1e</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_13"></a>Mitchard, E. (2023) <i>COMMENT: Tropical Forest Conservation Is Tricky to Measure, but We’re Running out of Time « Carbon Pulse</i> [online] Available at: <a href="https://carbon-pulse.com/189033/">https://carbon-pulse.com/189033/</a> (Accessed: August 31, 2023)</div>
  <div class="csl-entry"><a id="citeproc_bib_item_14"></a>Mitchard, E.T.A., Carstairs, H., Cosenza, R., Saatchi, S.S., Funk, J., Quintano, P.N., Brade, T., <i>et al.</i> (2023) <i>Serious Errors Impair an Assessment of Forest Carbon Projects: A Rebuttal of West et Al. (2023)</i> [online] doi: <a href="https://doi.org/10.48550/arXiv.2312.06793">10.48550/arXiv.2312.06793</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_15"></a>NYDF Assessment Partners (2020) <i>Goal 8 Assessment: Providing Finance for Forest Action. New York Declaration on Forests Progress Assessment</i> [online] Climate Focus (coordinator and editor). Available at: <a href="https://forestdeclaration.org/goals/goal-8#key">https://forestdeclaration.org/goals/goal-8#key</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_16"></a>New York Declaration on Forests (2022) <i>Reward Results by Countries and Jurisdictions</i> [online] Available at: <a href="https://forestdeclaration.org/wp-content/uploads/2022/02/Forest-rewards-Progress-since-2014.pdf">https://forestdeclaration.org/wp-content/uploads/2022/02/Forest-rewards-Progress-since-2014.pdf</a> (Accessed: August 26, 2022)</div>
  <div class="csl-entry"><a id="citeproc_bib_item_17"></a>New York Declaration on Forests Assessment Partners (2020) <i>Goal 9 Assessment: Rewarding Countries and Jurisdictions for Results</i> [online] Climate Focus (coordinator and editor). Available at: <a href="https://forestdeclaration.org/resources">https://forestdeclaration.org/resources</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_18"></a>New York Declaration on Forests Assessment Partners (2020) <i>Goal 1 Assessment: Striving to End Natural Forest Loss</i> [online] Climate Focus (coordinator and editor). Available at: <a href="https://forestdeclaration.org/resources">https://forestdeclaration.org/resources</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_19"></a>Ormeño, L.M. and Gregory, J. (2017) <i>Financing Conservation and Sustainable Land Use in the Amazon</i> [online] Washington D.C: Forest Trends. Available at: <a href="https://www.forest-trends.org/publications/financing-conservation-sustainable-land-use-amazon/">https://www.forest-trends.org/publications/financing-conservation-sustainable-land-use-amazon/</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_20"></a>Pauly, M., Tosteson, J., and Crosse, W. (2023) <i>New Analysis Reveals Just How Effective REDD+ Is.</i> [online] Available at: <a href="https://everland.earth/news/new-analysis-finds-that-redd-project-baselines-accurately-reflect-observed-jurisdictional-forest-loss-globally/">https://everland.earth/news/new-analysis-finds-that-redd-project-baselines-accurately-reflect-observed-jurisdictional-forest-loss-globally/</a> (Accessed: August 31, 2023)</div>
  <div class="csl-entry"><a id="citeproc_bib_item_21"></a>Schneider, M. and Jope, A. (2022) “Nestlé and Unilever CEOs: We Will Make Our Supply Chains Deforestation-Free”. <i>Financial Times</i> 29 June</div>
  <div class="csl-entry"><a id="citeproc_bib_item_22"></a>Silva-Chávez, G., Schaap, B., and Breitfeller, J. (2015) <i>REDD+ Finance Flows 2009-2014: Trends and Lessons Learned in REDDX Countries</i> [online] Washington: Forest Trends. Available at: <a href="https://www.forest-trends.org/wp-content/uploads/imported/reddx-report-2015-110915-pdf.pdf">https://www.forest-trends.org/wp-content/uploads/imported/reddx-report-2015-110915-pdf.pdf</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_23"></a>SourceMaterial (2023) “The Carbon Con: How Offsetting Claims Are Vastly Inflated”. <i>Sourcematerial</i> [online] 18 January. Available at: <a href="https://www.source-material.org/vercompanies-carbon-offsetting-claims-inflated-methodologies-flawed/">https://www.source-material.org/vercompanies-carbon-offsetting-claims-inflated-methodologies-flawed/</a> (Accessed: August 31, 2023)</div>
  <div class="csl-entry"><a id="citeproc_bib_item_24"></a>Tosteson, J. and Pauly, M. (2023) <i>The Science behind the Guardian Piece Is Fatally Flawed</i> [online] Available at: <a href="https://everland.earth/news/the-science-behind-the-the-guardian-piece-is-fatally-flawed/">https://everland.earth/news/the-science-behind-the-the-guardian-piece-is-fatally-flawed/</a> (Accessed: August 31, 2023)</div>
  <div class="csl-entry"><a id="citeproc_bib_item_25"></a>United Nations Framework Convention on Climate Change (2024) <i>UN Body Adopts Historic Human Rights Protections for Carbon Market Mechanism</i> [online] Available at: <a href="https://unfccc.int/news/un-body-adopts-historic-human-rights-protections-for-carbon-market-mechanism">https://unfccc.int/news/un-body-adopts-historic-human-rights-protections-for-carbon-market-mechanism</a> (Accessed: June 1, 2024)</div>
  <div class="csl-entry"><a id="citeproc_bib_item_26"></a>Verra (2023) <i>Verra Response to Guardian Article on Carbon Offsets</i> [online] Available at: <a href="https://verra.org/verra-response-guardian-rainforest-carbon-offsets/">https://verra.org/verra-response-guardian-rainforest-carbon-offsets/</a> (Accessed: January 21, 2023)</div>
  <div class="csl-entry"><a id="citeproc_bib_item_27"></a>Watson, C. and Schalatek, L. (2021) <i>Global Climate Finance Architecture</i> [online] 2. London, United Kingdom &#38; Washington DC, USA: Climate Funds Update (Overseas Development Institute &#38; Heinrich Böll Stiftung Washingon, DC). Available at: <a href="https://climatefundsupdate.org/about-climate-finance/global-climate-finance-architecture/">https://climatefundsupdate.org/about-climate-finance/global-climate-finance-architecture/</a> (Accessed: July 29, 2021)</div>
  <div class="csl-entry"><a id="citeproc_bib_item_28"></a>West, T.A.P., Börner, J., Sills, E.O., and Kontoleon, A. (2020) “Overstated Carbon Emission Reductions from Voluntary REDD+ Projects in the Brazilian Amazon”. <i>Proceedings of the National Academy of Sciences</i> [online] 117 (39), 24188–24194. doi: <a href="https://doi.org/10.1073/pnas.2004334117">10.1073/pnas.2004334117</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_29"></a>West, T.A.P., Wunder, S., Sills, E.O., Börner, J., Rifai, S.W., Neidermeier, A.N., Frey, G.P., <i>et al.</i> (2023) “Action Needed to Make Carbon Offsets from Forest Conservation Work for Climate Change Mitigation”. <i>Science</i> [online] 381 (6660), 873–877. doi: <a href="https://doi.org/10.1126/science.ade3535">10.1126/science.ade3535</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_30"></a>Wolosin, M., Breitfeller, J., and Schaap, B. (2016) <i>The Geography of REDD+ Finance</i> [online] Washington D.C: Forest Trends. Available at: <a href="https://www.forest-trends.org/publications/the-geography-of-redd-finance/">https://www.forest-trends.org/publications/the-geography-of-redd-finance/</a></div>
  <div class="csl-entry">NO_ITEM_DATA:west.etal2023-preprint</div>
</div>
