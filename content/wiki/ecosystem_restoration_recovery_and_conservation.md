+++
title = "Ecosystem Restoration, Recovery, and Conservation"
author = ["Matthew Harris"]
draft = false
math = true
toc = true
group_tag = "Conservation"
+++

## International instruments and agreements {#international-instruments-and-agreements}


### The Rio Conventions {#the-rio-conventions}

-   [Convention on Biological Diversity (CBD)]({{< relref "convention_on_biological_diversity_cbd.md" >}})
-   UN Framework Convention on Climate Change (UNFCCC)
-   UN Convention to Combat Desertification (UNCCD)


### The UN Decade on Ecosystem Restoration (UNDER, 2021-2030) {#the-un-decade-on-ecosystem-restoration--under-2021-2030}

Website: <https://www.decadeonrestoration.org/>
Resolution: 73/284
Date of adoption: 01 March 2019
**Key statements**:

-   _"Emphasizing_ that forests, wetlands, drylands and other natural ecosystems are essential for sustainable development, poverty alleviation and improved human well-being" _[2]_
-   "_Stressing_ the importance of the ecosystem approach for the integrated management of land, water and living resources and the need to step up efforts to tackle desertification, land degradation, erosion and drought, biodiversity loss and water scarcity, which are seen as major environmental, economic and social challenges for global sustainable development" _[2]_
-   "_Recalling also_ that sinks and reservoirs of greenhouse gases include forests, oceans, wetlands and soil, and recalling further the essential role in the adaptation to and mitigation of climate change and in enhancing the resilience of ecosystems and societies to its impacts" _[3]_
-   "_Noting also_ that restoration needs to be carried out in ways that balance social, economic and environmental objectives and with the engagement of relevant stakeholders, including indigenous peoples and local communities" _[4]_
-   "_Recognising_ that protecting ecosystems and avoiding harmful practices against animals, plants, microorganisms and non-living environments contributes to the coexistence of humankind in harmony with nature" _[5]_
-   "_Recognising also_ that ecosystem restoration is a complement to conservation activities and that priority should be given to conserving biodiversity and preventing the degradation of natural habitats and ecosystems by reducing pressures and maintaining ecological integrity and the provision of ecosystem services" _[5]_
-   "_Decides_ to proclaim 2021-2030 the United Nations Decade on Ecosystem Restoration, within existing structures and available resources, with the aim of supporting and scaling up efforts to prevent, halt and reverse the degradation of ecosystems worldwide and raise awareness of the importance of successful ecosystem restoration" _[5]_


### Biodiversity-related conventions {#biodiversity-related-conventions}

1.  Ramsar Convention on Wetlands of International Importance Especially as Waterfowl Habitat
2.  UNCCD
3.  Convention on International Trade in Endangered Species of Wild Fauna and Flora (CITES)
4.  International Treaty on Plant Genetic Resources for Food and Agriculture
5.  Convention on the Conservation of Migratory Species of Wild Animals


## NNL (No net loss) | Biodiversity {#nnl--no-net-loss--biodiversity}

-   <a href="#citeproc_bib_item_50">Vyawahare, 2022</a> (mongabay article) and <a href="#citeproc_bib_item_12">Devenish <i>et al.</i>, 2022</a> -- no net forest loss in Madagascar mining
    -   Thoughts:
        -   It's as if biodiversity can just be transferred from one area to another, like capital...
        -   It uses a counterfactual model to look at what deforestation would have been without the intervention. It's basically saying that actual deforestation in the present\* can be offset by reduced deforestation in the future, rather than actual forest growth. Like, even if you wanted to argue that the 'net zero' deforestation is good, it also means that there is no reduction in overall deforestation, which really needs to be zero
        -   And then there's this: "One of the biggest drawbacks of the analysis is that it relies on forest cover as an “imperfect proxy” for biodiversity, the paper says; the authors did not consider the fate of individual species. Also, the gains the study documented do not indicate that forest cover increased. They only show that deforestation was slower than it might have been without the offsets in place." -- Which highlights how this isn't a biodiversity offset but a "forest" offset,  and shows that they're using these terms interchangeably, when they're not. Surely it's obvious that forest cover /= biodiversity!?
        -   Then from the actual study: "Policies aimed at delivering no net loss (NNL) of biodiversity, in theory, allow development to proceed while avoiding environmental damage" -- Which is just a lie! 'Offsetting' damage doesn't mean no damage, just that you are balancing it with some improvement else where. The regional/global environment can't simply be seen in the idea of 'average damage'. The local environment is being damaged!


## Carbon vs Biodiversity {#carbon-vs-biodiversity}

-   <a href="#citeproc_bib_item_30">Martin <i>et al.</i>, 2013</a> -- _Carbon pools recover more quickly than plant biodiversity in tropical secondary forests_
    -   "Our results indicate that carbon pools and biodiversity show different recovery rates under passive, secondary succession and that colonization by undisturbed forest plant species is slow" _[abstract]_
    -   "Carbon pools may take approximately 80 years to recover following disturbance, faunal biodiversity 150 years and plant biodiversity well over 100 years"


## Connectivity {#connectivity}


### Definitions connectivity, corridor, passage (<a href="#citeproc_bib_item_4">Beier and Noss, 1998</a>) {#definitions-connectivity-corridor-passage}

-   "We define _corridor_ as a linear habitat, embedded in a dissimilar matrix, that connects two or more larger blocks of habitat and that is proposed for conservation on the grounds that it will enhance or maintain the viability of specific wildlife populations in the habitat blocks"
-   "We define _passage_ as a travel via a corridor by individual animals from one habitat patch to another"


### Connectivity definitions (<a href="#citeproc_bib_item_36">Noss, 1991</a>) {#connectivity-definitions}

-   <span class="underline">corridor</span> limited (in biogeographical terminology) to "broad, internally heterogeneous swaths of habitat that permit the direct spread of many or most taxa from one region to another (Brown and Gibson 1983)"
-   <span class="underline">filter</span> -- "dispersal routes containing a more limited habitat spectrum through which certain species pass and others are excluded" -- e.g., a clearcut where black bears move through but pine martens avoid


## Fragmentation and patch-size {#fragmentation-and-patch-size}


### EXAMPLE <a href="#citeproc_bib_item_48">Valente <i>et al.</i>, 2023</a> -- fragmentation effects on murrelet in Pacific Northwest {#example-fragmentation-effects-on-murrelet-in-pacific-northwest}

-   <span class="underline">Habitat changes</span>
    -   1988-2020, &sim;20% decline of habitat (&sim;0.5 million ha) -- 74% in private industrial lands and 20% in private non-industrial lands; only increase in federal lands (but not much)
    -   decline &rarr; fragmentation: &uarr; 17% of hard edges; all land ownership categories except federal lands; largest in private industrial (nearly doubled)
-   <span class="underline">Occupancy patterns</span>
    -   local scale: strong influence of habitat amount on occupancy
    -   local scale: &uarr; occupancy with fragmentation
    -   landscape scale: &darr; occupancy with fragmentation -- amplified effect inland
-   potential explanation for lack of population recovery despite two decades of protection under 1994 NW Forest Plan


### Criticism of minimum patch size (<a href="#citeproc_bib_item_42">Riva and Fahrig, 2023</a>) {#criticism-of-minimum-patch-size}

-   "One of the most widely applied general principles for biodiversity conservation is the SL &gt; SS principle that a single large patch (SL) -- or a few large patches -- has higher biodiversity than several small (SS) patches of the same cumulative area. The SL &gt; SS principle, initially proposed by Wilson &amp; Willis (1975) and Diamond (1975), is inspired by island biogeography theory and seems intuitive"
-   Here: use 76 metacommunities, 4401 species, 1190 patches across taxonomic groups using the recent FragSD datasets (76 datasets total)
-   "Therefore, when implementing a policy meant to benefit biodiversity, protection should be extended to as many small patches as possible, while also including one or a few large patches suitable for herptiles"
-   _conclusion_: **"we find that policies aimed at improving human-dominated landscapes for biodiversity protection will require abandoning minimum patch size criteria"**


## List of relevant literature {#list-of-relevant-literature}

-   <a href="#citeproc_bib_item_39">Olson <i>et al.</i>, 2001</a> and <a href="#citeproc_bib_item_13">Dinerstein <i>et al.</i>, 2017</a>-- ecoregions of the world
-   <a href="#citeproc_bib_item_3">Barnosky <i>et al.</i>, 2011</a>; <a href="#citeproc_bib_item_10">Cowie <i>et al.</i>, 2022</a> -- Sixth Mass Extinction
-   <a href="#citeproc_bib_item_5">Beyer <i>et al.</i>, 2020</a> -- ecoregion intactness loss
-   <a href="#citeproc_bib_item_7">Boulton <i>et al.</i>, 2022</a> -- loss of Amazon forest resilience
-   <a href="#citeproc_bib_item_8">Butchart <i>et al.</i>, 2010</a>
-   <a href="#citeproc_bib_item_14">Dixon <i>et al.</i>, 2016</a> -- wetland extent
-   <a href="#citeproc_bib_item_15">Erb <i>et al.</i>, 2018</a> -- forest management and grazing
-   <a href="#citeproc_bib_item_16">Fluet-Chouinard <i>et al.</i>, 2023</a> -- wetland loss
-   <a href="#citeproc_bib_item_17">Forzieri <i>et al.</i>, 2022</a> -- loss of forest resilience
-   <a href="#citeproc_bib_item_21">Isbell <i>et al.</i>, 2011</a> -- need for high plant diversity for ecosystem service provision
-   <a href="#citeproc_bib_item_22">Isbell <i>et al.</i>, 2015</a> -- biodiversity and increases resistance to environmental change
-   <a href="#citeproc_bib_item_20">International Union for Conservation of Nature, 2023</a> -- 28% of all species assessed threatened with extinction
-   <a href="#citeproc_bib_item_23">Lapola <i>et al.</i>, 2023</a> -- drivers and impacts of Amazon forest degradation
-   <a href="#citeproc_bib_item_24">Le Provost <i>et al.</i>, 2023</a> -- ecosystem services needs biodiversity
-   <a href="#citeproc_bib_item_25">Liang <i>et al.</i>, 2022</a> -- plant diversity and ...
-   <a href="#citeproc_bib_item_28">Malhi <i>et al.</i>, 2016</a> -- megafauna linked to ecosystem functioning since Pleistocene
-   <a href="#citeproc_bib_item_29">Malhi <i>et al.</i>, 2022</a> -- greater ecosystem energetics in (selectively) logged forests compared to old-growth forests
-   <a href="#citeproc_bib_item_31">Martin and Watson, 2016</a> -- intact ecosystems for climate mitigation
-   <a href="#citeproc_bib_item_34">Mitchell <i>et al.</i>, 2015</a> -- fragmentation reduces ecosystem services **TO CHECK**
-   <a href="#citeproc_bib_item_35">Newbold <i>et al.</i>, 2016</a> -- BII and PBs
-   <a href="#citeproc_bib_item_37">Oehri <i>et al.</i>, 2017</a> -- biodiversity linked to increased PP
-   <a href="#citeproc_bib_item_38">Oliver <i>et al.</i>, 2015</a> -- biodiversity and resilience of ecosystem functioning
-   <a href="#citeproc_bib_item_43">Rogers <i>et al.</i>, 2022</a> -- ecosystem integrity and climate mitigation
-   <a href="#citeproc_bib_item_46">Symes <i>et al.</i>, 2018</a> -- tropical biodiversity because of wildlife trade and deforestation
-   <a href="#citeproc_bib_item_47">United Nations Environment Programme, 2022</a> -- climate change mitigation and biodiversity targets
-   <a href="#citeproc_bib_item_51">Watson <i>et al.</i>, 2018</a> -- intact ecosystems for climate mitigation
-   <a href="#citeproc_bib_item_53">World Wildlife Fund, 2022</a> -- 69% decline in wildlife since 1970
-   <a href="#citeproc_bib_item_32">McMahon <i>et al.</i>, 2015</a> -- LiDAR and forest management
-   <a href="#citeproc_bib_item_9">Carroll <i>et al.</i>, 2018</a> -- future connectivity areas under CC
-   Palaeoecology and conservation
    -   <a href="#citeproc_bib_item_52">Willis and Birks, 2006</a> -- paleoecology and conservation
    -   <a href="#citeproc_bib_item_6">Birks, 2012</a> -- paleoecology and conservation
-   Nature-positive
    -   <a href="#citeproc_bib_item_26">Locke <i>et al.</i>, 2021</a> -- nature-positive
    -   <a href="#citeproc_bib_item_33">Milner-Gulland, 2022</a> -- nature-positive
-   Naturalness and wilderness
    -   <a href="#citeproc_bib_item_27">Lovejoy, 2016</a> -- importance of wilderness
    -   <a href="#citeproc_bib_item_1">Anderson, 1991</a> -- framework for evaluating naturalness
    -   <a href="#citeproc_bib_item_2">Angermeier, 2000</a> -- 'natural imperative for biological conservation'
    -   <a href="#citeproc_bib_item_45">Siipi, 2004</a> -- 'Naturalness in biological conservation'
    -   <a href="#citeproc_bib_item_40">Povilitis, 2001</a> -- 'Towards a robust natural imperative in biological conservation'
    -   <a href="#citeproc_bib_item_11">Czech, 2004</a> -- 'Chronological frame of reference for ecological integrity and natural conservation'
-   Rights to Nature
    -   <a href="#citeproc_bib_item_18">Gabriel, 2023</a> -- Reporting on Brazil's passing of a law that grants rights to a river -- the river Laje


## Conservation and climate change {#conservation-and-climate-change}

Present and future climate change will have a significant effect on how biodiversity conservation operates. A focus on existing ranges and ecologies in area-based conservation ignores the fact that in future, many species and ecosystems may shift their location. For example, <a href="#citeproc_bib_item_19">Hoffmann <i>et al.</i>, 2019</a> predict that in the temperate and northern high-latitudes, large areas of novel climate conditions will occur in PAs. They suggest the need for large and networked PA systems to be resilient against climate shifts. Similarly, <a href="#citeproc_bib_item_44">Sales and Pires, 2023</a> found that many climate refugia in South America are not covered by PAs. This means that species that may retreat to their refugia as climate change continues will not have these areas protected.

<a href="#citeproc_bib_item_41">Redford and Dudley, 2024</a> suggest the definition of a 'future conservation area' to supplement the IUCN PA and PA governance categories in order to reflect future range shifts and changes in biodiversity values with climate change. This would be areas that are neither PAs or OECMs, but, in future, could become important; either as a habitat, or as, for example, an important corridor. These could be species- or ecosystem-level objectives. They suggest that these don't need to be permanent; for example, there may be temporary areas that are protected while more permanent solutions for at-risk species are put in place. Although they wouldn't have legal protection in the short term, they could act like Key Biodiversity Areas (KBAs).

Additionally, existing PAs are potentially affected by future range shifts of invasive species. <a href="#citeproc_bib_item_49">Vicente <i>et al.</i>, 2013</a> applied species distribution modelling to three Australian wattle (_Acacia_) species in northern Portugal and found that these invasives will put increased pressure on particular species (_A. dealbata_) in existing PAs.


## Bibliography {#bibliography}

<style>.csl-entry{text-indent: -1.5em; margin-left: 1.5em;}</style><div class="csl-bib-body">
  <div class="csl-entry"><a id="citeproc_bib_item_1"></a>Anderson, J.E. (1991) “A Conceptual Framework for Evaluating and Quantifying Naturalness”. <i>Conservation Biology</i> [online] 5 (3), 347–352. Available at: <a href="http://www.jstor.org/stable/2385906">http://www.jstor.org/stable/2385906</a> (Accessed: January 6, 2023)</div>
  <div class="csl-entry"><a id="citeproc_bib_item_2"></a>Angermeier, P.L. (2000) “The Natural Imperative for Biological Conservation”. <i>Conservation Biology</i> [online] 14 (2), 373–381. doi: <a href="https://doi.org/10.1046/j.1523-1739.2000.98362.x">10.1046/j.1523-1739.2000.98362.x</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_3"></a>Barnosky, A.D., Matzke, N., Tomiya, S., Wogan, G.O.U., Swartz, B., Quental, T.B., Marshall, C., <i>et al.</i> (2011) “Has the Earth’s Sixth Mass Extinction Already Arrived?”. <i>Nature</i> [online] 471 (7336), 51–57. doi: <a href="https://doi.org/10.1038/nature09678">10.1038/nature09678</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_4"></a>Beier, P. and Noss, R.F. (1998) “Do Habitat Corridors Provide Connectivity?”. <i>Conservation Biology</i> [online] 12 (6), 1241–1252. doi: <a href="https://doi.org/10.1111/j.1523-1739.1998.98036.x">10.1111/j.1523-1739.1998.98036.x</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_5"></a>Beyer, H.L., Venter, O., Grantham, H.S., and Watson, J.E. (2020) “Substantial Losses in Ecoregion Intactness Highlight Urgency of Globally Coordinated Action”. <i>Conservation Letters</i> [online] 13 (2), e12692. doi: <a href="https://doi.org/10.1111/conl.12692">10.1111/conl.12692</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_6"></a>Birks, H.J.B. (2012) “Ecological Palaeoecology and Conservation Biology: Controversies, Challenges, and Compromises”. <i>International Journal of Biodiversity Science, Ecosystem Services &#38; Management</i> [online] 8 (4), 292–304. doi: <a href="https://doi.org/10.1080/21513732.2012.701667">10.1080/21513732.2012.701667</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_7"></a>Boulton, C.A., Lenton, T.M., and Boers, N. (2022) “Pronounced Loss of Amazon Rainforest Resilience since the Early 2000s”. <i>Nature Climate Change</i> [online] 12 (3), 271–278. doi: <a href="https://doi.org/10.1038/s41558-022-01287-8">10.1038/s41558-022-01287-8</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_8"></a>Butchart, S.H.M., Walpole, M., Collen, B., van Strien, A., Scharlemann, J.P.W., Almond, R.E.A., Baillie, J.E.M., <i>et al.</i> (2010) “Global Biodiversity: Indicators of Recent Declines”. <i>Science</i> [online] 328 (5982), 1164–1168. doi: <a href="https://doi.org/10.1126/science.1187512">10.1126/science.1187512</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_9"></a>Carroll, C., Parks, S.A., Dobrowski, S.Z., and Roberts, D.R. (2018) “Climatic, Topographic, and Anthropogenic Factors Determine Connectivity between Current and Future Climate Analogs in North America”. <i>Global Change Biology</i> [online] 24 (11), 5318–5331. doi: <a href="https://doi.org/10.1111/gcb.14373">10.1111/gcb.14373</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_10"></a>Cowie, R.H., Bouchet, P., and Fontaine, B. (2022) “The Sixth Mass Extinction: Fact, Fiction or Speculation?”. <i>Biological Reviews</i> [online] 97 (2), 640–663. doi: <a href="https://doi.org/10.1111/brv.12816">10.1111/brv.12816</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_11"></a>Czech, B. (2004) “A Chronological Frame of Reference for Ecological Integrity and Natural Conditions”. <i>Natural Resources Journal</i> [online] 44 (4), 1113–1136. Available at: <a href="https://www.jstor.org/stable/24889063">https://www.jstor.org/stable/24889063</a> (Accessed: April 7, 2023)</div>
  <div class="csl-entry"><a id="citeproc_bib_item_12"></a>Devenish, K., Desbureaux, S., Willcock, S., and Jones, J.P.G. (2022) “On Track to Achieve No Net Loss of Forest at Madagascar’s Biggest Mine”. <i>Nature Sustainability</i> [online] 1–11. doi: <a href="https://doi.org/10.1038/s41893-022-00850-7">10.1038/s41893-022-00850-7</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_13"></a>Dinerstein, E., Olson, D., Joshi, A., Vynne, C., Burgess, N.D., Wikramanayake, E., Hahn, N., <i>et al.</i> (2017) “An Ecoregion-Based Approach to Protecting Half the Terrestrial Realm”. <i>BioScience</i> [online] 67 (6), 534–545. doi: <a href="https://doi.org/10.1093/biosci/bix014">10.1093/biosci/bix014</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_14"></a>Dixon, M.J.R., Loh, J., Davidson, N.C., Beltrame, C., Freeman, R., and Walpole, M. (2016) “Tracking Global Change in Ecosystem Area: The Wetland Extent Trends Index”. <i>Biological Conservation</i> [online] 193, 27–35. doi: <a href="https://doi.org/10.1016/j.biocon.2015.10.023">10.1016/j.biocon.2015.10.023</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_15"></a>Erb, K.-H., Kastner, T., Plutzar, C., Bais, A.L.S., Carvalhais, N., Fetzel, T., Gingrich, S., <i>et al.</i> (2018) “Unexpectedly Large Impact of Forest Management and Grazing on Global Vegetation Biomass”. <i>Nature</i> [online] 553 (7686), 73–76. doi: <a href="https://doi.org/10.1038/nature25138">10.1038/nature25138</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_16"></a>Fluet-Chouinard, E., Stocker, B.D., Zhang, Z., Malhotra, A., Melton, J.R., Poulter, B., Kaplan, J.O., <i>et al.</i> (2023) “Extensive Global Wetland Loss over the Past Three Centuries”. <i>Nature</i> [online] 614 (7947), 281–286. doi: <a href="https://doi.org/10.1038/s41586-022-05572-6">10.1038/s41586-022-05572-6</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_17"></a>Forzieri, G., Dakos, V., McDowell, N.G., Ramdane, A., and Cescatti, A. (2022) “Emerging Signals of Declining Forest Resilience under Climate Change”. <i>Nature</i> [online] 608 (7923), 534–539. doi: <a href="https://doi.org/10.1038/s41586-022-04959-9">10.1038/s41586-022-04959-9</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_18"></a>Gabriel, J. (2023) <i>Brazilian City Passes First Law Guaranteeing Rights to a River</i> [online] Available at: <a href="https://www1.folha.uol.com.br/internacional/en/brazil/2023/06/brazilian-city-passes-first-law-guaranteeing-rights-to-a-river.shtml">https://www1.folha.uol.com.br/internacional/en/brazil/2023/06/brazilian-city-passes-first-law-guaranteeing-rights-to-a-river.shtml</a> (Accessed: June 28, 2023)</div>
  <div class="csl-entry"><a id="citeproc_bib_item_19"></a>Hoffmann, S., Irl, S.D.H., and Beierkuhnlein, C. (2019) “Predicted Climate Shifts within Terrestrial Protected Areas Worldwide”. <i>Nature Communications</i> [online] 10 (1), 4787. doi: <a href="https://doi.org/10.1038/s41467-019-12603-w">10.1038/s41467-019-12603-w</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_20"></a>International Union for Conservation of Nature (2023) <i>The IUCN Red List of Threatened Species. Version 2022-2</i>. Available at: <a href="https://www.iucnredlist.org/en">https://www.iucnredlist.org/en</a> (Accessed: April 18, 2023)</div>
  <div class="csl-entry"><a id="citeproc_bib_item_21"></a>Isbell, F., Calcagno, V., Hector, A., Connolly, J., Harpole, W.S., Reich, P.B., Scherer-Lorenzen, M., <i>et al.</i> (2011) “High Plant Diversity Is Needed to Maintain Ecosystem Services”. <i>Nature</i> [online] 477 (7363), 199–202. doi: <a href="https://doi.org/10.1038/nature10282">10.1038/nature10282</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_22"></a>Isbell, F., Craven, D., Connolly, J., Loreau, M., Schmid, B., Beierkuhnlein, C., Bezemer, T.M., <i>et al.</i> (2015) “Biodiversity Increases the Resistance of Ecosystem Productivity to Climate Extremes”. <i>Nature</i> [online] 526 (7574), 574–577. doi: <a href="https://doi.org/10.1038/nature15374">10.1038/nature15374</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_23"></a>Lapola, D.M., Pinho, P., Barlow, J., Aragão, L.E.O.C., Berenguer, E., Carmenta, R., Liddy, H.M., <i>et al.</i> (2023) “The Drivers and Impacts of Amazon Forest Degradation”. <i>Science</i> [online] 379 (6630), eabp8622. doi: <a href="https://doi.org/10.1126/science.abp8622">10.1126/science.abp8622</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_24"></a>Le Provost, G., Schenk, N.V., Penone, C., Thiele, J., Westphal, C., Allan, E., Ayasse, M., <i>et al.</i> (2023) “The Supply of Multiple Ecosystem Services Requires Biodiversity across Spatial Scales”. <i>Nature Ecology &#38; Evolution</i> [online] 7 (2), 236–249. doi: <a href="https://doi.org/10.1038/s41559-022-01918-5">10.1038/s41559-022-01918-5</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_25"></a>Liang, J., Gamarra, J.G.P., Picard, N., Zhou, M., Pijanowski, B., Jacobs, D.F., Reich, P.B., <i>et al.</i> (2022) “Co-Limitation towards Lower Latitudes Shapes Global Forest Diversity Gradients”. <i>Nature Ecology &#38; Evolution</i> [online] 6 (10, 10), 1423–1437. doi: <a href="https://doi.org/10.1038/s41559-022-01831-x">10.1038/s41559-022-01831-x</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_26"></a>Locke, H., Rockström, J., Bakker, P., Bapna, M., Gough, M., Hilty, J., Lambertini, M., <i>et al.</i> (2021) <i>A Nature-Positive World: The Global Goal for Nature</i>. [online] Available at: <a href="https://library.wcs.org/doi/ctl/view/mid/33065/pubid/DMX3974900000.aspx">https://library.wcs.org/doi/ctl/view/mid/33065/pubid/DMX3974900000.aspx</a> (Accessed: December 3, 2022)</div>
  <div class="csl-entry"><a id="citeproc_bib_item_27"></a>Lovejoy, T.E. (2016) “Conservation Biology: The Importance of Wilderness”. <i>Current Biology</i> [online] 26 (23), R1235–R1237. doi: <a href="https://doi.org/10.1016/j.cub.2016.10.038">10.1016/j.cub.2016.10.038</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_28"></a>Malhi, Y., Doughty, C.E., Galetti, M., Smith, F.A., Svenning, J.-C., and Terborgh, J.W. (2016) “Megafauna and Ecosystem Function from the Pleistocene to the Anthropocene”. <i>Proceedings of the National Academy of Sciences</i> [online] 113 (4), 838–846. doi: <a href="https://doi.org/10.1073/pnas.1502540113">10.1073/pnas.1502540113</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_29"></a>Malhi, Y., Riutta, T., Wearn, O.R., Deere, N.J., Mitchell, S.L., Bernard, H., Majalap, N., <i>et al.</i> (2022) “Logged Tropical Forests Have Amplified and Diverse Ecosystem Energetics”. <i>Nature</i> [online] 612 (7941), 707–713. doi: <a href="https://doi.org/10.1038/s41586-022-05523-1">10.1038/s41586-022-05523-1</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_30"></a>Martin, P.A., Newton, A.C., and Bullock, J.M. (2013) “Carbon Pools Recover More Quickly than Plant Biodiversity in Tropical Secondary Forests”. <i>Proceedings of the Royal Society B: Biological Sciences</i> [online] 280 (1773), 20132236. doi: <a href="https://doi.org/10.1098/rspb.2013.2236">10.1098/rspb.2013.2236</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_31"></a>Martin, T.G. and Watson, J.E.M. (2016) “Intact Ecosystems Provide Best Defence against Climate Change”. <i>Nature Climate Change</i> [online] 6 (2), 122–124. doi: <a href="https://doi.org/10.1038/nclimate2918">10.1038/nclimate2918</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_32"></a>McMahon, S.M., Bebber, D.P., Butt, N., Crockatt, M., Kirby, K., Parker, G.G., Riutta, T., <i>et al.</i> (2015) “Ground Based LiDAR Demonstrates the Legacy of Management History to Canopy Structure and Composition across a Fragmented Temperate Woodland”. <i>Forest Ecology and Management</i> [online] 335, 255–260. doi: <a href="https://doi.org/10.1016/j.foreco.2014.08.039">10.1016/j.foreco.2014.08.039</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_33"></a>Milner-Gulland, E.J. (2022) “Don’t Dilute the Term Nature Positive”. <i>Nature Ecology &#38; Evolution</i> [online] 6 (9), 1243–1244. doi: <a href="https://doi.org/10.1038/s41559-022-01845-5">10.1038/s41559-022-01845-5</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_34"></a>Mitchell, M.G.E., Bennett, E.M., and Gonzalez, A. (2015) “Strong and Nonlinear Effects of Fragmentation on Ecosystem Service Provision at Multiple Scales”. <i>Environmental Research Letters</i> [online] 10 (9), 094014. doi: <a href="https://doi.org/10.1088/1748-9326/10/9/094014">10.1088/1748-9326/10/9/094014</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_35"></a>Newbold, T., Hudson, L.N., Arnell, A.P., Contu, S., De Palma, A., Ferrier, S., Hill, S.L.L., <i>et al.</i> (2016) “Has Land Use Pushed Terrestrial Biodiversity beyond the Planetary Boundary? A Global Assessment”. <i>Science</i> [online] 353 (6296), 288–291. doi: <a href="https://doi.org/10.1126/science.aaf2201">10.1126/science.aaf2201</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_36"></a>Noss, R.F. (1991) “Landscape Connectivity: Different Functions at Different Scales”. in <i>Conserving Biodiversity: A Unified Approach</i>. ed. by Hudson, W. 27–39</div>
  <div class="csl-entry"><a id="citeproc_bib_item_37"></a>Oehri, J., Schmid, B., Schaepman-Strub, G., and Niklaus, P.A. (2017) “Biodiversity Promotes Primary Productivity and Growing Season Lengthening at the Landscape Scale”. <i>Proceedings of the National Academy of Sciences</i> [online] 114 (38), 10160–10165. doi: <a href="https://doi.org/10.1073/pnas.1703928114">10.1073/pnas.1703928114</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_38"></a>Oliver, T.H., Heard, M.S., Isaac, N.J., Roy, D.B., Procter, D., Eigenbrod, F., Freckleton, R., <i>et al.</i> (2015) “Biodiversity and Resilience of Ecosystem Functions”. <i>Trends in Ecology &#38; Evolution</i> [online] 30 (11), 673–684. doi: <a href="https://doi.org/10.1016/j.tree.2015.08.009">10.1016/j.tree.2015.08.009</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_39"></a>Olson, D.M., Dinerstein, E., Wikramanayake, E.D., Burgess, N.D., Powell, G.V.N., Underwood, E.C., D’amico, J.A., <i>et al.</i> (2001) “Terrestrial Ecoregions of the World: A New Map of Life on Earth: A New Global Map of Terrestrial Ecoregions Provides an Innovative Tool for Conserving Biodiversity”. <i>BioScience</i> [online] 51 (11), 933–938. doi: <a href="https://doi.org/10.1641/0006-3568(2001)051[0933:TEOTWA]2.0.CO;2">10.1641/0006-3568(2001)051[0933:TEOTWA]2.0.CO;2</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_40"></a>Povilitis, T. (2001) “Toward a Robust Natural Imperative for Conservation”. <i>Conservation Biology</i> [online] 15 (2), 533–535. doi: <a href="https://doi.org/10.1046/j.1523-1739.2001.015002533.x">10.1046/j.1523-1739.2001.015002533.x</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_41"></a>Redford, K.H. and Dudley, N. (2024) “Areas of Hope: Ensuring the Conservation of Future Values of Nature”. <i>Oryx</i> [online] 58 (3), 273–274. doi: <a href="https://doi.org/10.1017/S0030605324000553">10.1017/S0030605324000553</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_42"></a>Riva, F. and Fahrig, L. (2023) “Obstruction of Biodiversity Conservation by Minimum Patch Size Criteria”. <i>Conservation Biology</i> [online] n/a (n/a). doi: <a href="https://doi.org/10.1111/cobi.14092">10.1111/cobi.14092</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_43"></a>Rogers, B.M., Mackey, B., Shestakova, T.A., Keith, H., Young, V., Kormos, C.F., DellaSala, D.A., <i>et al.</i> (2022) “Using Ecosystem Integrity to Maximize Climate Mitigation and Minimize Risk in International Forest Policy”. <i>Frontiers in Forests and Global Change</i> [online] 5. Available at: <a href="https://www.frontiersin.org/articles/10.3389/ffgc.2022.929281">https://www.frontiersin.org/articles/10.3389/ffgc.2022.929281</a> (Accessed: April 1, 2023)</div>
  <div class="csl-entry"><a id="citeproc_bib_item_44"></a>Sales, L.P. and Pires, M.M. (2023) “Identifying Climate Change Refugia for South American Biodiversity”. <i>Conservation Biology</i> [online] n/a (n/a). doi: <a href="https://doi.org/10.1111/cobi.14087">10.1111/cobi.14087</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_45"></a>Siipi, H. (2004) “Naturalness In Biological Conservation”. <i>Journal of Agricultural and Environmental Ethics</i> [online] 17 (6), 457–477. doi: <a href="https://doi.org/10.1007/s10806-004-1466-1">10.1007/s10806-004-1466-1</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_46"></a>Symes, W.S., Edwards, D.P., Miettinen, J., Rheindt, F.E., and Carrasco, L.R. (2018) “Combined Impacts of Deforestation and Wildlife Trade on Tropical Biodiversity Are Severely Underestimated”. <i>Nature Communications</i> [online] 9 (1), 4052. doi: <a href="https://doi.org/10.1038/s41467-018-06579-2">10.1038/s41467-018-06579-2</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_47"></a>United Nations Environment Programme (2022) <i>The Benefits of Ecosystem Restoration: 11 Lessons Learned from an Analysis of 5 European Restoration Initiatives</i>. doi: <a href="https://doi.org/10.34892/5pa7-g318">10.34892/5pa7-g318</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_48"></a>Valente, J.J., Rivers, J.W., Yang, Z., Nelson, S.K., Northrup, J.M., Roby, D.D., Meyer, C.B., <i>et al.</i> (2023) “Fragmentation Effects on an Endangered Species across a Gradient from the Interior to Edge of Its Range”. <i>Conservation Biology</i> [online] n/a (n/a). doi: <a href="https://doi.org/10.1111/cobi.14091">10.1111/cobi.14091</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_49"></a>Vicente, J.R., Fernandes, R.F., Randin, C.F., Broennimann, O., Gonçalves, J., Marcos, B., Pôças, I., <i>et al.</i> (2013) “Will Climate Change Drive Alien Invasive Plants into Areas of High Protection Value? An Improved Model-Based Regional Assessment to Prioritise the Management of Invasions”. <i>Journal of Environmental Management</i> [online] 131, 185–195. doi: <a href="https://doi.org/10.1016/j.jenvman.2013.09.032">10.1016/j.jenvman.2013.09.032</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_50"></a>Vyawahare, M. (2022) <i>Deforestation-Neutral Mining? Madagascar Study Shows It Can Be Done, but It’s Complicated</i> [online] Available at: <a href="https://news.mongabay.com/2022/05/deforestation-neutral-mining-madagascar-study-shows-it-can-be-done-but-its-complicated/">https://news.mongabay.com/2022/05/deforestation-neutral-mining-madagascar-study-shows-it-can-be-done-but-its-complicated/</a> (Accessed: May 24, 2022)</div>
  <div class="csl-entry"><a id="citeproc_bib_item_51"></a>Watson, J.E.M., Evans, T., Venter, O., Williams, B., Tulloch, A., Stewart, C., Thompson, I., <i>et al.</i> (2018) “The Exceptional Value of Intact Forest Ecosystems”. <i>Nature Ecology &#38; Evolution</i> [online] 2 (4), 599–610. doi: <a href="https://doi.org/10.1038/s41559-018-0490-x">10.1038/s41559-018-0490-x</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_52"></a>Willis, K.J. and Birks, H.J.B. (2006) “What Is Natural? The Need for a Long-Term Perspective in Biodiversity Conservation”. <i>Science</i> [online] 314 (5803), 1261–1265. doi: <a href="https://doi.org/10.1126/science.1122667">10.1126/science.1122667</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_53"></a>World Wildlife Fund (2022) <i>Living Planet Report 2022: Building a Nature-Positive Society</i> [online] ed. by Almond, R., Grooten, M., Juffe Bignoli, D., and Petersen, T. Gland, Switzerland: WWF. Available at: <a href="https://www.wwf.org.uk/our-reports/living-planet-report-2022">https://www.wwf.org.uk/our-reports/living-planet-report-2022</a> (Accessed: April 18, 2023)</div>
</div>
