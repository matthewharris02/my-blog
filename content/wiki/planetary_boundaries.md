+++
title = "Planetary Boundaries"
author = ["Matthew Harris"]
draft = false
math = true
toc = true
group_tag = "Conservation"
+++

<div class="ox-hugo-toc toc">

<div class="heading">Table of Contents</div>

- [The Planetary Boundaries: A Summary](#the-planetary-boundaries-a-summary)
- [Positive and Negative views on the Planetary Boundaries framework](#positive-and-negative-views-on-the-planetary-boundaries-framework)
    - [Positive](#positive)
    - [Negative -- general](#negative-general)
    - [Land-Use Boundary Criticisms](#land-use-boundary-criticisms)
    - [the Biosphere Boundary Criticisms](#the-biosphere-boundary-criticisms)
- [Other references](#other-references)
- [New/Alternative Frameworks: Earth System Boundaries](#new-alternative-frameworks-earth-system-boundaries)
- [Downscaling the planetary boundaries](#downscaling-the-planetary-boundaries)
- [Bibliography](#bibliography)

</div>
<!--endtoc-->

This is the Planetary Boundaries Summary

<!--more-->


## The Planetary Boundaries: A Summary {#the-planetary-boundaries-a-summary}

<div class="table-caption">
  <span class="table-number">Table 1:</span>
  Summary of planetary boundaries and whether they've been crossed
</div>

| Boundary                                  | Crossed [&amp;] | Reference |
|-------------------------------------------|-----------------|-----------|
| Climate Change                            | Y               | [2]       |
| Biosphere Integrity: Genetic diversity    | R               | [2]       |
| Biosphere Integrity: Functional Diversity | ?               | [2, 5]    |
| Nitrogen Cycle                            | R               | [2]       |
| Phosphorus Cycle                          | R               | [2]       |
| Stratospheric ozone depletion             | G               | [2]       |
| Ocean acidification                       | G               | [2]       |
| Freshwater use: green water               | Y/R             | [4]       |
| Freshwater use: blue water                | G               | [2]       |
| Land-system change                        | Y               | [2]       |
| Atmospheric aerosol loading               | ?               | [2]       |
| Chemical Pollution/Novel Entities         | Y/R             | [3]       |

-   [&amp;] Codes (after [2]):
    -   G[reen]: below boundary (safe)
    -   Y[ellow]: In zone of uncertainty (increasing risk)
    -   R[ed]: Beyond zone of uncertainty (high risk)
    -   ?: Boundary not yet quantified

Key references:

-   [1] **Original paper**: <a href="#citeproc_bib_item_13">Rockström <i>et al.</i>, 2009</a>
-   [2] **More recent paper**: <a href="#citeproc_bib_item_14">Steffen <i>et al.</i>, 2015</a>
-   [3] **Novel entities**: <a href="#citeproc_bib_item_10">Persson <i>et al.</i>, 2022</a>
-   [4] **Green water**: <a href="#citeproc_bib_item_16">Wang-Erlandsson <i>et al.</i>, 2022</a>
-   [5] **Functional diversity**: <a href="#citeproc_bib_item_9">Newbold <i>et al.</i>, 2016</a>


## Positive and Negative views on the Planetary Boundaries framework {#positive-and-negative-views-on-the-planetary-boundaries-framework}


### Positive {#positive}

1.  it "brings a holistic, systemic epistemology that is valuable for characterizing global environmental systems" <a href="#citeproc_bib_item_2">Baum and Handoh, 2014, 13</a>
2.  "The idea [of planetary boundaries] is conceptually brilliant and politically seductive: clear, quantitative measurements with no obvious judgements on what is 'right' or 'wrong' to include. It is also liberating. Here is humanity's safe space: within it, do what you want" <a href="#citeproc_bib_item_5">Lewis, 2012, 417</a>


### Negative -- general {#negative-general}

1.  "it often says little about the impacts to humanity of crossing environmental boundaries" <a href="#citeproc_bib_item_2">Baum and Handoh, 2014, 13</a>


### Land-Use Boundary Criticisms {#land-use-boundary-criticisms}

-   <a href="#citeproc_bib_item_1">Bass, 2009</a>
    -   "there is much work to be done before the concept [PBs] can be used practically -- before it can be 'operationalized'" _[113]_
    -   the 15% boundary is not a consensus value (although the original authors will recognise that) and has little scientific authentication -- if so, why would governments follow it and not use a higher/lower value
    -   additionally, the idea that land use undermines human well-being seems counter-intuitive from the history of agricultural expansion and well-being improvements
    -   "The boundary of 15 per cent land-use change is, in practice, a premature policy guideline that dilutes the authors' overall scientific proposition. Instead, the authors might want to consider a limit on soil degradation or soil loss. This would be a more valid and useful indicator of the state of terrestrial health. More satisfactory policy guidelines on land use could subsequently be constructed, based on this and other relevant planetary boundaries" _[114]_


### the Biosphere Boundary Criticisms {#the-biosphere-boundary-criticisms}

References:

-   <a href="#citeproc_bib_item_7">Montoya <i>et al.</i>, 2018</a>
    -   Response: <a href="#citeproc_bib_item_12">Rockström <i>et al.</i>, 2017</a>
        -   Response: <a href="#citeproc_bib_item_8">Montoya <i>et al.</i>, 2018</a>
-   <a href="#citeproc_bib_item_6">Mace <i>et al.</i>, 2014</a>


## Other references {#other-references}

-   <a href="#citeproc_bib_item_5">Lewis, 2012</a>
    -   <a href="#citeproc_bib_item_15">Stockholm Resilience Centre, 2012</a> -- "Hence it would be a mistake to see any of the boundaries, as problems to be addressed by decision-makers at one spatial (global) scale only. This was by no means the purpose of the initial planetary boundaries analysis."
-   <a href="#citeproc_bib_item_3">Galaz, 2012</a>
-   <a href="#citeproc_bib_item_4">Galaz <i>et al.</i>, 2016</a>
-   <a href="#citeproc_bib_item_9">Newbold <i>et al.</i>, 2016</a>


## New/Alternative Frameworks: Earth System Boundaries {#new-alternative-frameworks-earth-system-boundaries}

-   <a href="#citeproc_bib_item_11">Rockström <i>et al.</i>, 2023</a> -- at first glance, appears to just be the PB framework in an alterative way, but need **to read** properly


## Downscaling the planetary boundaries {#downscaling-the-planetary-boundaries}

Although the PB framework is **not** designed to be downscaled (i.e., used at a scale smaller than planetary; (<a href="#citeproc_bib_item_14">Steffen <i>et al.</i>, 2015</a>)), different authors have still attempted to downscale the boundaries and combine them with other environmental impact measurements.


## Bibliography {#bibliography}

<style>.csl-entry{text-indent: -1.5em; margin-left: 1.5em;}</style><div class="csl-bib-body">
  <div class="csl-entry"><a id="citeproc_bib_item_1"></a>Bass, S. (2009) “Planetary Boundaries: Keep off the Grass”. <i>Nature Climate Change</i> [online] 1 (910), 113–114. doi: <a href="https://doi.org/10.1038/climate.2009.94">10.1038/climate.2009.94</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_2"></a>Baum, S.D. and Handoh, I.C. (2014) “Integrating the Planetary Boundaries and Global Catastrophic Risk Paradigms”. <i>Ecological Economics</i> [online] 107, 13–21. doi: <a href="https://doi.org/10.1016/j.ecolecon.2014.07.024">10.1016/j.ecolecon.2014.07.024</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_3"></a>Galaz, V. (2012) “Planetary Boundaries Concept Is Valuable”. <i>Nature</i> [online] 486 (7402), 191. doi: <a href="https://doi.org/10.1038/486191c">10.1038/486191c</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_4"></a>Galaz, V., de Zeeuw, A., Shiroyama, H., and Tripley, D. (2016) “Planetary Boundaries— Governing Emerging Risks and Opportunities”. <i>Solutions</i> [online] 7 (3), 46–54. Available at: <a href="https://thesolutionsjournal.com/article/planetary-boundaries-governing-emerging-risks-and-opportunities/">https://thesolutionsjournal.com/article/planetary-boundaries-governing-emerging-risks-and-opportunities/</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_5"></a>Lewis, S.L. (2012) “We Must Set Planetary Boundaries Wisely”. <i>Nature</i> [online] 485 (7399), 417. doi: <a href="https://doi.org/10.1038/485417a">10.1038/485417a</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_6"></a>Mace, G.M., Reyers, B., Alkemade, R., Biggs, R., Chapin, F.S., Cornell, S.E., Díaz, S., <i>et al.</i> (2014) “Approaches to Defining a Planetary Boundary for Biodiversity”. <i>Global Environmental Change</i> [online] 28, 289–297. doi: <a href="https://doi.org/10.1016/j.gloenvcha.2014.07.009">10.1016/j.gloenvcha.2014.07.009</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_7"></a>Montoya, J.M., Donohue, I., and Pimm, S.L. (2018) “Planetary Boundaries for Biodiversity: Implausible Science, Pernicious Policies”. <i>Trends in Ecology &#38; Evolution</i> [online] 33 (2), 71–73. doi: <a href="https://doi.org/10.1016/j.tree.2017.10.004">10.1016/j.tree.2017.10.004</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_8"></a>Montoya, J.M., Donohue, I., and Pimm, S.L. (2018) “Why a Planetary Boundary, If It Is Not Planetary, and the Boundary Is Undefined? A Reply to Rockström et Al.” <i>Trends in Ecology &#38; Evolution</i> [online] 33 (4), 234. doi: <a href="https://doi.org/10.1016/j.tree.2018.01.008">10.1016/j.tree.2018.01.008</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_9"></a>Newbold, T., Hudson, L.N., Arnell, A.P., Contu, S., De Palma, A., Ferrier, S., Hill, S.L.L., <i>et al.</i> (2016) “Has Land Use Pushed Terrestrial Biodiversity beyond the Planetary Boundary? A Global Assessment”. <i>Science</i> [online] 353 (6296), 288–291. doi: <a href="https://doi.org/10.1126/science.aaf2201">10.1126/science.aaf2201</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_10"></a>Persson, L., Carney Almroth, B.M., Collins, C.D., Cornell, S., de Wit, C.A., Diamond, M.L., Fantke, P., <i>et al.</i> (2022) “Outside the Safe Operating Space of the Planetary Boundary for Novel Entities”. <i>Environmental Science &#38; Technology</i> [online] 56 (3), 1510–1521. doi: <a href="https://doi.org/10.1021/acs.est.1c04158">10.1021/acs.est.1c04158</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_11"></a>Rockström, J., Gupta, J., Qin, D., Lade, S.J., Abrams, J.F., Andersen, L.S., Armstrong McKay, D.I., <i>et al.</i> (2023) “Safe and Just Earth System Boundaries”. <i>Nature</i> [online] 619 (7968), 102–111. doi: <a href="https://doi.org/10.1038/s41586-023-06083-8">10.1038/s41586-023-06083-8</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_12"></a>Rockström, J., Richardson, K., and Steffen, W. (2017) <i>A Fundamental Misrepresentation of the Planetary Boundaries Framework</i> [online] Available at: <a href="https://www.stockholmresilience.org/research/research-news/2017-11-20-a-fundamental-misrepresentation-of-the-planetary-boundaries-framework.html">https://www.stockholmresilience.org/research/research-news/2017-11-20-a-fundamental-misrepresentation-of-the-planetary-boundaries-framework.html</a> (Accessed: November 4, 2022)</div>
  <div class="csl-entry"><a id="citeproc_bib_item_13"></a>Rockström, J., Steffen, W., Noone, K., Persson, Å., Chapin, F.S.I., Lambin, E., Lenton, T.M., <i>et al.</i> (2009) “Planetary Boundaries: Exploring the Safe Operating Space for Humanity”. <i>Ecology and Society</i> [online] 14 (2). doi: <a href="https://doi.org/10.5751/ES-03180-140232">10.5751/ES-03180-140232</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_14"></a>Steffen, W., Richardson, K., Rockstrom, J., Cornell, S.E., Fetzer, I., Bennett, E.M., Biggs, R., <i>et al.</i> (2015) “Planetary Boundaries: Guiding Human Development on a Changing Planet”. <i>Science</i> [online] 347 (6223), 1259855. doi: <a href="https://doi.org/10.1126/science.1259855">10.1126/science.1259855</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_15"></a>Stockholm Resilience Centre (2012) <i>Planetary Boundaries Are Valuable for Policy</i> [online] Available at: <a href="https://www.stockholmresilience.org/research/research-news/2012-07-02-planetary-boundaries-are-valuable-for-policy.html">https://www.stockholmresilience.org/research/research-news/2012-07-02-planetary-boundaries-are-valuable-for-policy.html</a> (Accessed: July 17, 2022)</div>
  <div class="csl-entry"><a id="citeproc_bib_item_16"></a>Wang-Erlandsson, L., Tobian, A., van der Ent, R.J., Fetzer, I., te Wierik, S., Porkka, M., Staal, A., <i>et al.</i> (2022) “A Planetary Boundary for Green Water”. <i>Nature Reviews Earth &#38; Environment</i> [online] 3 (6), 380–392. doi: <a href="https://doi.org/10.1038/s43017-022-00287-8">10.1038/s43017-022-00287-8</a></div>
</div>
