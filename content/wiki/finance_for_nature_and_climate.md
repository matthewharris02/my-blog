+++
title = "Finance for Nature and Climate"
author = ["Matthew Harris"]
draft = false
math = true
toc = true
group_tag = "Conservation"
+++

## <span class="section-num">1</span> List of Initiatives, Coalitions and Announcements relating to Climate Finance [Collected] {#list-of-initiatives-coalitions-and-announcements-relating-to-climate-finance-collected}

Also see diagrams in <a href="#citeproc_bib_item_8">Finance for Biodiversity Initiative, 2021</a> and <a href="#citeproc_bib_item_26">Pinko <i>et al.</i>, 2021</a>

-   **XXXX-XX-XX** | Green Gigaton Challenge (<a href="#citeproc_bib_item_12"><i>Green Gigaton Challenge</i>, n.d.</a>; <a href="#citeproc_bib_item_7">Edwards, 2021</a>).
-   **2023-08-03** | CreditNature provide updated NARIA metric framework whitepaper (NO_ITEM_DATA:creditnature2023)
-   **2023-07-23** | Full ICVM criteria announced for the high-integrity carbon credits (<a href="#citeproc_bib_item_15">Integrity Council for the Voluntary Carbon Market, 2023</a>)
-   **2022-12-12** | Climate Fund for Nature -- Kering, L'Occitane Group, Mirova.
    -   "The ambitious Climate Fund for Nature will mobilize resources from the Luxury Fashion and Beauty sectors to protect and restore nature, with a particular focus on women empowerment. EUR 140m are already committed out of a EUR 300m target size and the fund will be open to new partner companies to support the scaling up of its positive impacts on the ground. The fund will be managed by Mirova"
    -   Operations start: Q1 2023
-   **2021-XX-XX** | Voluntary Carbon Market Integrity Initiative (<a href="#citeproc_bib_item_19">McCormick <i>et al.</i>, 2021</a>)
    -   new UK taskforce launched
-   **2021-XX-XX** | Uggla and General Atlantic climate fund (<a href="#citeproc_bib_item_28">Tett <i>et al.</i>, 2021</a>; <a href="#citeproc_bib_item_10">General Atlantic, 2021</a>).
    -   aims to raise USD 3 bn
-   **2021-XX-XX** | Rise Climate Fund (TPG) and Global Transition Fund (Brookfield) (NO_ITEM_DATA:tett.etal2021a)
    -   more than $ 1 bn of the total raised by TPG came from other corporations, many of which have publicly committed to cutting their emissions. The private equity manager has teamed up with more than 20 companies including Apple, Alphabet, Bank of America, General Electric and General Motors to form a 'Rise Climate Coalition'". TPG said it was "actively reducing our investment in the fossil fuel sector" from 0.5% to 0% of their AUM
-   **2021-XX-XX** | International Network for Sustainable Financial Policy Insights, Research, and Exchange (INSPIRE) launched by NGFS (NO_ITEM_DATA:NGFS2021).
-   **2021-XX-XX** | Announcement of LEAF Coalition (<a href="#citeproc_bib_item_29">Unilever, 2021</a>).
    -   Lowering Emissions by Accelerating Forest Finance (LEAF)
    -   $ 1bn pledged by countries and major companies
-   **2020-XX-XX** | AXA IM, BNP Paribas, Sycomore AM, Mirova
    -   "launch a joint initiative to develop pioneering tool for measuring investment impact on biodiversity" (<a href="#citeproc_bib_item_1">AXA IM, 2020</a>)
-   **2016-XX-XX** | Announcement of five-year IFC forest bond (NO_ITEM_DATA:conservationinternational.nda; <a href="#citeproc_bib_item_16">Klopfer and Panajyan, 2016</a>).
    -   USD 152 million, five-year bond for forests; cash or carbon-credit coupon
    -   developed with BHP Billiton and Conservation International
    -   "The Forests Bond offers investors a choice between a cash or interest payment in the form of Verified Carbon Units (VCUs) from the Kasigau Corridor REDD Project in Kenya" NO_ITEM_DATA:conservationinternational.nda


## <span class="section-num">2</span> Literature Summaries {#literature-summaries}


### <span class="section-num">2.1</span> Carbon Offsets {#carbon-offsets}

<a href="#citeproc_bib_item_2">Allen <i>et al.</i>, 2020</a> suggest four principles for using carbon offsets to reach net zero:

1.  "Cut emissions, use high quality offsets, and regularly revise offsetting strategy as best practice evolves"
2.  "Shift to carbon removal offsetting" -- rather than carbon reduction
3.  "Shift to long-lived storage" -- this has a low risk of reversal
4.  "Support the development of net zero aligned offsetting" through publicising and market-signalling


### <span class="section-num">2.2</span> REDD+ Costs {#redd-plus-costs}

-   <a href="#citeproc_bib_item_22">Nantongo and Vatn, 2019</a> -- estimating transaction costs of REDD+ in two REDD+ pilot projects: RDS Rio Negro, Brazil; and Kilosa Eastern-Central Tanzania
    -   <span class="underline">transaction costs</span> -- "the costs of establishing, maintaining and using a certain governance structure" (p. 2)


### <span class="section-num">2.3</span> Theory/Background {#theory-background}

<a href="#citeproc_bib_item_30">United Nations Environment Programme, 2021</a> classify finance for nature into:

1.  Public finance
    a. Governments
    b. Development finance institutions (DFIs)
       a. National
       b. Bilateral
       c. Multilateral
    c. Environment/climate funds
       a. National
       b. Bilateral/multilateral
2.  Private finance
    a. Commercial financial institutions
    b. Investors
    c. Corporations
    d. Philanthropies
3.  Financial instruments
    1.  Capital supply
    2.  Risk mitigation
    3.  Fiscal,revenue

<a href="#citeproc_bib_item_14">Hourcade <i>et al.</i>, 2021</a> discusses the current state of the financial system in the context of climate change and COVID-19.

<a href="#citeproc_bib_item_11">Graham and Silva-Chávez, 2016</a> -- Paris Agreement and REDD+/climate finance -- 'carrots' and 'sticks':

-   <span class="underline">Carrots</span>:
    -   Early Actor benefits, including international climate finance from donor countries and international development agencies
    -   “Improving Enabling Conditions for Investment in Green Business”
    -   International collaboration
-   <span class="underline">Sticks</span>:
    -   Countries using policy measures to meet their NDCs, particularly in the forest and land sector, such as: increasing regulation for emitters, reducing subsidies for high-emitting activities, shifting priorities to low-emissions developments (or ‘green growth’)
    -   Requirement for countries to report and be accountable for emissions increases the focus in transparency of actions and results, which will have implications on the private sector
    -   The private sector will face costs if they don’t engage in the battle against climate change

<a href="#citeproc_bib_item_27">Streck, 2016</a> -- Paris Agreement and REDD+


### <span class="section-num">2.4</span> Funding for IPLCs {#funding-for-iplcs}

-   <a href="#citeproc_bib_item_13">Hatcher <i>et al.</i>, 2021</a> -- lack of funding for IPLCs in forest management and tenure rights
    -   "donor support for IPLC tenure and forest management has remained relatively constant over the past decade, while falling far short of the level of support needed to meaningfully scale up the role of IPLCs in fighting climate change and biodiversity loss" (p. 15)
    -   "Between 2011 and 2020, donors disbursed approximately $2.7 billion (on average $270 million annually) for projects supporting IPLC tenure and forest management capabilities in tropical countries"; 11% towards tenure projects
    -   "Much of the funding identified here as support for IPLC tenure and forest management flows through large intermediaries and is unlikely to have reached IPLCs or their organizations directly"
    -   Funding by donor:

        | Donor           | Funding (USD) |
        |-----------------|---------------|
        | US              | 414 million   |
        | Norway          | 371 million   |
        | Germany         | 330.7 million |
        | UK              | 264.9 million |
        | Sweden          | 154.2 million |
        | Finland         | 89.8 million  |
        | Multilateral    | 1.3 billion   |
        | Private funding | 3% of total   |
    -   [REDD+]({{< relref "forest_conservation.md#redd-plus" >}}): "though much of the funding for REDD+ to date has been for readiness, the bulk of REDD+ funding is intended to be results based, and therefore the funding will depend on the government’s ability and interest in reducing deforestation" -- leads IPLCs to be vulnerable to lack of REDD+ funding, particularly as results-based financing "undervalues IPLCs longstanding contributions to keep deforestation low", and it is focused on carbon, not other ES.
    -   Barriers: lack of legal recognition of IPLC organisations
    -   16% of all disbursements and 5% of all commitments to the Amazon fund ($76 million) were disbursed to local communities. 1.4% of FCPF Readiness Fund’s disbursements ($6.7 mil) to IPs and CSO. 75% of the Tenure Facility’s ("purpose-built to fund tenure rights security projects led by IPLCs") disbursements "have gone directly to IPLCS-led projects and project support"


### <span class="section-num">2.5</span> Criticisms {#criticisms}

<a href="#citeproc_bib_item_17">Lang, 2016</a>, <a href="#citeproc_bib_item_18">2016</a> from REDD-Monitor (a anti-REDD+ blog) criticises the IFC bond arguing it is greenwashing


## <span class="section-num">3</span> Figures {#figures}

According to <a href="#citeproc_bib_item_30">United Nations Environment Programme, 2021</a>, a total USD 113 bn is invested in NbS, of which USD 23 bn is for Agriculture, Forestry and Fishing under domestic governance.

<a href="#citeproc_bib_item_6">Deutz <i>et al.</i>, 2020</a> estimate the current financing of biodiversity conservation and the potential flows in 2030 (see table on page 48).

<a href="#citeproc_bib_item_3">Choi <i>et al.</i>, 2020</a> provides information on the green finance for the International Development Finance Club, 2015-2019.

<a href="#citeproc_bib_item_9">Forest Trends’ Ecosystem Marketplace, 2021</a> -- State of Forest Finance 2021, summary of 2017-2019 and all data since 2000.

NO_ITEM_DATA:ICAI2021 -- UK International Climate Finance


### <span class="section-num">3.1</span> Landscape of Climate Finance {#landscape-of-climate-finance}

-   2019: <a href="#citeproc_bib_item_5">Climate Policy Initiative, 2020</a>
-   2021: <a href="#citeproc_bib_item_4">Climate Policy Initiative, 2021</a>

New reports (2022) summarise the entire decade, and focus on AFOLU.


### <span class="section-num">3.2</span> New York Declaration on Forests -- assessments {#new-york-declaration-on-forests-assessments}

-   <a href="#citeproc_bib_item_21">NYDF Assessment Partners, 2019</a>
-   <a href="#citeproc_bib_item_24">New York Declaration on Forests Assessment Partners, 2020</a>
-   <a href="#citeproc_bib_item_25">New York Declaration on Forests Assessment Partners, 2020</a>
-   <a href="#citeproc_bib_item_20">NYDF Assessment Partners, 2020</a>
-   <a href="#citeproc_bib_item_23">New York Declaration on Forests, 2022</a>


## <span class="section-num">4</span> Bibliography {#bibliography}

<style>.csl-entry{text-indent: -1.5em; margin-left: 1.5em;}</style><div class="csl-bib-body">
  <div class="csl-entry"><a id="citeproc_bib_item_1"></a>AXA IM (2020) <i>AXA IM, BNP Paribas AM, Sycomore AM and Mirova Launch Joint Initiative to Develop Pioneering Tool for Measuring Investment Impact on Biodiversity</i> [online] Press Release. Available at: <a href="https://realassets.axa-im.com/content/-/asset_publisher/x7LvZDsY05WX/content/axa-im-bnp-paribas-am-sycomore-am-and-mirova-launch-joint-initiative-to-develop-pioneering-tool-for-measuring-investment-impact-on-biodiversity/23818">https://realassets.axa-im.com/content/-/asset_publisher/x7LvZDsY05WX/content/axa-im-bnp-paribas-am-sycomore-am-and-mirova-launch-joint-initiative-to-develop-pioneering-tool-for-measuring-investment-impact-on-biodiversity/23818</a> (Accessed: July 8, 2021)</div>
  <div class="csl-entry"><a id="citeproc_bib_item_2"></a>Allen, M., Axelsson, K., Caldecott, B., Hale, T., Hepburn, C., Mitchell-Larson, E., Malhi, Y., <i>et al.</i> (2020) <i>The Oxford Principles for Net Zero Aligned Carbon Offsetting</i> [online] University of Oxford. Available at: <a href="https://www.smithschool.ox.ac.uk/publications/reports/Oxford-Offsetting-Principles-2020.pdf">https://www.smithschool.ox.ac.uk/publications/reports/Oxford-Offsetting-Principles-2020.pdf</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_3"></a>Choi, J., Macquarie, R., Naran, B., and Falconer, A. (2020) <i>IDFC Green Finance Mapping 2020</i> [online] Climate Policy Initiative. Available at: <a href="https://www.climatepolicyinitiative.org/publication/idfc-green-finance-mapping-2020/">https://www.climatepolicyinitiative.org/publication/idfc-green-finance-mapping-2020/</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_4"></a>Climate Policy Initiative (2021) <i>Global Landscape of Climate Finance 2021</i> [online] Climate Policy Initiative. Available at: <a href="https://www.climatepolicyinitiative.org/publication/global-landscape-of-climate-finance-2021/">https://www.climatepolicyinitiative.org/publication/global-landscape-of-climate-finance-2021/</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_5"></a>Climate Policy Initiative (2020) <i>Updated View on the Global Landscape of Climate Finance 2019</i> [online] Climate Policy Initiative. Available at: <a href="https://www.climatepolicyinitiative.org/publication/updated-view-on-the-global-landscape-of-climate-finance-2019/">https://www.climatepolicyinitiative.org/publication/updated-view-on-the-global-landscape-of-climate-finance-2019/</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_6"></a>Deutz, A., Heal, G.M., Niu, R., Swanson, E., Townshend, T., Li, Z., Delmar, A., <i>et al.</i> (2020) <i>Financing Nature: Closing the Global Biodiversity Financing Gap</i> [online] The Paulson Institute, The Nature Conservancy, and the Cornell Atkinson Center for Sustainability. Available at: <a href="https://www.paulsoninstitute.org/conservation/financing-nature-report/">https://www.paulsoninstitute.org/conservation/financing-nature-report/</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_7"></a>Edwards, R. (2021) <i>The Green Gigaton Challenge: Bringing REDD+ to Scale Primer</i> [online] Washington, D.C.: Green Gigaton Challenge. Available at: <a href="https://www.greengigaton.com/uploads/1/3/4/7/134750777/green_gigaton_challenge_primer_june_2021.pdf">https://www.greengigaton.com/uploads/1/3/4/7/134750777/green_gigaton_challenge_primer_june_2021.pdf</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_8"></a>Finance for Biodiversity Initiative (2021) <i>The Climate-Nature Nexus: Implications for the Financial Sector</i> [online] Finance for Biodiversity Initiative. Available at: <a href="https://a1be08a4-d8fb-4c22-9e4a-2b2f4cb7e41d.filesusr.com/ugd/643e85_276c8cfee51d4bca97c082bb64e8058a.pdf">https://a1be08a4-d8fb-4c22-9e4a-2b2f4cb7e41d.filesusr.com/ugd/643e85_276c8cfee51d4bca97c082bb64e8058a.pdf</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_9"></a>Forest Trends’ Ecosystem Marketplace (2021) <i>Buyers of Voluntary Carbon Offsets, a Regional Analysis. State of the Voluntary Carbon Markets 2020. Third Installment Featuring European Buyers Offset Prices, Volumes, and Insights</i>. Washington, DC: Forest Trends</div>
  <div class="csl-entry"><a id="citeproc_bib_item_10"></a>General Atlantic (2021) <i>General Atlantic Forms BeyondNetZero to Invest in Growth Companies Delivering Innovative Climate Solutions</i> [online] New York, NY and London, GB: Press Release. Available at: <a href="https://www.generalatlantic.com/media-article/general-atlantic-forms-beyondnetzero-to-invest-in-growth-companies-delivering-innovative-climate-solutions/">https://www.generalatlantic.com/media-article/general-atlantic-forms-beyondnetzero-to-invest-in-growth-companies-delivering-innovative-climate-solutions/</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_11"></a>Graham, P. and Silva-Chávez, G. (2016) <i>The Implications of the Paris Climate Agreement for Private Sector Roles in REDD+</i> [online] Forest Trends. Available at: <a href="https://www.forest-trends.org/publications/the-implications-of-the-paris-climate-agreement-for-private-sector-roles-in-redd/">https://www.forest-trends.org/publications/the-implications-of-the-paris-climate-agreement-for-private-sector-roles-in-redd/</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_12"></a><i>Green Gigaton Challenge</i> (n.d.) [online] Available at: <a href="https://www.greengigaton.com/">https://www.greengigaton.com/</a> (Accessed: July 23, 2021)</div>
  <div class="csl-entry"><a id="citeproc_bib_item_13"></a>Hatcher, J., Owen, M., and Yin, D. (2021) <i>Falling Short: Donor Funding for Indigenous Peoples and Local Communities to Secure Tenure Rights and Manage Forests in Tropical Countries (2011-2020)</i> [online] Oslo: Rainforest Foundation Norway. Available at: <a href="https://www.regnskog.no/en/news/falling-short">https://www.regnskog.no/en/news/falling-short</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_14"></a>Hourcade, J.C., Glemarec, Y., de Coninck, H., Bayat-Renoux, F., Ramakrishna, K., and Revi, A. (2021) <i>Scaling up Climate Finance in the Context of Covid-19: A Science-Based Call for Financial Decision-Makers</i> [online] South Korea: Green Climate Fund. Available at: <a href="https://www.greenclimate.fund/document/scaling-up-climate-finance">https://www.greenclimate.fund/document/scaling-up-climate-finance</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_15"></a>Integrity Council for the Voluntary Carbon Market (2023) <i>Global Benchmark for High-Integrity Carbon Credits Aims to Mobilize Climate Finance at Speed and Scale</i> [online] Available at: <a href="https://icvcm.org/global-benchmark-for-high-integrity-carbon-credits-aims-to-mobilize-climate-finance-at-speed-and-scale/">https://icvcm.org/global-benchmark-for-high-integrity-carbon-credits-aims-to-mobilize-climate-finance-at-speed-and-scale/</a> (Accessed: August 15, 2023)</div>
  <div class="csl-entry"><a id="citeproc_bib_item_16"></a>Klopfer, A. and Panajyan, S. (2016) <i>IFC Issues Innovative \$152 Million Bond to Protect Forests and Deepen Carbon-Credit Markets</i> [online] Washington D.C: Press Release. Available at: <a href="https://ifcext.ifc.org/ifcext/Pressroom/IFCPressRoom.nsf/0/594A016A78A7B14E8525805D00461397">https://ifcext.ifc.org/ifcext/Pressroom/IFCPressRoom.nsf/0/594A016A78A7B14E8525805D00461397</a> (Accessed: July 7, 2021)</div>
  <div class="csl-entry"><a id="citeproc_bib_item_17"></a>Lang, C. (2016) <i>IFC Launches US\$152 Million REDD Greenwashing Bond for Mining Corporation BHP Billiton. With Help from Conservation International</i> [online] Available at: <a href="https://redd-monitor.org/2016/11/01/ifc-launches-us152-million-redd-greenwashing-bond-for-mining-corporation-bhp-billiton-with-help-from-conservation-international/">https://redd-monitor.org/2016/11/01/ifc-launches-us152-million-redd-greenwashing-bond-for-mining-corporation-bhp-billiton-with-help-from-conservation-international/</a> (Accessed: July 7, 2021)</div>
  <div class="csl-entry"><a id="citeproc_bib_item_18"></a>Lang, C. (2016) <i>Response from IFC Fails to Answer Any of REDD-Monitor’s Questions About Its “Forest Bond”</i> [online] Available at: <a href="https://redd-monitor.org/2016/11/29/response-from-ifc-fails-to-answer-any-of-redd-monitors-questions-about-its-forest-bond/">https://redd-monitor.org/2016/11/29/response-from-ifc-fails-to-answer-any-of-redd-monitors-questions-about-its-forest-bond/</a> (Accessed: July 7, 2021)</div>
  <div class="csl-entry"><a id="citeproc_bib_item_19"></a>McCormick, M., Hook, L., Jacobs, J., and Chu, A. (2021) “Carbon Capture’s next Act”. <i>Financial Times: Energy Source</i> [online] 29 July. Available at: <a href="https://www.ft.com/content/4b45acc3-4180-46c1-add1-84850361b2ed">https://www.ft.com/content/4b45acc3-4180-46c1-add1-84850361b2ed</a> (Accessed: August 14, 2023)</div>
  <div class="csl-entry"><a id="citeproc_bib_item_20"></a>NYDF Assessment Partners (2020) <i>Goal 8 Assessment: Providing Finance for Forest Action. New York Declaration on Forests Progress Assessment</i> [online] Climate Focus (coordinator and editor). Available at: <a href="https://forestdeclaration.org/goals/goal-8#key">https://forestdeclaration.org/goals/goal-8#key</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_21"></a>NYDF Assessment Partners (2019) <i>Protecting and Restoring Forests: A Story of Large Commitments yet Limited Progress. New York Declaration on Forests Five-Year Assessment Report</i> [online] Climate Focus (coordinator and editor). Available at: <a href="https://www.climatefocus.com/publications/nydf-2019-progress-report-protecting-and-restoring-forests">https://www.climatefocus.com/publications/nydf-2019-progress-report-protecting-and-restoring-forests</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_22"></a>Nantongo, M. and Vatn, A. (2019) “Estimating Transaction Costs of REDD+”. <i>Ecological Economics</i> [online] 156, 1–11. doi: <a href="https://doi.org/10.1016/j.ecolecon.2018.08.014">10.1016/j.ecolecon.2018.08.014</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_23"></a>New York Declaration on Forests (2022) <i>Reward Results by Countries and Jurisdictions</i> [online] Available at: <a href="https://forestdeclaration.org/wp-content/uploads/2022/02/Forest-rewards-Progress-since-2014.pdf">https://forestdeclaration.org/wp-content/uploads/2022/02/Forest-rewards-Progress-since-2014.pdf</a> (Accessed: August 26, 2022)</div>
  <div class="csl-entry"><a id="citeproc_bib_item_24"></a>New York Declaration on Forests Assessment Partners (2020) <i>Goal 1 Assessment: Striving to End Natural Forest Loss</i> [online] Climate Focus (coordinator and editor). Available at: <a href="https://forestdeclaration.org/resources">https://forestdeclaration.org/resources</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_25"></a>New York Declaration on Forests Assessment Partners (2020) <i>Goal 9 Assessment: Rewarding Countries and Jurisdictions for Results</i> [online] Climate Focus (coordinator and editor). Available at: <a href="https://forestdeclaration.org/resources">https://forestdeclaration.org/resources</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_26"></a>Pinko, N., Pastor, A.O., Tonkonogy, B., and Choi, J. (2021) <i>Framework for Sustainable Finance Integrity</i> [online] Climate Policy Initiative. Available at: <a href="https://www.climatepolicyinitiative.org/publication/framework-for-sustainable-finance-integrity/">https://www.climatepolicyinitiative.org/publication/framework-for-sustainable-finance-integrity/</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_27"></a>Streck, C. (2016) “Mobilizing Finance for REDD+ After Paris”. <i>Journal for European Environmental &#38; Planning Law</i> [online] 13 (2), 146–166. doi: <a href="https://doi.org/10.1163/18760104-01302003">10.1163/18760104-01302003</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_28"></a>Tett, G., Nauman, B., Temple-West, P., and Talman, K. (2021) “TPG and Brookfield Haul in \$12bn for Climate Funds”. <i>Financial Times: Moral Money</i> [online] 28 July. Available at: <a href="https://www.ft.com/content/904d4f91-172d-428c-94ee-bcc6f36c83c0">https://www.ft.com/content/904d4f91-172d-428c-94ee-bcc6f36c83c0</a> (Accessed: July 28, 2021)</div>
  <div class="csl-entry"><a id="citeproc_bib_item_29"></a>Unilever (2021) <i>New Public-Private Coalition Launched to Mobilize More Than \$1 Billion to Protect Tropical Forests and Enhance Global Climate Action</i> [online] Washington D.C: Press Release. Available at: <a href="https://www.unilever.com/news/press-releases/2021/public-private-coalition-launched-to-protect-tropical-forests-and-enhance-global-climate-action.html">https://www.unilever.com/news/press-releases/2021/public-private-coalition-launched-to-protect-tropical-forests-and-enhance-global-climate-action.html</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_30"></a>United Nations Environment Programme (2021) <i>State of Finance for Nature</i> [online] Nairobi. Available at: <a href="https://www.unep.org/resources/state-finance-nature">https://www.unep.org/resources/state-finance-nature</a></div>
  <div class="csl-entry">NO_ITEM_DATA:creditnature2023</div>
  <div class="csl-entry">NO_ITEM_DATA:tett.etal2021a</div>
  <div class="csl-entry">NO_ITEM_DATA:NGFS2021</div>
  <div class="csl-entry">NO_ITEM_DATA:conservationinternational.nda</div>
  <div class="csl-entry">NO_ITEM_DATA:ICAI2021</div>
</div>
