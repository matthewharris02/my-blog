+++
title = "About"
author = ["Matthew Harris"]
draft = false
+++

Hi there!! Welcome to my 'blog'. I'm currently using it as a testing ground for hugo and to publish some of my long 'wiki' notes on topics I have studied or am working on.

If you want to know more about how this blog is setup, then visit [My Blog Setup]({{< ref "/blog/publishing-website-with-gitlab-hugo-emacs.md" >}} "My Blog Setup")
