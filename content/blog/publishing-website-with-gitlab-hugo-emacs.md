+++
title = "Publishing my website with GitLab CI pipelines, hugo and emacs"
author = ["Matthew Harris"]
draft = false
math = true
toc = true
+++

## Introduction {#introduction}

Hi! This document will detail how I have setup this blog with [Hugo](https://gohugo.io/), [emacs](https://www.gnu.org/software/emacs/), and [Org Mode](https://orgmode.org/), hosted on [Gitlab](https://gitlab.com/) pages. I write this partly as a record for me and a learning resource, as well as for anyone that may want to do something similar. Trying to set this up required **a lot** of trial and error (particularly error!), and I hope this makes it easier for others.


## An overview of the configuration {#an-overview-of-the-configuration}

The articles and posts for this blog are written in [Org Mode](https://orgmode.org/), a form of markup for the [emacs](https://www.gnu.org/software/emacs/) text editor. The website itself is generated using Hugo, a static-site generator that converts markdown files into html and creates a website. To convert the org-mode files into hugo-compatible markdown, I use [ox-hugo](https://ox-hugo.scripter.co). Both the org-mode files and the hugo website are hosted on gitlab, and the website is generated using gitlab pipelines and hosted on gitlab pages.

The theme for the website is my own [fork](https://github.com/matthewharris02/hermit-v2-fork) of the [Hermit - V2](https://github.com/1bl4z3r/hermit-V2) theme. I forked it, rather than making small edits in the hugo way (see later) for two reasons:

1.  I wanted to make quite a few changes to make it fit my purpose
2.  To help learn how hugo works, and become more familiar with html and css.


## Gitlab CI Setup {#gitlab-ci-setup}

All relevant `org` files are in a directory called `blog` and pushed to a private repository. On this repository a pipeline runs to:
a) convert the org files to hugo-compatible markdown; and
b) push the markdown files to the repo which holds the hugo website.

To do this, a `.gitlab-ci.yml` file sets up the pipeline.

```yaml
variables:
  GIT_SUBMODULE_STRATEGY: recursive
```

I am not sure this is entirely necessary for this repo, but ensures the submodules are cloned.

```yaml
build:
  image: silex/emacs:29.1-alpine-ci
  stage: build
```

This then uses the `silex/emacs` docker image and defines the build stage.


### Pre-setup SSH {#pre-setup-ssh}

But first, we must do some SSH stuff to be able to clone the website repository (to push the changed files to later):

```yaml
before_script:
  - apk add openssh-client git unzip sshpass
  - eval $(ssh-agent -s)
  - echo "$SSH_PRIVATE_KEY_TOOLKIT" | tr -d '\r' | ssh-add -
  - mkdir -p ~/.ssh
  - chmod 700 ~/.ssh
  - ssh-keyscan gitlab.com >> ~/.ssh/known_hosts
  - chmod 644 ~/.ssh/known_hosts
  - git config --global user.email <email-address>
  - git config --global user.name "Gitlab Runner"
```

This is mainly from [here](https://mmk2410.org/2022/02/08/publishing-my-website-using-gitlab-ci-pipelines/), [here](https://forum.gitlab.com/t/git-push-from-inside-a-gitlab-runner/30554/3) and [here](https://forum.gitlab.com/t/git-push-from-inside-a-gitlab-runner/30554/3). The first line pulls in the necessary packages. Then it sets and configures git using the variables private key variable defined in the repo (see below).


### Main script {#main-script}

After this, the pipeline then runs the main script:

First it clones the repo which holds the hugo site.

```yaml
script:
  - ssh git@gitlab.com
  - git clone git@gitlab.com:matthewharris02/my-blog.git ./my-blog
```

Then it creates a variable, `CHANGED_FILES` which lists only the files that have changed in the `blog` org directory. This is later passed into `export.el` so that hugo only re-builds those org files that have changed.

This however doesn't work if the most recent commit from the last pull isn't in the blog directory. As `CHANGED_FILES` will be an empty string if no files in the `blog` directory have changed, then if this happens `CHANGED_FILES` is re-defined to be all files in `blog` directory. This is not the best workaround, but it works effectively as the entire pipeline only runs when certain files/directories are changed (see below).

```yaml
- export CHANGED_FILES=$(git diff-tree --name-status --no-commit-id -r $CI_COMMIT_SHA -- blog)
- if [ "$CHANGED_FILES" == "" ]; then export CHANGED_FILES=$(ls blog/*.org); fi
```

Once this variable has been set, it runs the emacs script (see next section) that exports the org files to hugo-compatible markdown:

```yaml
- emacs -Q --script .build/export.el
```

Finally, it pushes the changes to the repo with the hugo website.

```yaml
- cd ./my-blog
- git remote rm origin && git remote add origin "git@gitlab.com:matthewharris02/my-blog.git"
- git add ./content
- export mydate=$(date +%Y-%m-%d-%H%M%S)
- git commit -m "New blog posts from CI/CD. $mydate" || echo "No changes, nothing to commit!"
- git push origin master
```

The `mydate` variable is defined so that the commit message includes the date of changes pushed. This is slightly unnecessary as obviously the date of the commit is automatic with git, but it makes it easier when reading commit messages without delving in to them.


### Only run when certain files change {#only-run-when-certain-files-change}

Finally the script defines some changes rules to only run when certain files/directories change. This is done as I push my other notes to the same repo that aren't published and I don't want the pipeline to run everytime I push other notes.

```yaml
rules:
  - changes:
      - .build/export.el
      - .csl/*
      - blog/*.org
      - org-setup-blog.org
      - .gitlab-ci.yml
```

The non-org files are all set-up files for this to work. This works with the `CHANGED_FILES` workaround above so that as soon as one file of these files change, the entire blog directory is re-exported.


## Emacs export script {#emacs-export-script}

To export the org files to hugo-markdown, the `.build/export.el` emacs lisp script is used. This is mainly from [here](https://gitlab.com/mmk2410/mmk2410.org/-/blob/main/.build/ox-hugo-build.el?ref_type=heads), with some additions.

The first part sets up the main emacs packages needed as well as some package-specific configuration that is needed for my org mode/hugo set up. The custom functions can also be found in my personal [emacs configuration](https://gitlab.com/matthewharris02/linux-config).

```emacs-lisp
(package-initialize)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)

(setq-default load-prefer-newer t)
(setq-default package-enable-at-startup nil)

(package-refresh-contents)
(package-install 'use-package)
(setq package-ser-dir (expand-file-name "./.packages"))
(add-to-list 'load-path package-user-dir)
(require 'use-package)
(setq use-package-always-ensure t)

(use-package org
  :config
  (setq org-export-with-tags nil)
  ;; Babel
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((R . t)
     (python . t)
     )
   )
  )

(use-package org-ref
  :after org
  :init
  ;; Define function to allow org-ref citations within ox-hugo export
  ;; https://ox-hugo.scripter.co/doc/org-ref-citations/
  (with-eval-after-load 'ox
    (defun my/org-ref-process-buffer--html (backend)
      "Preprocess `org-ref' citations to HTML format.

Do this only if the export backend is `html' or a derivative of
that."
      ;; `ox-hugo' is derived indirectly from `ox-html'.
      ;; ox-hugo <- ox-blackfriday <- ox-md <- ox-html
      (when (org-export-derived-backend-p backend 'html)
        (org-ref-process-buffer 'html)))
    (add-to-list 'org-export-before-parsing-hook #'my/org-ref-process-buffer--html))
  )

(use-package ox-hugo
  :after org)

(defun org-hugo--tag-processing-fn-remove-tags-maybe (tags-list info)
  "Remove user-specified tags/categories.
See `org-hugo-tag-processing-functions' for more info."
  ;; Use tag/category string (including @ prefix) exactly as used in Org file.
  (let ((tags-categories-to-be-removed '("blog"))) ;"my_tag" "@my_cat"
    (cl-remove-if (lambda (tag_or_cat)
                    (member tag_or_cat tags-categories-to-be-removed))
                  tags-list)))
(add-to-list 'org-hugo-tag-processing-functions
             #'org-hugo--tag-processing-fn-remove-tags-maybe)
```

Then, the script defines a function to export a file with ox-hugo.

```emacs-lisp
(defun my/export (file)
  (save-excursion
    (find-file file)
    (org-hugo-export-wim-to-md t)))
```

Then I pull in the `CHANGED_FILES` variable defined earlier to only get the files that have change. And finally, I loop over all the files in `changed-files` and export them.

```emacs-lisp
(set 'changed-files (split-string
                     (getenv "CHANGED_FILES") "\n"
                     ))
(mapcar (lambda (file)
          (unless (string= "" file) (my/export file))
          )
        changed-files
        )
```

The first command defines `changed-files` as the value of the `CHANGED_FILES` environment variable, using `getenv`, which is then converted to a list of strings where each element is the file path.

Then `mapcar` maps over the `changed-files` list and exports each file using the function defined above. The `unless` condition skips the function if any element in `changed-files` is an empty string. This happens if no files are changed and prevents ox-hugo (and therefore the entire pipeline) from failing. I am not sure if this is necessary now that the there is the conditional subsitution on `CHANGED_FILES` above.
