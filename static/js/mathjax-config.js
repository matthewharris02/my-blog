window.MathJax = {
  loader: {load: ['[tex]/mhchem', '[tex]/gensymb']},
  tex: {
    packages: {'[+]': ['mhchem', 'gensymb']},
    macros: {
      unit: ['\\,\\textrm{#1}', 1]
    },
  }
};
